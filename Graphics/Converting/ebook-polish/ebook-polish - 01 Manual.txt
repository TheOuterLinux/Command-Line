EBOOK-POLISH(1)                              calibre                              EBOOK-POLISH(1)

NAME
       ebook-polish - ebook-polish

          ebook-polish [options] input_file [output_file]

       Polishing  books  is all about putting the shine of perfection onto your carefully crafted
       e-books.

       Polishing tries to minimize the changes to the internal code of your e-book.  Unlike  con‐
       version,  it  does  not flatten CSS, rename files, change font sizes, adjust margins, etc.
       Every action performs only the minimum set of changes needed for the desired effect.

       You should use this tool as the last step in your e-book creation process.

       Note that polishing only works on files in the AZW3 or EPUB formats.

       Whenever you pass arguments to ebook-polish that have spaces in them,  enclose  the  argu‐
       ments in quotation marks. For example: "/some path/with spaces"

[OPTIONS]
       --compress-images, -i
              Losslessly  compress  images in the book, to reduce the filesize, without affecting
              image quality.

       --cover, -c
              Path to a cover image. Changes the cover specified in the e-book. If  no  cover  is
              present, or the cover is not properly identified, inserts a new cover.

       --embed-fonts, -e
              Embed  all  fonts that are referenced in the document and are not already embedded.
              This will scan your computer for the fonts, and if they are found, they will be em‐
              bedded into the document. Please ensure that you have the proper license for embed‐
              ding the fonts used in this document.

       --help, -h
              show this help message and exit

       --jacket, -j
              Insert a "book jacket" page at the start of the book that  contains  all  the  book
              metadata  such  as  title,  tags, authors, series, comments, etc. Any previous book
              jacket will be replaced.

       --opf, -o
              Path to an OPF file. The metadata in the book is updated from the OPF file.

       --remove-jacket
              Remove a previous inserted book jacket page.

       --remove-unused-css, -u
              Remove all unused CSS rules from stylesheets and <style> tags. Some  books  created
              from  production  templates  can  have  a large number of extra CSS rules that dont
              match any actual content. These extra rules can slow  down  readers  that  need  to
              parse them all.

       --smarten-punctuation, -p
              Convert  plain text dashes, ellipsis, quotes, multiple hyphens, etc. into their ty‐
              pographically correct equivalents. Note that the algorithm can  sometimes  generate
              incorrect  results,  especially when single quotes at the start of contractions are
              involved.

       --subset-fonts, -f
              Subsetting fonts means reducing an embedded font to  contain  only  the  characters
              used  from  that  font in the book. This greatly reduces the size of the font files
              (halving the font file sizes is common). For example, if the book uses  a  specific
              font for headers, then subsetting will reduce that font to contain only the charac‐
              ters present in the actual headers in the book. Or if the book embeds the bold  and
              italic  versions  of a font, but bold and italic text is relatively rare, or absent
              altogether, then the bold and italic fonts can either be  reduced  to  only  a  few
              characters or completely removed. The only downside to subsetting fonts is that if,
              at a later date you decide to add more text to your books,  the  newly  added  text
              might not be covered by the subset font.

       --upgrade-book, -U
              Upgrade  the  internal  structures of the book, if possible. For instance, upgrades
              EPUB 2 books to EPUB 3 books.

       --verbose
              Produce more verbose output, useful for debugging.

       --version
              show program's version number and exit

AUTHOR
       Kovid Goyal

COPYRIGHT
       Kovid Goyal

3.48.0                                  September 13, 2019                        EBOOK-POLISH(1)
