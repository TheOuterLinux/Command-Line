#####[ Add text to an animated GIF ]#####

convert '/path/to/animated.gif' -coalesce -gravity North -pointsize 30 -fill white -font Arial -annotate +0+200 'Example Text' '/path/to/output.gif'

- 'gravity' are positions such as North (top), South (bottom), East (left), and West (right).
- 'fill' is font color
- the 'annotate' is the position relative to the set gravity.
