###############################
# ImageMagick - Batch Convert #
###############################

| Some examples
`--------------

Convert all SVG images to PNG in current directory:
---------------------------------------------------

    find . -name \*.svg -print0 | xargs -0 -n1 -P4 -I{} bash -c 'X={}; convert "$X" "${X%.svg}.png"'
    
    *The -P4 flag tells xargs to run for processes at a time
    
OR

    parallel convert {} {.}.png ::: *.svg
    
    *If you have 'parallel' package installed, this will allow you to
     convert an SVG image to PNG for each processor you have
    
Resize all JPG's in the current directory without overwriting:
-------------------------------------------------------------

    for i in *.JPG; do convert -resize 1920x1080 -quality 85 $i `basename $i .png`-resized.png; done
