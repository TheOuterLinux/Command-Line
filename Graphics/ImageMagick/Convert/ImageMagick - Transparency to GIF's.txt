#######################################
# ImageMagick - Transparency of GIF's #
#######################################

The GIF format takes an all or nothing approach to transparency, meaning
that you cannot have a semi-transparent image or color. So to sort of get
around this, you can use:

    convert "input.png" -channel A -ordered-dither o2x2 "output.gif"
