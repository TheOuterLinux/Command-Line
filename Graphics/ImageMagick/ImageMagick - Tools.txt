#######################
# ImageMagick - Tools #
#######################

animate - animate an image sequence on any X server.
compare - mathematically and visually annotate the difference between an 
          image and its reconstruction.
composite - overlap one image over another.
conjure - interpret and execute scripts written in the Magick Scripting 
          Language (MSL).
convert - convert between image formats as well as resize an image, blur, 
          crop, despeckle, dither, draw on, flip, join, re-sample, and 
          much more.
display - display an image or image sequence on any X server.
identify - describe the format and characteristics of one or more image 
           files.
import - save any visible window on an X server and outputs it as an image 
         file. You can capture a single window, the entire screen, or any 
         rectangular portion of the screen.
mogrify - resize an image, blur, crop, despeckle, dither, draw on, flip, 
          join, re-sample, and much more. Mogrify overwrites the original 
          image file, whereas, convert writes to a different image file.
montage - create a composite image by combining several separate images. 
          The images are tiled on the composite image optionally adorned 
          with a border, frame, image name, and more.
stream - a lightweight tool to stream one or more pixel components of the 
         image or portion of the image to your choice of storage formats. 
         It writes the pixel components as they are read from the input 
         image a row at a time making stream desirable when working with 
         large images or when you require raw pixel components.

