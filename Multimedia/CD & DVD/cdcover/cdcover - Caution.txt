#####################
# cdcover - Caution #
#####################

This program is used to create covers for classic jewel cases and not the
cheap thin ones that you commonly find in bulk. This also outputs to a 
.tex file. You will need a converter for this to print it and even then,
most of what I have tried gives me constant errors. However, I decided to
mention it because I haven't found anything else to replace it yet.
