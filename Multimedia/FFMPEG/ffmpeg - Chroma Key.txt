###########################
# ffmpeg - Chroma Key.txt #
###########################

In a nutshell, use as:

ffmpeg -i <base-video> -i <overlay-video> -filter_complex \
'[1:v]colorkey=0x<color>:<similarity>:<blend>[ckout];[0:v][ckout]overlay[out]' \
-map '[out]' <output-file>

where <color> is the rgb color to match in hex (ex: 0x000000 for black), 
<similarity> is a tolerance on the color-match (ex: 0.3), and <blend> (ex: 0.2) 
controls whether the opacity is on-off or how gradual it is.

See https://ffmpeg.org/ffmpeg-filters.html#colorkey
