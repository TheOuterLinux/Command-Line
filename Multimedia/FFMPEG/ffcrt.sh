#!/bin/bash

# FROM THE ORIGINAL SCRIPT:
# -------------------------
# A collection of FFmpeg filterchains which can be used to create a stylised
# 'CRT screen' effect on given input.
#
# The filter-chains have been split apart to increase modularity at the cost of
# sacrificing simplicity and increasing redundant code. Filter-chains can be
# added or removed in various orders, but special attention must be paid to
# selecting the correct termination syntax for each stage.
#
# Includes basic demonstration FFmpeg command which takes "$1" input file.
#
# Version: 2019.04.06_02.49.13
# Source https://oioiiooixiii.blogspot.com
# Page: https://oioiiooixiii.blogspot.com/2019/04/ffmpeg-crt-screen-effect.html
#
# MESSAGE FROM THEOUTERLINUX (https://theouterlinux.gitlab.io):
# -------------------------------------------------------------
# I made some edits to this script so as to be a bit more universal in 
# that it does not resize the video to either NTSC or PAL; its only job
# is to make videos and animated GIFs look like they have been recorded
# from an analog camcorder pointed at a screen. Conversion will probably
# be at around a third of the playback time so a 30 minute video will
# take about 90 minutes to convert, unless you have a really "beefy" 
# computer.
#
# Last updated by TheOuterLinux: 2023/05/07
#
# To use, just do:
#
#    ffcrt.sh /path/to/video.ext
#
#    or
#
#    ffcrt.sh /path/to/video.ext /path/to/output.ext
#
# This script requires 'ffmpeg' and 'coreutils' to be installed.

INPUT="$1"
OUTPUT="$2"

if [ "$INPUT" != '' ]
then
    FILE_EXT="$(echo "$INPUT" | awk -F . '{if (NF>1) {print $NF}}' | tr '[:upper:]' '[:lower:]')"
    WIDTH="$(ffprobe -v error -show_entries stream=width -of default=noprint_wrappers=1 "$INPUT" | tail -n 1 | cut -d '=' -f2)"
    HEIGHT="$(ffprobe -v error -show_entries stream=height -of default=noprint_wrappers=1 "$INPUT" | tail -n 1 | cut -d '=' -f2)"
    BITRATE="$(ffprobe -v error -show_entries stream=bit_rate -of default=noprint_wrappers=1 "$INPUT" | head -n 1 | cut -d '=' -f2)"
    BITRATE=$(printf %.2f "$(( BITRATE * 10/9))")
    #IMPORTANT NOTE: Bitrate is not used in this script by default.
    #Add as '-b:v $BITRATE' to the ffmpeg lines if you want to keep
    #file-sizes down.
fi

### FILTERCHAINS #############################################################

# Reduce input to 25% NTSC resolution
shrink120="scale=-2:120"

# Crop to 4:3 aspect ratio at 25% NTSC resolution
crop43="crop=160:120"

# Create RGB chromatic aberration
rgbFX="split=3[red][green][blue];
      [red] lutrgb=g=0:b=0,
            scale=168x120,
            crop=160:120 [red];
      [green] lutrgb=r=0:b=0,
              scale=164x120,
              crop=160:120 [green];
      [blue] lutrgb=r=0:g=0,
             scale=160x120,
             crop=160:120 [blue];
      [red][blue] blend=all_mode='addition' [rb];
      [rb][green] blend=all_mode='addition',
                  format=gbrp"

# Create YUV chromatic aberration
yuvFX="split=3[y][u][v];
      [y] lutyuv=u=0:v=0,
          scale=192x120,
          crop=160:120 [y];
      [u] lutyuv=v=0:y=0,
          scale=168x120,
          crop=160:120 [u];
      [v] lutyuv=u=0:y=0,
          scale=160x120,
          crop=160:120 [v];
      [y][v] blend=all_mode='lighten' [yv];
      [yv][u] blend=all_mode='lighten'"

# Create edge contour effect
edgeFX="edgedetect=mode=colormix:high=0"

# Add noise to each frame of input
noiseFX="noise=c0s=3:allf=t"

# Add interlaced fields effect to input
interlaceFX="split[a][b];
             [a] curves=darker [a];
             [a][b] blend=all_expr='if(eq(0,mod(Y,2)),A,B)':shortest=1"

# Re-scale input to full NTSC resolution
scale2NTSC="scale=${WIDTH}:${HEIGHT}"

# Re-scale input to full NTSC resolution with linear pixel
scale2NTSCpix="scale=${WIDTH}:${HEIGHT}:flags=neighbor"

# Add magnetic damage effect to input [crt screen]
# This is essentially a red font character blurred "all-to-crap" in the 
# top right corner
screenGauss="[base];
             nullsrc=size=${WIDTH}x${HEIGHT},
                drawtext=
                   fontfile=/usr/share/fonts/truetype/freefont/FreeSerif.ttf:
                   text='@':
                   x=600:
                   y=30:
                   fontsize=170:
                   fontcolor=red@1.0,
             boxblur=80 [gauss];
             [gauss][base] blend=all_mode=screen:shortest=1"

# Add a light reflection to the top left corner or botton right corner 
# of the screen
reflections="[base];
             nullsrc=size=${WIDTH}x${HEIGHT},
             format=gbrp,
             drawtext=
               fontfile=/usr/share/fonts/truetype/freefont/FreeSerif.ttf:
               text='€':
               x=50:
               y=50:
               fontsize=150:
               fontcolor=white,
             drawtext=
               fontfile=/usr/share/fonts/truetype/freefont/FreeSerif.ttf:
               text='J':
               x=600:
               y=460:
               fontsize=120:
               fontcolor=white,
             boxblur=25 [lights];
             [lights][base] blend=all_mode=screen:shortest=1"

# Add more detailed highlight to input [crt screen]
highlight="[base];
             nullsrc=size=${WIDTH}x${HEIGHT},
             format=gbrp,
             drawtext=
               fontfile=/usr/share/fonts/truetype/freefont/FreeSerif.ttf:
               text='¡':
               x=80:
               y=60:
               fontsize=90:
               fontcolor=white,
             boxblur=70 [lights];
             [lights][base] blend=all_mode=screen:shortest=1"

# Curve input to mimic curve of crt screen
curveImage="vignette,
            format=gbrp,
            lenscorrection=k1=0.1:k2=0.1"

# Add bloom effect to input [crt screen]
bloomEffect="split [a][b];
             [b] boxblur=20,
                    format=gbrp [b];
             [b][a] blend=all_mode=screen:shortest=1"

### FFMPEG COMMAND #####################################################

#Original....
#ffmpeg \
   #-i "$1" \
   #-vf "
         #${shrink120},
         #${crop43},
         #${rgbFX},
         #${yuvFX},
         #${noiseFX},
         #${interlaceFX},
         #${scale2NTSC}
         #${screenGauss}
         #${reflections}
         #${highlight},
         #${curveImage},
         #${bloomEffect}
      #" \
   #"${1}__crtTV.mkv"
   
########################################################################


function HELP(){
    echo ''
    echo 'Script: ffcrt.sh'
    echo ''
    echo 'USAGE:'
    echo '    ffcrt.sh "/path/to/video.ext"'
    echo ''
    echo '    Or'
    echo ''
    echo '    ffcrt.sh "/path/to/input.ext" "/path/to/output.ext"'
    echo ''
    echo '    ffcrt.sh "/path/to/input.ext" "/path/to/output.ext" 640 480'
    echo ''
    echo 'DESCRIPTION:'
    echo '    Use this script to easily convert a modern video format'
    echo '    to something that looks like it was recorded with an'
    echo '    analog camera pointed at a CRT monitor, but without the'
    echo '    the annoying moving lines.'
    echo ''
}


function FFMPEG_NOOUTPUT(){
    ffmpeg -y -hide_banner -threads 0 \
    -i "$INPUT" \
    -vf "
    ${noiseFX},
    ${interlaceFX},
    ${scale2NTSC}
    ${screenGauss}
    ${reflections}
    ${highlight},
    ${curveImage},
    ${bloomEffect}
    " \
    -c:v libx265 "${INPUT%%.*}_CRT.mp4"
}

function FFMPEG(){
    ffmpeg -y -hide_banner -threads 0 \
    -i "$INPUT" \
    -vf "
    ${noiseFX},
    ${interlaceFX},
    ${scale2NTSC}
    ${screenGauss}
    ${reflections}
    ${highlight},
    ${curveImage},
    ${bloomEffect}
    " \
    -b:v $BITRATE "$OUTPUT"
}

function FFMPEG_FROMGIF(){
    if [ "$OUTPUT" = '' ]
    then
        ffmpeg -y -hide_banner -threads 0 \
        -i "$INPUT" \
        -vf "
        ${noiseFX},
        ${interlaceFX},
        ${scale2NTSC}
        ${screenGauss}
        ${reflections}
        ${highlight},
        ${curveImage},
        ${bloomEffect}
        " \
        -c:v libx265 "${INPUT%%.*}_CRT.mp4"
    else
        ffmpeg -y -hide_banner -threads 0 \
        -i "$INPUT" \
        -vf "
        ${noiseFX},
        ${interlaceFX},
        ${scale2NTSC}
        ${screenGauss}
        ${reflections}
        ${highlight},
        ${curveImage},
        ${bloomEffect}
        " \
        "$OUTPUT"
    fi
}

#####[ MAIN ]#####

if [ "$1" = '' ] || [ "$1" = '-h' ] || [ "$1" = '--help' ]
then
    HELP
else
    if [ "$INPUT" = "$OUTPUT" ] || [ "$OUTPUT" = '' ]
    then
        if [ "$FILE_EXT" = 'gif' ]; then FFMPEG_FROMGIF; else FFMPEG_NOOUTPUT; fi
    elif [ "$FILE_EXT" = 'gif' ]
    then
        FFMPEG_FROMGIF
    else
       FFMPEG
    fi
fi
