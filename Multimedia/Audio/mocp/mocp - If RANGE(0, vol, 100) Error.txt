######################################
# mocp - If RANGE(0, vol, 100) Error #
######################################

If you see an error while trying to open 'mocp' that contains 'RANGE(0, vol, 100)',
then this means your PulseAudio volume for the device it was going to
use is above 100%, which MOC doesn't know how to handle.

To fix this, try:

    pacmd set-sink-volume 0 65536
    pacmd set-sink-volume 1 65536
    
    *65536 = 100% volume
    *45875 = 70% volume
    
As long as the PulseAudio volumes are at 100% or lower, 'mocp' should now
run just fine.
