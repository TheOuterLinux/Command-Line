This folder contains scripts that aid in the goal of transerring audio
files to VLC's mobile app in order to get away from proprietary
software such as iTunes for adding music.

Scripts:
    * mp32ogg-batch-processor
        1. cd into audio folder containing MP3 files
        2. Run the script
    * ogg2mp3
        1. cd into audio folder containing OGG files
        2. Run the script
    * ytdl-music-processor
        1. cd into desired download directory
        2. Run as "/path/to/ytdl-music-processor" [URL]
    * ytdl-music-processor-URList
        1. cd into desired download directory
        2. Run as "/path/to/ytdl-music-processor-URList" "/path/to/URList.txt"
    * ytdl-music-processor-TitlesList
        1. cd into desired download directory
        2. Run as "/path/to/ytdl-music-processor-TitlesList" "/path/to/TitlesList.txt"
