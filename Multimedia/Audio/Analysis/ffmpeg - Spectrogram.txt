########################
# ffmpeg - Spectrogram #
########################

A spectrogram in this case is a visual representation of the audio file.

    ffmpeg -i audio-in.wav -lavfi showspectrumpic image-out.png
