abc2abc version 1.88 September 25 2016 abc2abc
Usage: abc2abc <filename> [-s] [-n X] [-b] [-r] [-e] [-t X]
       [-u] [-d] [-v] [-V X[,Y,,,]] [-P X[,Y...]] [-ver] [-X n]
  -s for new spacing
  -n X to re-format the abc with a new linebreak every X bars
  -b to remove bar checking
  -r to remove repeat checking
  -e to remove all error reports
  -t X to transpose X semitones
  -nda No double accidentals in guitar chords
  -nokeys No key signature. Use sharps
  -nokeyf No key signature. Use flats
  -u to update notation ([] for chords and () for slurs)
  -usekey n Use key signature sf (sharps/flats)
  -d to notate with doubled note lengths
  -v to notate with halved note lengths
  -V X[,Y...] to output only voices X,Y...
  -P X[,Y...] restricts action to voice X,Y..., leaving other voices intact
  -ver  prints version number and exits
  -X n renumber the all X: fields as n, n+1, ..
  -OCC old chord convention (eg. +CE+)
