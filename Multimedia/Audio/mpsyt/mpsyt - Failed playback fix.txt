#######################
# Failed playback fix #
#######################

If for some reason a song will not play on mpsyt, try:

    set player mpv
    set playerargs --user-agent "Mozilla"
