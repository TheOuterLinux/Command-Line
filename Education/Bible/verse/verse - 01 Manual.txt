verse(1)                             General Commands Manual                             verse(1)

NAME
       verse - Displays a daily devotional verse from scripture

SYNOPSIS
       verse

DESCRIPTION
       Displays  a verse taken from the database in /usr/share/verse/daily.verse that is specific
       for the current date.

INTEGRATION
       If you invoke verse from /etc/profile then the daily devotional verse will appear on  each
       login for each user. Alternatively you could put the invocation of verse into the .profile
       of a user and have each user choose if he wants to use verse or not.

       Note that you will run into trouble if you have dialin users whose scripts call  pppd  and
       put the verse call into /etc/profile. The output might confuse the ppp programs.

AUTHOR (of the verse program not the database)
       Christoph Lameter <clameter@debian.org>

DEBIAN                                     Bible Verse                                   verse(1)
