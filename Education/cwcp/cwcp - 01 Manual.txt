CWCP(1)                              General Commands Manual                              CWCP(1)

NAME
       cwcp - curses-based Morse tutor program

SYNOPSIS
       cwcp  [-s --system=SYSTEM]  [-d --device=DEVICE]  [-w --wpm=WPM] [-t --tone=HZ] [-v --vol‐
       ume=PERCENT] [-g --gap=GAP] [-k --weighting=WEIGHT] [-T --time=TIME]  [-f,  --infile=FILE]
       [-F, --outifile=FILE]  [-h --help] [-V --version]

       cwcp installed on GNU/Linux systems understands both short form and long form command line
       options.  cwcp installed on other operating systems may understand  only  the  short  form
       options.

       There are no mandatory options.

       Options  may  be  predefined  in the environment variable CWCP_OPTIONS.  If defined, these
       options are used first; command line options take precedence.

DESCRIPTION
       cwcp is a curses-based interactive Morse code tutor program.   It  allows  menu  selection
       from  a  number of sending modes, and also permits character sounding options, such as the
       tone pitch, and sending speed, to be varied from the keyboard  using  a  full-screen  user
       interface.

   COMMAND LINE OPTIONS
       cwcp  understands  the  following  command line options.  The long form options may not be
       available in non-LINUX versions.

       -s, --system=SYSTEM
              Specifies the way that cwcp generates tones.  Valid values are: null for no  tones,
              just  timings, console for tones through the console speaker, alsa for tones gener‐
              ated through the system sound card using ALSA sound system, oss for tones generated
              through  system  sound  card using OSS sound system, pulseaudio for tones generated
              through system sound card using PulseAudio sound system, soundcard for tones gener‐
              ated through the system sound card, but without explicit selection of sound system.
              These values can be shortened to 'n', 'c', 'a', 'o', 'p', or 's', respectively. The
              default value is 'pulseaudio'.

       -d, --device=DEVICE
              Specifies  the  device  file to open for generating a sound.  cwcp will use default
              device if none is specified. The default devices are: /dev/console for  sound  pro‐
              duced through console, default for ALSA sound system, /dev/audio for OSS sound sys‐
              tem, a default device for PulseAudio sound system.  See also NOTES ON USING A SOUND
              CARD below.

       -w, --wpm=WPM
              Sets  the  initial  sending speed in words per minute.  The value must be between 4
              and 60.  The default value is 12 WPM.

       -t, --tone=HZ
              Sets the initial sounder pitch in Hz.  This value must be between 0 and  4,000.   A
              value  of  0  selects  silent operation, and can be used for timing checks or other
              testing.  The default value is 800Hz,

       -v, --volume=PERCENT
              Sets the initial sending volume, as a percentage of full scale volume.   The  value
              must  be  between  0 and 100.  The default value is 70 %.  Sound volumes work fully
              for sound card tones, but cwcp cannot control the volume of tones from the  console
              speaker.  In this case, a volume of zero is silent, and all other volume values are
              simply sounded.

       -g, --gap=GAP
              Sets the initial extra gap, in dot lengths, between  characters  (the  'Farnsworth'
              delay).  It must be between 0 and 60.  The default is 0.

       -k, --weighting=WEIGHT
              Sets  the initial weighting, as a percentage of dot lengths.  It must be between 20
              and 80.  The default is 50.

       -T, --time=TIME
              Sets the initial practice time, in minutes.  cwcp  stops  after  generating  random
              Morse code for this period.  The value must be between 1 and 99.  The default is 15
              minutes.

       -f, --infile=FILE
              Specifies a text file that cwcp can read to configure its practice text.  See  CRE‐
              ATING CONFIGURATION FILES below.

       -F, --outfile=FILE
              Specifies a text file to which cwcp should write its current practice text.

       -h, --help
              Prints short help message.

       -V, --version
              Prints information about program's version, authors and license.

   USER INTERFACE
       cwcp  is  a curses-based program that takes over the complete operation of the terminal on
       which it is run.  If colours are available on the  terminal,  it  will  produce  a  colour
       interface.

       The cwcp screen is divided into several distinct areas:

       The Menu Selection window
              The  Menu  Selection window shows the main modes that cwcp offers.  Use the F10 and
              F11 or KEY_DOWN and KEY_UP keys to select the mode.  F9 or  Return  start  sending,
              and F9 again or Esc stop sending.  Changing mode also stops sending.

       The Morse Code Display window
              This window displays each Morse code character after it has been sent.

       The Speed Control window
              The  Speed  window  shows the current Morse code sending speed in words per minute.
              Pressing the F2 or KEY_RIGHT keys increases the speed; pressing the F1 or  KEY_LEFT
              keys decreases the speed.

       The Tone Control window
              This window shows the current Morse code tone pitch.  Use the F4 or KEY_HOME key to
              increase the pitch, and the F3 or KEY_END key to decrease  it.   Values  change  in
              steps of 20Hz.

       The Volume Control window
              This  window  shows  the current Morse code volume.  Use the F6 key to increase the
              volume, and the F5 key to decrease it.  Values change in steps of  1%.   Note  that
              cwcp  cannot  control the volume of the console speaker, so the volume control only
              works effectively for tones generated on the sound card.

       The Gap Control window
              This window shows the current additional 'Farnsworth' gaps  to  be  appended  after
              each  Morse  code  character  is  sounded.   Use  F8 to increase the gap, and F7 to
              decrease it.

       The Time Control window
              This window shows the selected practice time.  After generating  Morse  code  in  a
              particular  mode  for this amount of time, cwcp stops automatically.  Use KEY_NPAGE
              to increase the time, and KEY_PPAGE to decrease it.  During sending, the  value  in
              this window counts down to one, and after final minute of sending has elapsed, cwcp
              stops sending.  The timer operates like a microwave or  kitchen  timer;  it  counts
              down  on  its  own,  but  the time remaining can also be altered manually while the
              timer operates.

       The following keys vary the screen colours:

       { key  Changes the foreground colour of the window boxes.

       } key  Changes the background colour of the window boxes.

       [ key  Changes the foreground colour of the window contents.

       ] key  Changes the background colour of the window contents.

       Eight screen colours are available for each: black, red,  green,  yellow,  blue,  magenta,
       cyan,  and  white.   Use a key to cycle round these colours for the particular part of the
       display controlled by that key.  On a change of colours, the complete screen is repainted.

       Use Ctrl-L to repaint the complete screen, in case of screen corruption.   Use  Ctrl-V  to
       clear  the  Morse  Code  Display  Window.  This command is available only when cwcp is not
       sending.

       To leave cwcp, press F12 or Ctrl-C, or select Exit on the mode menu.

       All of the above command keys may be used while random characters are being sent, and when
       keyboard input is being sent.

       If  function  keys are not available on the terminal, Ctrl-<key> combinations can be used.
       On the top row of letter keys on the keyboard, the keys Ctrl-Q to Ctrl-I may  be  used  as
       alternatives  for  F1  to  F8,  and  on the second row of letter keys, Ctrl-A to Ctrl-F as
       alternatives for F9 to F12.  For KEY_PPAGE and KEY_NPAGE, use Ctrl-O and Ctrl-P.

   RANDOM CHARACTERS AND WORDS
       cwcp sends random characters in groups of five, with a space between each group.  After  a
       period  of sending, set in the Time Control window, cwcp stops automatically.  It can also
       be stopped manually, before this time period expires.

       When sending random words, cwcp sends the complete word, followed  by  a  space.   Because
       short  words  are  easier to copy without writing, cwcp's default dictionary contains only
       three, four, and five-letter words in its random words list.

       cwcp chooses at random from a list of around 3000 words in its  default  dictionary.   You
       can change this text using a configuration file, read at startup.  See CREATING CONFIGURA‐
       TION FILES below.

   NOTES ON USING A SOUND CARD
       By default, cw tries to open default PulseAudio. If PulseAudio server is  not  accessible,
       cw  tries to open OSS device "/dev/audio" to access the system sound card.  This is gener‐
       ally the correct device to use, but for systems with special requirements, or  those  with
       multiple  sound cards, the option -d or --device, combined with -s or --system can be used
       to specify the device and audio system for sound card access.  If the  sound  card  device
       cannot be set up, cwcp prints the error message

              cannot set up soundcard sound

       and exits.

       Sound  card  devices,  when  opened  through  OSS  sound system, are usually single-access
       devices, so that when one process has opened the device,  other  processes  are  prevented
       from  using  it.  In  such cases cwcp will of course conflict with any other programs that
       expect exclusive use of the system sound card (for example, MP3 players).  If  cwcp  finds
       that the sound card is already busy, it prints the error message

              open /dev/audio: Device or resource busy

       and exits.

       The sound card device is not used if cwcp is only sending tones on the console speaker.

   AUDIO OUTPUT - DEFAULTS AND SELECTION
       cwcp  first tries to access sound card using PulseAudio sound system, using default device
       name, unless user specifies other audio device with option -d or --device.

       cwcp then tries to access sound card using OSS audio system and default OSS  audio  device
       name ('/dev/audio'), unless user specifies other audio device with option -d or --device.

       If  opening  soundcard  through  OSS fails, cwcp tries to access the sound card using ALSA
       audio system, and default ALSA audio device name ('default'), unless user specifies  other
       audio device with option -d or --device.

       If  opening  soundcard through ALSA also fails, cwcp tries to access system console buzzer
       using default buzzer device '/dev/console', unless user specifies other audio device  with
       option -d or --device.

       It  is very common that in order to access the console buzzer device user has to have root
       privileges.  For that reason trying to open console buzzer almost always fails.   This  is
       not  a program's bug, this is a result of operating system's restrictions.  Making cwcp an
       suid binary bypasses this restriction.  The program does not fork() or exec(),  so  making
       it  suid  should  be  relatively safe.  Note however that this practice is discouraged for
       security reasons.

       As stated, user can tell cwcp which device to use, using -d  or  --device  option.   Which
       device  files  are suitable will depend on which operating system is running, which system
       user ID runs cwcp, and which user groups user belongs to.

   CREATING CONFIGURATION FILES
       cwcp contains a default set of modes and practice text that  should  be  enough  to  begin
       with.  It can however read in a file at startup that reconfigures these to provide differ‐
       ent character groupings, word sets, and other practice data.

       To read a configuration file, use the -f or  --infile  command  line  options.   The  file
       should  introduce each cwcp mode with a section header in '[' ... ']' characters, followed
       by the practice text for that mode, with elements separated by whitespace.  Lines starting
       with a semicolon or hash are treated as comments.  For example

              ; Simple example mode
              [ A to Z ]
              A B C D E F G H I J K L M N O P Q R S T U V W X Y Z

       cwcp  will  generate five character groups for modes whose elements are all single charac‐
       ters, and treat other modes as having elements that are complete  words.   As  a  starting
       point  for  customized modes, cwcp will write its default configuration to a file if given
       the undocumented -# option, for example "cwcp -# /tmp/cwcp.ini".

NOTES
       cwcp owes its existence to the DOS Morse code tutor  CP222C.EXE,  by  VU2ZAP.   CP222C.EXE
       seemed  to  offer the right range of facilities for learning Morse code in a simple, easy-
       to-use, and compact package.  cwcp is very much modeled on that DOS  program,  and  should
       prove  useful  both for learning the code, and for experienced CW users who want, or need,
       to improve their receiving speed.

       Curses may impose a delay when recognizing the Esc key alone, as this character  is  often
       the first of a sequence generated by a function key.  cwcp responds instantly to F9.

       The  characters  echoed  in  the Morse Code Display window may be ASCII representations of
       Morse procedural signals; see the cw(7,LOCAL) man page for details.

   HINTS ON LEARNING MORSE CODE
       Here are a few hints and tips that may help with the process of learning Morse code.

       Firstly, do NOT think of the elements as dots and dashes.  Instead, think of them as  dits
       and dahs (so 'A' is di-dah).  If you think of them in this way, the process of translating
       sound into characters will be learned much more easily.

       Do not learn the characters from a table.  Learn them by watching the groups appear on the
       screen, and listening to the sounds produced as each is sent.  In the very initial stages,
       it may be beneficial if you can find a person to take you through the first stages of rec‐
       ognizing characters.

       Do  not  waste your time learning Morse code at 5 WPM.  Set the speed to 12 or 15 WPM, but
       use extra spacing (the Gap window) to reduce the effective speed to much  lower  -  around
       four  or  five WPM effective speed.  This way, you will learn the rhythm of the characters
       as they are sent, but still have plenty of time  between  characters.   As  you  practice,
       decrease the gap to zero.

       Learn  in  stages.  Start by learning the EISH5 group, then progress down through the menu
       as each group is mastered.  The groups contain characters which are in some  way  related,
       either by sound, or by type of character.

       Once  you  have  completed all the groups EISH5 to "'$(+:_ (or 23789 if you do not want to
       learn procedural signals yet), use the full character set options, and the  words  and  CW
       words  options,  to  sharpen your skill.  If you have difficulties with particular charac‐
       ters, return to that group and practice again with a smaller character set.

       Resist the temptation to try to learn or improve your speed by copying off-air.  You  will
       not  know what speed you are working at, and much hand-sent Morse is not perfectly formed.
       What you can gain off-air though is a general 'resilience', a  tolerance  for  Morse  code
       where  the  timing of individual elements, or spacing between characters and words, is not
       100% accurate.

       If working to attain a particular speed for a test, always set the speed slightly  higher.
       For  example,  if  aiming for 12 WPM, set the tutor speed to 14 or 15 WPM.  This way, when
       you drop back to 12 WPM you will feel much more relaxed about copying.  Be aware that cwcp
       is  not necessarily going to send at exactly the speed you set, due to limitations in what
       can be done with UNIX timers.  It often sends at a slower speed than you set, so  be  very
       careful with this if you have a target speed that you need to reach.

       Use  the  program to make cassette tapes that you can take with you in a walkman or in the
       car, for long journeys.  You do not have to write down everything  you  hear  to  practice
       Morse code.  Simply listening to the shapes of characters over a period will help to train
       your brain into effortless  recognition.   In  fact,  slavishly  writing  everything  down
       becomes  a  barrier  at speeds of 15-20 WPM and above, so if you can begin to copy without
       writing each character down, you will find progress much easier above these  speeds.   But
       do  not  over-use  these  tapes, otherwise you will quickly memorize them.  Re-record them
       with new contents at very regular intervals.

       Try to spend at least 15-30 minutes each day practicing.  Much less than  this  will  make
       progress glacially slow.  But significantly more than an hour or so may just result in you
       becoming tired, but not improving.  Recognize when it is time to stop for the day.

       Do not worry if you reach a speed 'plateau'.  This is common, and you will  soon  pass  it
       with a little perseverance.

       At  higher  speeds, CW operators tend to recognize the 'shape' of whole words, rather than
       the individual characters within the words.  The CW words menu option can be used to  help
       to practice and develop this skill.

ERRORS AND OMISSIONS
       The calibration option is a bit ropy.  It simply sends PARIS repeatedly, and relies on you
       to time the sending and then work out if any adjustment to the speed is really  necessary.
       Automatic calibration by making measurements over a given period would be a lot better.

       Generating random CW QSOs would be fun to do.

       A  facility  to  allow  a user to key Morse into the system, and to have it send code, and
       measure the accuracy of keying, would be nice.

SEE ALSO
       Man   pages   for   cw(7,LOCAL),   libcw(3,LOCAL),   cw(1,LOCAL),   cwgen(1,LOCAL),    and
       xcwcp(1,LOCAL).

cwcp ver. 3.5.0                          CW Tutor Package                                 CWCP(1)
