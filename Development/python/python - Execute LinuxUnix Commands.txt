########################################
# python - Execute Linux/UNIX Commands #
########################################

Basic usage:

    import os
    os.system("command")
    
Print the output of 'date' command:

    import os
    os.system("date")
    
    
Store date output as a variable and print:

    import os
    f = os.popen('date')
    now = f.read()
    print "Today is ", now
    
------------------------------------------------------------------------
    
Using subprocesses instead:

    import subprocess
    subprocess.call("command1")
    subprocess.call(["command1", "arg1", "arg2"])

Print the output of 'date' command:

    import subprocess
    subprocess.call("date")
    
Store date output as a variable and print:

    import subprocess
    p = subprocess.Popen("date", stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    print "Today is", output
    
Realtime output of PING:

    import subprocess
    cmdping = "ping -c4 www.duckduckgo.com"
    p = subprocess.Popen(cmdping, shell=True, stderr=subprocess.PIPE)
    while True:
        out = p.stderr.read(1)
        if out == '' and p.poll() != None:
            break
        if out != '':
            sys.stdout.write(out)
            sys.stdout.flush()
