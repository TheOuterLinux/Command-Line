#####[PyPy - Portable Python]#####

1. Grab a copy of portable Python from https://www.pypy.org/download.html
2. Extract .tar.bz2
3. cd /path/to/portablePython
4. ./bin/pypy -m ensurepip
5. ./bin/pypy -m pip install numpy

And then of course you can run it just like you would normal Python and
modules are installed to its directory for portability.
