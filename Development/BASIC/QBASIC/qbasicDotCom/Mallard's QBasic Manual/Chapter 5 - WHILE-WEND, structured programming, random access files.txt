
                        Basic Basic -  chapter V
                    by David Zohrob (dr.qbasic@qbasic.com)
                            http://qbasic.com

��������������������������������������������������������������������������������
                              [author's note]

Wow!  Almost two months between tutorials TO THE DAY.  Anyway, I'm going to
cover a LOT of ground in this tutorial, so PLEASE pay attention.  Got your
#2 pencil in hand?  Okay.  Mark the ovals with a DARK, COMPLETE circle...

-Dave Zohrob
7/7/1997

��������������������������������������������������������������������������������

        Welcome!  In this chapter, we will cover the following commands
    and topics:  (finally!  I haven't changed that sentence since chapter 2!)

  � WHILE...WEND                        �  Random Access Files
  � APPEND                              �  GET (file I/O)
  � SUB...END SUB                       �  PUT (file I/O)
  � FUNCTION...END FUNCTION             �  LEN
  � GOSUB...RETURN                      �  TYPE...END TYPE
  � DIM SHARED                          �  COMMON SHARED
  � RTRIM$                             

        Sorry about the delay between tutorials, but I'm going to write one
  RIGHT NOW just for you, a QB-hungry programmer.  I'll also write another
  one this week because of my guilty conscience in not providing enough fodder
  for you guys to learn.  Enough of the "Dave's Thoughts," though, let's get
  right to it.
        Let's get right to the meat of things.  Don't be scared by the amount
  of stuff listed up there, there's a lot of important information here and
  it's not that hard to learn.  We'll cover another method of looping, ways
  to add on to the end of sequential files, structured programming, and
  "random-access" files.  That's a lot of material.
        First off, the last of the looping techniques.  We've already tried
  DO...LOOP and FOR...NEXT, and here comes WHILE...WEND.  It's very similar
  to a DO...UNTIL loop, and I rarely use it in my programs, but you might
  as well learn everything you can.  Its basic syntax is as follows:

  WHILE <condition>
  ...
  WEND

        That's it.  Let's say you want to make a simple guessing game.  All
  it takes is a simple loop, as in the next example.

  RANDOMIZE TIMER
  PRINT "Guessing game!"
  number = INT(RND * 10) + 1                    'random number from 1 to 10
  PRINT "The number is from 1 to 10... go!"
  WHILE guess <> 10                             'loop until the guess = 10
  INPUT "Guess"; guess
  tries = tries + 1                             'count the number of tries
  WEND                                          
  PRINT "Good job!  You got it in"; tries; " tries!"
  END

        There's a working, somewhat interesting program in only 10 lines!
  We're finally getting somewhere.  That's all there is to a WHILE...WEND
  loop.  So simple, and yet so useful!

        We talked about sequential files in Chapter 4.  Those were the files
  that you could only INPUT from or OUTPUT to at one time.  There's one more
  way to add to these files, and that is done by using the APPEND command.
  You use it just like OUTPUT and INPUT:

  OPEN "file.txt" FOR APPEND AS #1

        APPEND is just like OUTPUT, except it doesn't erase what's already
  in the file before it adds to it.  It's useful for ongoing high score lists
  and a bunch of other things that I'm sure your creative mind can think of.
  Again, not much to it.  Just fool around until you get the hang of it.

        Moving on, we'll now discuss structured programming.  Programs are
  huge jumbles of thousands of lines of code, and if everything was just
  arranged from top to bottom, it would take forever to find a problem in
  the program!  Plus, it just looks messy and is hard learn from.  So,
  the geniuses of the 1960s developed something called "structured program-
  ming."  Using this technique, we can create "subprograms" and "functions"
  in our programs to better arrange our code.
        In order to create and edit a subprogram in QBasic, all you have to
  do is go to the [E]dit menu on the menu toolbar and choose New [S]UB...
  QBasic will then prompt you for its name.  Don't get too creative and call
  it "Bob" or "Jane," give it a name like "DoGraphics" so you can tell what
  the SUB really does.
        QBasic will then let you edit the SUB.  Program just as you would
  normally, and when you're done, go to [V]iew on the toolbar, choose [S]UBs
  and then pick your program's filename off of the menu.  If you want to go
  back and edit your SUB, go to the [V]iew menu, choose [S]UBs and pick your
  SUB off the list.  You can have as many subroutines as you want in a pro-
  gram.  Be aware, though, that variables you create in a SUB only are
  accessible in the SUB and variables in the main body of the program are
  only accessible from the main body of the program unless you make them
  SHARED using the following command:

  COMMON SHARED variable$, variable2, variable3!

        This statement goes after your "DECLARE SUB..." declarations (QBasic
  places these statements in your program once you save it) and before any
  other executable lines of your program.  To make an array SHARED, you just
  stick the word SHARED in after the DIM command, like this:

  DIM SHARED array$(100)

        Make sure that any variables you want to be SHARED (called "global"
  variables) are in your COMMON SHARED statement.  Otherwise, your program
  will turn up a lot of errors.  To pass variables on to the SUB as you call
  it from the main program, you must change the name of the SUB.  While you
  are editing it, just use a line like this:

  SUB DoBox (x1, y1, x2, y2)

        QBasic sticks the "SUB DoBox" in there for you.  Now, when you call
  the SUB from the main program, use a line like this:

  DoBox 30, 20, 50, 20

        Otherwise, you can just type the name of the SUB in the main program
  to call it up, like this:

  mainloop: CLS
  DoGraphics
  DoLevel
  WaitForKey

        As a complement to SUBs, those programming gurus of the '60s also
  developed FUNCTIONS.  These are similar to SUBs, but they basically just
  perfom a specific calculation to return a number.  You create them the
  same way you would create a SUB, by going to the [E]dit menu, but choose
  New [F]UNCTION.  You can switch what you're editing the same way.  Here's
  a short example:

  FUNCTION Cube(num)
        Cube = num * num * num
  END FUNCTION

  [main program:]

  CLS
  INPUT "Number"; number
  num3 = Cube(number)
  PRINT number; "cubed ="; num3
  END

        The last topic I'll cover in this tutorial is random-access files.
  These files are kind of like database files - they have a set record type
  and a number of records.  They're very useful for database applications.
  To make the record, you have to define it near the top of your program
  with a TYPE...END TYPE clause.  Here's a short example for an address
  program:

  TYPE people
        nm AS STRING * 40            'set the name as a 40-character string
        age AS INTEGER               'set the age as an integer
        address AS STRING * 60       'set address as a 60-character string
  END TYPE

        The next thing you have to do before you open the file is use a
  DIM command to set a random-access array up for the TYPE, as follows:

  DIM person AS people

        Now, you must open the file.  You again use the OPEN command, but
  now you have to add in two commands: LEN and RANDOM.  Here's an example:

  OPEN "address.dat" FOR RANDOM AS #1 LEN = LEN(person)

        This will open up your random-access file of addresses with the
  record length that you set in your TYPE.  Now, in order to get records
  or put records to a file, you have to use special variable names and
  different file commands.  Here's a quick example of adding a record to
  the file we created above:

  INPUT "What record to add"; record
  INPUT "Name"; person.nm
  INPUT "Age"; person.age
  INPUT "Address"; person.address
  PUT 1, record, person

        As you can see you have to have your DIMmed array name, then a
  period, then the variable name you set in your TYPE.  Then, you have to
  PUT the record to the file using the PUT command (how appropriate).  The
  syntax for the PUT command is like this:

  PUT [filename], [recordnumber], [arrayname]

        It's very simple.  To get an array from the file, you use basically
  the same method, except with the GET command. The GET command has exactly
  the same syntax as the PUT command, except it reads from the file into
  the array that you specify.  Here's an example from the program we have
  above.

  INPUT "View which record"; record
  GET 1, record, person
  PRINT "Name"; person.nm
  PRINT "Age"; person.age
  PRINT "Address"; person.address

        That's how you use random-access files.  An example is available,
  called RANDOM.BAS.  If you didn't download it with the .ZIP file, please
  download it from http://www.qbasic.com/random.bas.  Random-access files
  are very useful for a lot of applications, but they're also very complex.
  One last thing before I conclude - when an input doesn't fill up all of
  the STRING you specified in the array (like in the above example: if
  the person's name isn't 40 characters) the variable WILL have all the
  extra spaces on the end to fill up the 40 characters.  To remedy this,
  use the RTRIM$ command, as follows:

  personname$ = RTRIM$(person.nm)

        This will trim all the extra spaces off of the end of the string.

        And that's it for this tutorial.  There's a heck of a lot of stuff
  here, and if it's confusing, please ask me questions.  They might take a
  long time to answer, but I need to know how to improve my tutorials so
  they help YOU the most.

  Exercise!

        1.  Create a simple database program for addresses and names
            using structured programming.


��������������������������������������������������������������������������������

Keep programming!
-David Zohrob
