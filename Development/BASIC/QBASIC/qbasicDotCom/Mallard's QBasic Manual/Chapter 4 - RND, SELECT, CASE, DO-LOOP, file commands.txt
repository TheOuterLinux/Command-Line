
                        Basic Basic -  chapter IV
                    by David Zohrob (mallard@gcomm.com)
                            http://qbasic.com

��������������������������������������������������������������������������������
                              author's note

Okay, so here's the next chapter.  Sorry it takes a while between chapters
(like two months), but you've all probably heard my stories of the U of D
musical, so..  just enjoy, learn a bit, and give me an e-mail with what you
think about my next installment.

-Dave Zohrob
5/8/1997

��������������������������������������������������������������������������������

        Welcome!  In this chapter, we will cover the following commands:

  � DO...LOOP                           �  OPEN
  � INT                                 �  CLOSE
  � RANDOMIZE TIMER                     �  RND                                 �  PRINT #
  � SELECT...END SELECT                 �  INPUT #
  � INKEY$

        So you haven't read a tutorial in two months or so.. and now, I'm
  going to satisfy your programming appetite with some juicy commands to
  spice up your programs.  The first command we'll discuss here is the
  DO...LOOP type of loop.  In the previous chapter, we talked about the
  FOR...NEXT loop, which let you do something a certain number of times.
  This is great if you know EXACTLY how many times you want something to
  be done.  But what if you don't?  What if you want something to be done
  until a condition is filled?  This is exactly the void the DO...LOOP
  loop fills.
        All you have to do is say DO WHILE... or UNTIL.. a condition is
  met, such as "x = 10".  Then put the commands you want to be in the loop,
  and have a LOOP command to close it all up.  Here's a very simple example
  of how to use the DO loop.

  DO UNTIL a = 10
  try = try + 1
  PRINT "Try number"; try
  PRINT
  PRINT "Guess the super-secret number!"
  INPUT "Well"; a
  LOOP   

        Of course, there's at least two ways to do everything in programming,
  so instead of DO UNTIL a = 10 as the first line, you could use the line
  DO WHILE a <> 10 and it would do the same thing.  That's it!  The loop
  is very simple, but infinitely useful.
        One such use of the DO loop is to have a "Press Any Key" prompt that
  new programmers are so eager to stick in their programs, but usually have
  to settle for "Please press enter... <sigh>".  This is accomplished with
  the nifty function INKEY$, which is the IMMEDIATE, CURRENT key being
  pressed on the keyboard at any given moment.  To make a "Press a key"
  prompt, you can cut and paste this next little bit into your program.

  DO WHILE INKEY$ = ""
  LOOP

        And that's it!  Holy posable action figures, Batman!(tm)  INKEY$ also
  has tons of uses in your program, and one of its main uses is to have
  a "hot-key" menu instead of an INPUT menu.  However, this requires a more
  complex construction similar to an IF...THEN construction.  This new
  set of commands is the SELECT CASE...END SELECT group, and lets you set
  a bunch of different IF...THEN like statements into one big grouping in
  your program instead of having hundreds of seperate statements.  It also
  allows you to have multiple commands per CASE of a variable.
        The use is pretty simple, as shown in this example:

  PRINT "The Main Menu"
  PRINT "1) End the program"
  PRINT "2) Surprise"
  PRINT
  INPUT "Choice"; chc
  SELECT CASE chc
        CASE 1
        PRINT "Fine, then!"
        END
        CASE 2
        PRINT "Surprise! AAAH!!"
        PRINT "Are you surprised?  No?  Oh well..."
        END
        CASE ELSE                                  'note this little
        PRINT "Why didn't you pick 1 or 2?"        'command which lets you
        END                                        'trap invalid answers
  END SELECT

        All you need to do is have a SELECT CASE variable statement, then
  give all the seperate CASEs as you would an IF statement.  When you're all
  done, be SURE to stick an END SELECT in there or else your program won't
  run.
        One other nifty programming trick I stuck in there was the CASE ELSE
  command.  This checks to see that the variable does equal one of your
  CASEs, and if it doesn't, executes the commands under its CASE.  And that's
  the basics of the SELECT CASE..END SELECT group of statements, which you
  will probably see a lot in programs written by myself just because they're
  very useful commands.
        Another very requested item is how to do random numbers in QBasic
  for dice rolls or whatever you might need it for.  There's a very simple
  way to do this, involving the LET, INT, and RND commands.  Here's the syn-
  tax of it:

        x = INT(RND * 10) + 1

        This would give you a number between 1 and 10.  To get a number
  between 0 and 10, jus get rid of the "+ 1" portion of the line.  That's
  all you have to do for random numbers - just change the RND number to get
  your upper bound.
        Now, on to the file commands.  This really should be covered in a
  different chapter, but due to the overwhelming demand for a files tutorial,
  here's the basics of how to use files in QBasic.  The kind of files we'll
  be dealing with are called "sequential files."  There's not much to that
  complex term (why use a big word when a dimunitive one will do?).  All it
  really means is that we write to the file one line at a time, from top to
  bottom.
        You need to use the OPEN command to open a file and the CLOSE command
  to close it (wow! try and follow that logic!).  Be sure to CLOSE every file
  that you open before your program terminates so that your data is saved.
  To open a file, you have to use the command in this way:

        OPEN "filename.ext" FOR [OUTPUT/INPUT/APPEND] AS #1 [or other number]

        The OPEN command is far more complex than this, but this is as far
  as I'll go for now.  The [OUTPUT/INPUT/APPEND] portion changes, depending
  on what you want to do with the file you've opened.  If you want to read
  from the file, line by line, you have to use the INPUT command.  To write
  to the file, overwriting whatever is already there, you have to use the
  OUTPUT command.  And to append to the file (add on to the end), you use
  the APPEND command. See how all that works out?
        So here we are.  We've opened our file.  Now what do we do?  I know
  you still remember the PRINT and INPUT commands (how could you not?), so
  depending on whether you're INPUTting from or OUTPUTting to, there are
  two things to do.  You still use the PRINT command to print and the INPUT
  command to read from the file, but now you stick a file number (like #1)
  in front of what you want to PRINT or INPUT.  For example:

  OPEN "file.txt" FOR OUTPUT AS #1
  PRINT #1, "Hello world!"
  CLOSE
  
  OPEN "file.txt" FOR INPUT AS #1
  INPUT #1, s$
  PRINT s$
  CLOSE

	And that's how you use files.  Pretty simple for sequential files,
  huh?  Good.  You're going to need to fool around with these commands a 
  LOT before you get the hang of it.  There's one more thing to say about
  files, though... the INPUT # statement only reads up to a comma on a
  line.  So, you must use LINE INPUT to read an entire line.  For example:

  OPEN "file.txt" FOR INPUT AS #1
  LINE INPUT #1, s$
  PRINT s$
  CLOSE

	This would read the entire line into the variable "s$".  Just re-
  member, when using the OUTPUT command, it ERASES what is in the file al-
  ready.  Be sure not to get rid of anything important!  

	So that's the fourth installment in my little series here.  I hope
  you learned a lot, and don't worry, it won't be that long before I put 
  out a fifth chapter.  So keep programming, and try these exercises:

  *exercises*

  1.  Make a guessing game that starts with a random number between 1 and 
  20.  Give the player 5 guesses, and after each guess tell them whether
  they are too high or too low.

  2.  Use the file commands a LOT.  And I mean a LOT.  If you're making a
  game, try to create a high score list or a "save game" feature.


  3.  Keep at it!
	
��������������������������������������������������������������������������������

Have a wonderful day.
Peace!  -David Zohrob
