                     Vic's QBasic Programming Tutorial

                           Basic Tutorial XXII!!

                            Programming Apathy...

           Hmm...  Wonder what that means... eh...  Who cares...

-----------------------------------------------------------------------------

I will now hope to cover the ametuer programming killer...

Yes, thats right...
You, are an ametuer programmer...
I, am an ametuer programmer...

You don't get paid to write a pong clone in qb, do you?

---

So, you read all the tutors...

And you are now going to embark on a brand spaking new game...
One, that you have envisioned in your brain a thousand times...

You start, you might even get as far as drawing a bunch of sprites for
the game even...

But, suddenly...
coolgame.bas goes into your crap folder...

You know the folder...

Its like an attic...
You keep it there, just in case you may want to work on it in the future...

But, you know deep down inside, it might as well go into the recycle bin...

Its not that you can't program...
Its not that you have run out of ideas...

It is, that thinking of programming for that game, would be like scraping
your brain with a 20 pronged tazer...

or something like that...

It's called Apathy...

But wait,
I'm not apathetic!
I do my homework, and get it in on time!!!
I even know what apathetic means!

I make sure to do the dishes, and clean my room!

But, ask yourself...

do you enjoy it?

99% of people would say no...
(but you might, I don't know...)

You learned to program for fun...
That makes you an ametuer...

You may be a proffesional dish cleaner or room cleaner...
But do you get paid to program?

---


What exactly is it that makes you quit in the middle...

Its very simple...

What makes you turn in your homework on time... (or close)

You would fail right?

What would happen if you didn't finish your Pong clone...

...

nothing right?
Your crap folder would hold one more file, making the "Atic" drive
one file bigger...

But to tell you the truth, I NEVER go into my Crap folder...
But, I can't delete it...


Let me once again share with you an e-mail that someone wrote to me...
And, once again, nothing has been changed...
(this time no foul words were used...)
      (it was big suprise!)

(But, this time, I asked for permission...)


From: ---(address withheld)

Subject:  RE: RE: Need help in qb 45!

Message:

> Run the command qb /s
> this loads the support for the command to get the mouse working
> (and, don't forget to read the tutor on mouse control)
> (its in there too)

Thanx...
I guess I missed that part...

And, I have another query...

How come I havn't seen any of your games lately on the internet?
You only have one file on your page, and it looks really cool!
You have to have more games or appz somewhere out there!
With all the tutorz you have, you should be making some pretty cool games...
It allmost seems that you are taking other peoples tutorz and just
putting your name on them!  Are you even writing tutorz anymore?


end...


---


It was an inoccent question, but it was like a punch in my face...
(or having your brain scraped by a 20 pronged tazer...)

e' hem...

But, I realized she was right...
I have learned so many programming techniques, but havn't done much with
them...

The most use they ever got, was writing these tutorz...  I mean tutors...
Ha ha! get it?  He said tutorz in his e-mail and-
and-
and- um-

e' hem...


But, here...

Let me go to my crap folder...
(mines located here...)

c:\windows\profiles\radiohands\desktop\Crap

(nice name huh...)
(get it?)
(my crap folders name is ''' sorry...  I won't do that again...)

Now, I will count all the files in my crap folder...

Ok, Im in the folder...

Now,,,
I will click -

EDIT
  Then
Select All

Now, lets see...

at the bottom left-hand corner of the screen, it says...

"166 object(s) selected"

166!!!

no lie!



------------------------------------------------

Ok, enough talk about the problem...

Lets get to the solution...

Thats right! solution!

And, as soon as you find it out, E-mail me!

So I lied, there is no REAL solution...

But, I will list many of the reasons games do get finished, and
how you can Help stop programming Apathy...



---
-------------------


What causes programming apathy?

Commitment...

that is the #1 cause...
(Duh...)

When you go to work, you know what is expected of you...
and, you get paid to do it...
So, you are more likely to do it right?
(well, most of you...)



----

To help illistrate this for you, I will tell you about a company that
we all grew up with...

A company that creates games...

ID SOFTWARE...

GET IT!!  ID?  LIKE IDEA??!! HA!!!
what?
thats not what the ID is for?
o' well, its not important...


When you first ran WolfenStien, and quit, or finished the game, what
did you see?

Credits right?

(I can't believe that the only reward you get for finishing a game is 
a bunch of credits!!!)

How many credits?

A TON!!!

And, you are at home creating your Wolfenstien clone by yourself!?

Why do they have so many credits?

Because, they are a team...

Wich is more fun, Playing tennis against a wall, or playing tennis against
a friend? (or enemy)

Wich one are you most likely to do longer?

...

Here is another example having to do with another game creater...

3D realms...
remember them?

Duke nukem 3d...

(by the way)
where the he** is Duke Nukem Forever?

It was supposed to be out in what... 1997?
What the he## is taking the som Da## long?!

ha!  notice how I put the two ## next to the 4 letter words...
he** could be HECK...  Da## could be DARN...
HA! get it! ha! ha! h' h' sorry...
eh.. no I'm not...

where was I?
oh yeah... Duke nukem 3d...

The original Build engine (build.exe) was programmed in QB!!!
Would you believe it?

It was built by Ken Silverman...

Only one guy...

But, it was far away from being Duke Nukem...

Apathy probly hit the engine late, and thats when it was picked up
by 3d realms...

And, with the team, Ken and 3dRealms, they Spanked the first person
shooter world...
(its still fun to me!)
(its also fun to say Spanked...)
(Try it...)
(yes, both of them...)
(if you still can't afford Duke nukem 3d, just say Spanked...)
{~Spanked~}
(sweeeeeeeet...)


e' hem...

Although, Ken doesn't program games for money anymore...


---

Hey, and you want to know some more crappy info?
sure you do!

APOGEE

(sounds like Apathy...)

Have you heard of that company...

Yeah...
They wrote really old games...

But, they wrote many of thier earlier games in QBasic...

but, what do you see at the end of thier games?

Credits...

A motherload of credits...

People got together in QB?
I thought that takes a bigger language, like C++!

Nope...

You can do it too...

Friends don't like to program?

Do a trick that the guys at NASA do...

Split up your personalities, and do each job thinking its your only job...
They split up thier minds, and they're good at it too...

It helps in doing every important job good...
If some of the most important jobs at a place like NASA get done
half/Arsed...  What would happen?
Your next job would be cleaning the ship's debris off the launchpad...

If you work as a person that changes tires...

Think if your only job was to change one tire on one car, wouldn't you
enjoy work?

---

for example (in game programming)...

Start working on nothing but the engine...
Then stop working on the engine...

After the engine is done, start working on the graphics...

After graphics, work on sounds or whatever...

Forget about the last job you did,,,
Forget about the jobs you need to do...

The people at NASA meditate...
you can try that...
Although, I don't think I will cover that method here...

go back to your crap folder, and grab an old forgotton file...

Change it...

Most likely, you forgot what most of the lines of code are for...

and thats good!

Its a new project!



----------


Don't like the first idea for game programming?

Well...
There are a few more methods of getting the job done...

One very important thing to do, is Project Planning...

Speeks for itself doesn't it...

Before you open QB or VB or VC++, Write everything down on paper...

thats right...
EVERYTHING...

That way, when you get programming, the tedious thought proccessesing is
over, and you basically have to copy what is down on paper, and just make
sure it works...

This really helps in code organization...

Did you forget what part of your code covers sprite movement?
Well, go through your notes...

If the sprite movement isn't going right, take out a pen and do the old
version of cut and paste (ie. Erase/cross out and rewrite)

Draw all of your graphics on paper...
And, while creating the engine, don't keep graphics in mind...
This is a big apathy creator...

Graphics force your engine to work with those graphics only...

A good thing to do is to Write the engine, THEN draw graphics and sounds to
fit your engine...

If your engine supports 3 inch sprites, and you have allready got 12 inch
sprites, you would most likely change the engine right?

DON'T!!!

The work on your engine should be over!
erase the extra eyballs on your alien sprite, and make him fit...

Does your car sprite take up 3 lanes of trafic?

Change the game into a motorcycle game, or shrink your car...
Use paint to shrink down the size of your sprite, and copy it into QB...

If you were working on a game like that, and your boss wrote the game
engine, and you were in charge of graphics, would you complain to the boss
that the engine sucks?!  He would fire you, and get someone that knows what
they are doing...

KEEP TO YOUR NOTES!

IF you screwed up on the notes big time, and it doesn't work in the real
programming world, Rip up the old notes, and build new notes...
DO NOT CHANGE THE CODE IF YOU HAVN'T WRITTEN THE NEW CODE IN YOUR NOTES!!!

Organization secretly goes out the window...


One question you might have, is that you are not sure how to write all this
crap on paper...

For this answer, look up these things...

PSUEDO CODE, & FLOWCHARTS!

PSUEDO CODE is very simple...

It's basically basic code...

but, instead of saying -


Screen 13
cls

open "dude.txt" for input as #1
  for i = 1 to 100
    input #1, dude(i)
  next
close #1

for i = 1 to 100
  print dude(i)
next


You would just say - 


Change screen mode
Clear Screen
Read data from file
Print DATA from file


And, you can eventually get more indepth when you get into your actaul
coding in qb, or, if you like, you can get more indepth in the Psuedo code...

It helps to leave some things to the actaul programming section of your work

Psuedo, as you might allready know, means FALSE...
So, its FALSE code...

Meaning that if you put this into a compiler, it wouldn't work...

If, you wanted to cover a certain part of code as code, you would write it
much like you would in QB...
For the Read Data From File line, this is what it would look like in
Psuedo code, if you chose to write it all out...

Open file "dude.txt"
Read from the file 100 times
output each input from the file to the screen

and, you can get more indepth if you like...


Screen 13
Clear Screen
Open file "dude.txt"

for i = 1 to 100
  Read input and store in array DUDE(i)
Next

Close file "dude.txt"



!!NOTE!!

But, one of the biggest advantages of Psuedo code, is that it can be read
by anyone, no matter how much programming experiance they have...

If your boss, wants to review your progress, or a team member wants to
better understand how the engine does certain things, they could easily
look at your notes, and understand them...
---



Although, this is a great advantage over basic Code, you may feel that your
readers may want another way to view your code, to get the basic feel of
it...

A great way to do this, is with FLOWCHARTS...

It's self explainitory...

You use drawings to show the general flow of a program...

One of the problems with this code, is that you can't use FOR NEXT loops...

If you ever want to use a loop, you need to set up a counter...
This is because, it is harder for people to follow FOR loops along a path...


Flowcharts work kind of like this...

If you can draw a line from start to finish, with no problems, then you
have a good general flow of the program...

If you can't finish, you know exactly where your problem is...

I would even go to say, that it is much much easier to use then DEBUG...

Except that flowcharting is done on paper...

There are a few symbols that you need to learn in order to flowchart...

such as...


|-------|
|_______| - your basic code box...


  
   /\
  /  \
 /    \   - Decision symbols...
 \    /
  \  /
   \/


 (  start )

 ( finish )

start and finish circles...
(Text can't draw good circles... actually thier ovals...)


 /--------/ 
/________/   -  Input/Output boxes...





---
whats a code box for?

Code... 
such as...

x = x + 54
or
tannis = fred - 4 + graddy

THESE BOXES DO NOT HOLD IF STATMENTS!!!
we'll get to those in a minute...

here is a crappy example...

  (start)
     |
|--------|
|  x=x+1 |
|________|
     |
  (Stop)

---

Decision symbols...

These are the If Then statments...
In a way though, they are Yes/No statments...

If a sertain condition is met, they go in one direction...
If it isn't, then it goes into another direction...

quick crappy example...

 (start)
    |
|-------|
| x = 5 |
|_______|
    |
   / \
  /   \
 / x=5 \___No___|-----------|
 \     /        |x = not 5? |----->|
  \   /         |___________|      |
   \ /                             |
    |                              |
    |yes                           |
    |                              V
 (stop)<----------------------------


as you can see,
it asks yes or no...

see if you can run the program to the end...

it should be easy...

in the beginning, x is told to be 5
so, when it gets to the decision symbol, it asks if x equals five...

Since, it has been set to five, the answer is yes, and if follows down the
line to the stop symbol...

If, x didn't equall five, it would follow the no arrow, and go to the code
box, where x would = not 5 (wich doesn't really make sense...)
after x = not 5 it would follow the arrows until it hits the stop sign...

---------

now, the input and output symbol...

Its easy...
If you want to input or output something, you would use this box...

DO NOT get them confused with basic code boxes!!!

here is an example


    (start)
       |
  /-----------/
 / input num /
/___________/
      |
  |-------|
  | x=x+1 |<-------
  |_______|       ^
      |           |
     / \          |
    /   \         |
   / x=4 \___no__>|
   \     /
    \   /
     \ /
      |
      | yes
  /-------------/
 / Print num   /
/_____________/
      |
    (STOP)

And, here is your basic loop...
(see if you can follow the programm...)
what this does, is first, let you input a number, and stores it in the
variable NUM

then, x is added by one...
(so, first, x = 1)
then, it goes to the decision box...
if x equals four, then it would go to yes,
but, remember that x right now equals 1, so it would follow the line
that goes back to the code box (x=x+1)
now x = 2, but x still doesn't equall 4, so it would go back around...
it will do this, until x does equall 4...
when x equalls 4, it would follow the yes line, and go to the next I/O box
and print num...
then it would stop...

I know that I should have put the print num in between the loop, but I'm too
lazy to change it...

Apathetic, if you will...

you could write out this code like this, if you wanted...

input num
for i = 0 to 4
next
print num

or you could more accuretly do it like this...

input num
do while x < 4
x=x+1
loop
print num

or

input num
do
x = x +1
loop until x = 4
print num

and, there you go...

thats all you really need to know about flowcharting...
good luck, it will greatly improve how you think of programming...

Visualization, will also help the reader of your code...
Does grandma wonder what your coding?
Show her...



----------------------------------------------

Shared Game engines...

You know what they are...

Remember Duke nukem?

Here are a few games that have come from that very engine...
(the Build.exe Engine...)

1.) Duke nukem (duh)
2.) Shadow warrior
3.) Redneck Rampage

How did those games get created?
They just added graphics right?

NOPE...

Just as much planning goes on for a Modification (mod) as did the original
game...

Documentation, Planning, Flowcharts, psuedo Code...

but, most of the boring tedious crap is all out of the way...

Try that...

Has a friend written a cool piece of code?
Have you found a cool game on the internet?
Do you have a cool piece of code written by yourself?

Whip it out and create a MOD...

Once again though...

Give credit where credit is due...

and thats all I have to say about that...

----------------------------------------------------------------------------

Before I leave you to the dogs,

Remember one important thing...

If you are creating an engine...

Make sure you document constantly...

Document how your sprites move...
Document how your sprites are Saved and Loaded...
Document how your sprites are animated...
Document how your sprites interact...
Document your collision detection...
Document The general flow of the engine...

And, if any of these things change, MAKE SURE TO CHANGE THE NOTES!!!

Are you writing a PONG game?
DOCUMENT IT!!!

When you don't feel like programming,
Take out your notes,
Look at some of the documentation...

Look to see how you can make it smaller...
look to see how you can make it more understandable...
LOOK TO SEE HOW YOU CAN MAKE IT BETTER!

It is much easier to make changes in your notes and documentation, then it
is to change JUST the code...

If you change the code, you might as well rewrite the documentation for it...

If a time comes, where you feel that your needs no longer fit the
documentation...  Throw them out, and RE-Plan your project...
DO NOT abandon the documentation and stick to pure code...

That leads to apathy...

If you have to go through 500 lines of code,
don't you think you would get tired of searching for whats wrong, or what
needs to be changed?

When there is a page of documentation for each thing, it is much easier to
see what is causing the problem...

---

think about the Quake engines...

They created Quake 1...
They sold quake 1 at the stores...

How did they create the engine for quake 2?

They revised thier documentation, and created a brand new engine...

They didn't Revise thier direct code...
They threw it out the window, and stuck to the documentation of the engine...

---

Do you have an image editor that you just wrote?

Do you want to revolutionize the way it draws, and saves?

Do NOT take the old program and edit it to fit your new ideas...

The code is from an old way of thinking...

The documentation, is How you came to that way of thinking...

and, it can be used to create a whole new way of thinking...

But, the code will block you from your goals...

And, that will of coarse lead to PROGRAMMING APATHY!!!

---

You have been warned!

good luck!
and have a nice day!!!



----------------------------------------------------------------------------
****************************************************************************
----------------------------------------------------------------------------

   File  Edit  View  Search  Run  Debug  Calls  Options                   Help
+--------------------------------- Untitled -------------------------------�+-+
�Thats it for this tutorial, If I didn't get into enough detail in the         
�explanations then just look at the source code and try to figure it out
�on your own.  All else fails E-Mail Me...  I want to make these tutorials     _
�as easy to understand as posible!!!                                           _
�                                                                              _
�My current E-Mail address is RADIOHANDS@AOL.com                               _
�                                                                              _
�If you are using this tutorial on your page, please leave the tutorial        _
�exactly as it is... please don't change anything, unless its spelling         _
�errors... Theres alot of them! I don't like using the backspace key...        _
�                                                                              _
�The original website that these were on is                                    _
�                                                                              _
�http://members.aol.com/radiohands/index.html                                  _
�                                                                              _
�Thank you                                                                     _
�Vic Luce                                                                      _
�Finished                                                                      _
�Febuary, 4                                                                    _
�2001                                                                          _
�                                                                              
� ___________________________________________________________________________�
 <Shift+F1=Help> <F6=Window> <F2=Subs> <F5=Run> <F8=Step>     �     N 00001:008


...
If you want to be notified when a new tutorial is out..
Send An E-mail to VQB-subscribe@egroups.com
(check website)