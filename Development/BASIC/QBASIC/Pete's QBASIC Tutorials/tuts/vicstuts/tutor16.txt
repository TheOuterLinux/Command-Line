
                     Vic's QBasic Programming Tutorial

                           Basic Tutorial XVI

                           Making QB Libraries...
                     Make QB your very own language...
                           Radiohands@aol.com

-----------------------------------------------------------------------------

In my last tutorial, I told you how to use Qbasic library files...
Now comes the fun part... Making your very own Library files!!

Why bother making your own library files when there are tons of them
out there wich are a hundred times better than you could ever make?

Because!! its fun, and you can custimize the library just for you,
You could make a new faster PSET command and you could make is so you
could call it like this
instead of

PSET x,y,color

you could do this

Dogcrap x,y,color

I don't know why you would want to, but I once received an e-mail asking how
you could make a new command in qbasic and call it dogsh**... (where do
these people come from?)

I used to feel intimidated in the fact that there were people out there
that could do something in qb that I couldn't...  I even refused to use
their libraries because I though that it was cheating and it just didn't feel
right...  After a while I grew up and used their libraries and found that

1.)  Some of them sucked
2.)  Some of them Were a godsend (dqb) <-- the best...
3.)  Some of them did stuff that seemed imposible (future) <-- best VESA LIB
4.)  Some of them were confusing and didn't know how to use them...
5.)  Many (only the good ones) came with source code...

But it still didn't feel right to use theirs...  I can program in ASM...
I know what needs to be done different, and for my own programs what is
needed and what is not...  Let me try...



I opend up a new text file and got to work...

dqb used screen mode 13...
I would use screen mode X...

DQB uses virtual screens (computers memory)...
I would use the actuall screen (modex has 4 pages) (vga memory(quick))...

DQB uses EMS...
I would look at what it takes to program EMS and pass out...
they got me there...

DQB can scroll (asm computer commandS (copy/paste))
Modex can scroll Naturally and not use virtual screens(1 hardware command!)

DQB has to set up...
Mine, just call a command no setup...

dqb usually calls commands like ex.  DQB.PSET (page,x,y,clr)
call my pset like this...  vp (page,x,y,clr) (quick and easy to remember)



!!!!!!!!!!!!!
NOTE!!!!!!!!!
!!!!!!!!!!!!!

I don't mean to pick on DQB, I love it, and I am sure some people would
prefer the way its set up...  If you write a library youreself, you would
be more familiar with the commands and their setup That is the point I
am trying to get across...

----------------------------------------------------------------------------
Ok, I think you know why you would want to make your own library, lets get
to work!!...


 
First step to making library...

Open up Notepad... (NOTEPAD.exe)...
Not worpad or works or WORD or word perfect...
Good o'le Notepad (you can use the others, but why bother?)

----
First, lets set up the skeleton that every library will have...

like this...

;----------


.MODEL medium,basic
.386
.STACK 100h
.CODE

END


;----------

!!note!! you don't need the ;-------...

Well you could change this somewhat to fit your own purposes, but I havn't
run into a case where that needs to be changed...

the commands you want to make will be in between the .CODE and END...



First, lets cover making a sub (known as a PROC in ASM)

For this example lets make a sub that turns on the mouse...
Do you remember the asm commands needed to turn on the mouse from the old
tutorials?

MOV ax,1h
INT 33h
RET

ok before we add that, let set up the sub (proc)
lets call this sub mouseon...
this is how it would look...

PUBLIC mouseon
mouseon PROC

now write in the program in ... (mov ax,1h...)
then at the end of this write ENDP to end the sub (same as END SUB)


it should now look like this alltogether...




.MODEL medium,basic
.386
.STACK 100h
.CODE

PUBLIC mouseon
mouseon PROC

MOV ax,1h
INT 33h
RET

mouseon ENDP



END


Thats what it should look like...
You are finished with your first command, and if you wanted, you could make
this into a library...

lets do that...


------------------------------------------
******************************************
------------------------------------------

Before we continue, there is one thing that you need before you can continue
with making the library... You need a real ASM assembler...
The ones that I use and you may be familiar with are

1.) MASM  - Macro Assembler
2.) TASM  - Turbo Assembler
3.) NASM  - FREEWARE assembler

If you don't have any of these assemblers than you have to go find them...
Either find them online/get from a freind/ or buy them for about $500...
I have never used NASM comiler alone, and I'm not sure how it would work...
you can try, but I can't offer any help...

before you do a search on the internet, look around your own hardrive for
them...  you most likely have it, you just don't know it...

For the sake of this tutorial being simple, I will use MASM...  It is very
easy to handle, and has worked easy in the past...

NOTE!! If you find MASM or TASM online it is illegal to download...
Pirated software is illeagal and wrong...  I am in no way telling you to
download it...  If you can't get a friend to use his/her computer for the
assembling, then don't bother...  you could although use NASM to do this for
you all free and legal like...

------------------------------------------
******************************************
------------------------------------------

asuming you have a assembler (legally), we will coninue...

Save the new file you are working on in the same folder as your copy of
MASM or TASM and name it dude.txt...

go to a dos prompt and go to the directory and type this in...

MASM dude.txt
and hit enter until it makes it into a obj file...

or if you have tasm change it to 
TASM dude.txt

...


Now we have an obj file... copy that obj file (dude.obj) to the directory
where you have qb...
This directory should also contain the files

Qb.exe
Lib.exe
Link.exe
and a few other lib files like...
bcom45.lib
bqlb45.lib

If you are missing any of these files you can't make a library file...

now that you have the file dude.obj in the same directory as qb then
open up a dos box and cd to the directory...

type...

lib dude.lib +dude.obj;
and hit enter...

then if everything goes right then type this...

link /q dude.lib,dude.qlb,NUL,BQLB45.LIB
and hit enter...

if everything goes correct and nothing is missing you are all done...
you have created your very own library file...
your qb directory should now have these files..

dude.obj
dude.lib
dude.qlb

the dude.qlb is your new library file...

-----

Now that we have our library file made, lets use it...

in the same dos box type this in...

qb /ldude
and hit enter...

can you remember the sub we created? mouseon?
before you can use it, you have to tell qb that it exists, so type this in...

DECLARE SUB mouseon ()

Now we can use it!!
Lets do...

Type this in after Declare SUB...


SCREEN 13
mouseon


the complete program should look like this...


DECLARE SUB mouseon ()
SCREEN 13
mouseon


If everything went correct when you made the library, when you press run,
you should see the mouse on the screen...

If there was a problem during the library making, It shouldn't be to hard
to find out what went wrong,
here are a few things that could have went wrong...

1.) you didn't copy the program corectly (misspeled words)in dude.txt
2.) MASM or TASM reports an error and doesn't create a dude.obj...
    this would be solved by #1...
3.) when compiling the library with LIB and LINK you get a missing file...
    To solve this, look for the missing file on the internet...
4.) You also might have messed up on the commands, make sure you write the
    commands how you see them...



----------------------------------------

Ok I hope that works for you,
Now lets talk about adding functions to your library file...

go back and open up dude.txt...

now, lets talk about functions in Qb libraries...

With a function, you are looking for a value to return to qbasic...
this returning value must then be stored in the AX register where qb can
find it...

for this example, we will get the mouse's x value and bring it back to
qbasic...

I am assuming that you know how to program in ASSembly... So I will not
go into great detail what it is doing here...

mov ax,3
int 33h
mov ax,cx
shr ax,1
RET

This is the code I would uset to find the value of the mouses X position...

lets go over it...

mov ax,3
int 33h

calls the mouse function that gets the mouse x and y values (along with the
mouses button value...)

mov ax,cx

the mouses x value is stored in the CX register and in order for QB
to read it, it has to be in the AX register... so lets move it there...

Ok, the SHR ax,1 can be a little confusing...
What it does is divide the value so it will work in screen 13...
You must change that if you want it to work in other screen modes (12,9...)


------------


To set up a function you just set it up as another sub...
Only, when you call it, it will not perform anything, it will just
bring back a value...


So this is what the mouse sub looked like...

PUBLIC mouseon
mouseon PROC

MOV ax,1h
INT 33h
RET

mouseon ENDP




This is what the MOUSEX would look like...




PUBLIC mousex
mousex PROC

mov ax,3
int 33h
mov ax,cx
shr ax,1
RET

mousex ENDP



the only thing that is different, is the code and the name...


So, type this into the dude.txt...
alltogether the file should look like this...


;------------------------ This is the new library... (dude.txt)


.MODEL medium,basic
.386
.STACK 100h
.CODE


PUBLIC mouseon
mouseon PROC

MOV ax,1h
INT 33h
RET

mouseon ENDP



PUBLIC mousex
mousex PROC

mov ax,3
int 33h
mov ax,cx
shr ax,1
RET

mousex ENDP



END


;---------------------- That was the New library...




OK!!
Do you remember how to make the library?  I hope...
I will cover it again briefly...


FIRST!!!!!!!!!!  IMPORTANT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

MAKE SURE YOU DELETE DUDE.LIB & DUDE.OBJ & DUDE.QLB from your qb directory,
or THIS WILL NOT WORK!!!!!!!!!!!!!!!!!!!!!!


save as dude.txt in the same directory as MASM or TASM
(If I were you, I would put masm or tasm in the same directory as QB...)

Goto a dos prompt and cd to the directory where dude.txt is...
type in masm dude.txt and keep hitting enter until it leaves you alone...

Now, it should have made a dude.obj... If it didn't then look over the code..

copy the dude.obj to the same directory as qb...

got a dos prompt and CD to the qb directory...

type these commands in respectivly...



lib dude.lib +dude.obj;
link /q dude.lib,dude.qlb,NUL,BQLB45.LIB



If everything goes OK, and you have all the files, then you will now have
a new working library...

in the same dos prompt, type

qb /ldude

QB should open up with your new library...

---

Ok, your all ready to use your library...

First, you have to tell qbasic the names of the functions in QB...

The first one we know...

DECLARE SUB mouseon ()

how do you declare a function...

!!THIS IS IMPORTANT!!

You would think, you would declare it like this...

!!WRONG!!  DECLARE FUNCTION mousex () !!WRONG!!

in order to get qbasic to know that it is supposed to return an integer,
not a word you need to ad the "%" sign to it...

So, This is what the correct version would look like...

!!CORRECT!!  DECLARE FUNCTION mousex% ()  !!CORRECT!!

But then How would you call it?

Like this...

x = mousex

or display it like this...

print mousex or print x

It would look like that not x = mousex%...  If you call it like mousex% you
will get en error, or it will return back 0 all the time...

--
So, in our new library we have these functions...

DECLARE SUB mouseon ()
DECLARE FUNCTION mousex% ()



This is a short little program that will show you how to use the commands...


'--- begin sample...

DECLARE SUB mouseon ()
DECLARE FUNCTION mousex% ()
CLS
SCREEN 13
mouseon

DO: press$ = INKEY$
LOCATE 1, 1: PRINT mousex
LOOP UNTIL press$ = CHR$(27)

'--- end sample...





                                  | NEXT |
----------------------------------------------------------------------------

Alright!!! We know how to get input from a Library file... Whats Next?

Puting input into a library file right???

This is where things get a bit complex...
But if you know ASM then it isn't that bad... and you know asm right???

We (I) want to make a new sub that looks like this...

DECLARE SUB mousexy (BYVAL x%, BYVAL y%)

this sub puts the mouse cursor at the position x and y...
we could also adapt this to put a pixel at point x,y. but were not...
so there...

What kind of code would you need to write to get the values you put into
the sub (x,y)?

First, you have to know what QB does when you call the sub and give it
values along with it...
What does qb do with The X & Y values?

QB stores the values in memory...  Who would of thought???
(yes I'm being sarcastic...)
I bet you allready knew that, what you want to know maybe is how to get
those values out of the memory...
I know the word MEMORY sends shivers down your spine, but it is much
easier than you would think...

I am assuming that you know what the stack is in ASM...
If you don't, exit this tutorial and go find one for ASM that covers it...
one thing to keep in mind though... First on first off... I think you know
what I'm talking about, and this could help you understand the order of the
stack where the values are stored...


Ok, first, we have to Point the Stack To the peice of memory that has the
values in them...  Very easy... It never changes...

PUSH BP    ; this gets the values from the memory...
MOV BP,SP  ; Point to the memory...

Thats it...
You can now access the values of X and Y!!!!

Now I will tell you how to get the values and store them into a register...
(man I'm tired...)

MOV CX,[BP+8]
MOV DX,[BP+6]

CX and DX have the values of X & Y...
Why 8 & 6�
because 2 & 4 are taken... 'nuf said...  Ignore 2 & 4... oops...

If you have more than one variable, they would go like this...

6 8 10 12 14 16... you get the picture...

OK OK Enough!
Lets make the sub...

Look up the code to set the mouses x and y position and follow the
bouncing ball...

PUBLIC mousexy      ;O  set up sub...
mousexy PROC        ; \
  PUSH BP           ;  O point to memory...
  MOV BP,SP         ;   \
  MOV CX,[BP+8]     ;    O get X position (or is it y?) and store in CX...
  SHL CX,1          ;   O Set up x for screen 13 y doesnt need it...
  MOV DX,[BP+6]     ;  O get y (or x) and store in DX...
  MOV AX,4          ; /
  INT 33h           ;/
  POP BP            ;O  Return Memory...
  RET 4             ; O Return 4 bytes or something... 2 * # of variables...
mousexy ENDP        ;O End sub...


Wow...
I'm tired...

Important points to understand...

You must pop BP...  BP must be poped... Do or Die...
Make sure you RETurn the correct ammount... If you don't do it correctly,
you will run out of memory and crash you computer...

It is easy to find out how much must be returned...
Number of arrarys Times 2
so, in our example there are 2 arrays...
2 * 2 = 4...
RET 4...


----------------------------------------------------------------------------

Ok, open up your old dude.txt library and add it...

The dude.txt file should now look like this...


.MODEL medium,basic
.386
.STACK 100h
.CODE


PUBLIC mouseon
mouseon PROC

MOV ax,1h
INT 33h
RET

mouseon ENDP



PUBLIC mousex
mousex PROC

mov ax,3
int 33h
mov ax,cx
shr ax,1
RET

mousex ENDP


PUBLIC mousexy
mousexy PROC
  PUSH BP
  MOV BP,SP
  MOV CX,[BP+8]
  SHL CX,1
  MOV DX,[BP+6]
  MOV AX,4
  INT 33h
  POP BP
  RET 4
mousexy ENDP

END




-----

Look, I'm tired...
Please tell me you remember how to make the library runable!?
If not from memory, then scroll up...
but before you do, 

MAKE SURE YOU DELETE THE OLD DUDE.LIB AND DUDE.QLB!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Fell asleep on the ! key, sorrrrrrry..............................


Here is your subs now...
make sure you add BYVAL to your own library subs...

DECLARE SUB mouseon ()
DECLARE FUNCTION mousex% ()
DECLARE SUB mousexy (BYVAL x%, BYVAL y%)

Great!!!!
Now here is your last example...


   File  Edit  View  Search  Run  Debug  Options                          Help
+--------------------------------- Untitled -------------------------------�+-+
�DECLARE SUB mouseon ()                                                        
�DECLARE FUNCTION mousex% ()                                                   _
�DECLARE SUB mousexy (BYVAL x%, BYVAL y%)                                      _
�SCREEN 13                                                                     _
�CLS                                                                           _
�mouseon                                                                       _
�mousexy 50, 50                                                                _
�sleep 1                                                                       _
�mousexy 100, 100                                                              _
�                                                                              _
�                                                                              _
�                                                                              _
�                                                                              _
�
�                                                                              _
�                                                                              _
�                                                                              _
�                                                                              
� ___________________________________________________________________________�
+--------------------------------- Immediate ----------------------------------�
�                                                                              �
�                                                                              �
 <Shift+F1=Help> <F6=Window> <F2=Subs> <F5=Run> <F8=Step>     �     N 00007:015



Thats what your QB window will look like...
Thats what Mine looks like, and it runs...
(only with a nice blue and grey tint...)

!Note! No I didn't draw the QB screen...
I copied and pasted it right out of the dos window...
Well,,,, I thought it was cool...

I wonder what it would look like with a file menu thing up...
Ok, you talked me into it...


   File  Edit  View  Search  Run  Debug  Calls  Options                   Help
+--------------------------+---------------------+-------------------------�+-+
�DECLARE SUB mouseon ()    � Start      Shift+F5 �                             
�DECLARE FUNCTION mousex% (� Restart             �                             _
�DECLARE SUB mousexy (BYVAL� Continue         F5 �                             _
�SCREEN 13                 � Modify COMMAND$...  �                             _
�CLS                       +---------------------�                             _
�mouseon                   � Make EXE File...    �                             _
�mousexy 50, 50            � Make Library...     �                             _
�SLEEP 1                   +---------------------�                             _
�mousexy 100, 100          � Set Main Module...  �                             _
�                          +---------------------+                             _
�                                                                              _
�                                                                              _
�                                                                              _
�                                                                              _
�
�                                                                              _
�                                                                              _
�                                                                              
� ___________________________________________________________________________�
+--------------------------------- Immediate ----------------------------------�
�                                                                              �
�                                                                              �
 F1=Help � Runs current program                               �     N 00010:001


Thats was cool...
What about a help menu???
Here we go...

   File  Edit  View  Search  Run  Debug  Calls  Options                   Help
+-------------------------- HELP: Table of Contents -----------------------�+-+
�  Help on Help   Contents   Index                         Copyright   
�------------------------------------------------------------------------------
�Using QuickBASIC                      BASIC Programming Language          _
�  �                                       �                                   _
�  +- Shortcut Key Summary               +- Functional Keyword Lists       _
�  �      Edit Keys                      +- Syntax Notation Conventions    _
�  �      View Keys                      �                                   _
�  �      Search Keys                    +- Fundamental Building Blocks    _
�  �      Run and Debug Keys             +- Data Types                     _
�  �      Help Keys                      +- Expressions and Operators      _
�  �                                       +- Modules and Procedures         _
�  +- Limits to QuickBASIC               +- Selected Programs              _
�  +- Version 4.5 Differences            �                                   _
�  +- QB Command Line Options            +- ASCII Character Codes          _
�  +- Survival Guide                     +- Keyboard Scan Codes            
� ___________________________________________________________________________�



huh huh...


Where was I?



oh yeah... thats all folks...

!! Wait!!  I have a great Idea for the ending instead of the usuall...


   File  Edit  View  Search  Run  Debug  Calls  Options                   Help
+--------------------------------- Untitled -------------------------------�+-+
�Thats it for this tutorial, If I didn't get into enough detail in the         
�explanations then just look at the source code and try to figure it out
�on your own.  All else fails E-Mail Me...  I want to make these tutorials     _
�as easy to understand as posible!!!                                           _
�                                                                              _
�My current E-Mail address is RADIOHANDS@AOL.com                               _
�                                                                              _
�If you are using this tutorial on your page, please leave the tutorial        _
�exactly as it is... please don't change anything, unless its spelling         _
�errors... Theres alot of them! I don't like using the backspace key...        _
�                                                                              _
�The original website that these were on is                                    _
�                                                                              _
�http://members.aol.com/radiohands/index.html                                  _
�                                                                              _
�Thank you                                                                     _
�Vic Luce                                                                      _
�Finished                                                                      _
�March 2                                                                       _
�2000                                                                          _
�                                                                              
� ___________________________________________________________________________�
 <Shift+F1=Help> <F6=Window> <F2=Subs> <F5=Run> <F8=Step>     �     N 00001:008


...
If you want to be notified when a new tutorial is out..
Send An E-mail to RADIOHANDS@AOL.com
with the subject saying VQBLIST and then your E-mail address(check website)

