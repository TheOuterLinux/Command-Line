              PROGRAMMING IN QuickBasic 4.5 for Complete Beginners.
              *****************************************************

               By Raze 1st-2nd March 2000, Updated 3rd April 2000

                                 LESSON I

   Skill Level - Beginner

IN THIS LESSON:
===============
Operators:
=  <  >  *  +  -  /  \ ; : , " " '  $

Keywords:
CLS
DO/LOOP
END
FOR/NEXT
GOSUB/RETURN
GOTO
IF/THEN
INPUT
LET
PRINT
REM
SELECT CASE




CONTENTS:
=========

PART 1: Background on QB
   - Tips
   - Introduction
   - Language Levels
   - Low Level
   - High Level
   - Where can I get QuickBasic?
   - The QuickBasic IDE

PART 2: Practical Stuff
   - Outputting text to screen
   - Running your program
   - Semi-Colon
   - Clearing the screen
   - Variables
   - Text variables (strings)
   - User input
   - Loops
   - Using TAB indentations
   - FOR/NEXT loops
   - Decision making 1 (IF/THEN)
   - Decision making 2 (More on IF/THEN)
   - Decision making 3 (SELECT CASE)
   - CASE ELSE
   - Label Subroutines using GOTO, GOSUB and RETURN
PART 3: Putting It All To Good Use
   - Feature program: Text Adventure Demo for QBasic 1.1+


                     PART 1 - BACKGROUND ON QB
                     =========================

   Tips:
   =====

   -  Copy and paste the examples shown into QB and try 'em out.
   - Try writing your own little experimental programs based on what you
      learn each lesson!
   - Read this while in Windows so you can ALT+TAB between the tutors and
      QuickBasic, and paste the examples easily.
   - Use the contents to search for what you need to know quickly: Just use
     search to find the exact same line of text... Don't worry, it doesn't
     have to be case sensitive.
     

   INTRODUCTION
   ============

You wanna make games. That's why you're reading this right? Well, especially
if you're a complete newbie, you've come to the right place. It might not be
as easy as you think, then again it might not be as hard as you think. It's
just a matter of grasping the concepts of programming basically.

Now don't expect to be making your own arcade games, platforms or RPGs within
days, weeks or months even: Developing an advanced programming skill takes a
lot of time and practice, and to gain a full understanding we have to start
from the very fundamentals and build our way up.

A lot of this information in the first few paragraphs can be bypassed, it
merely gives a bit of background on where QBASIC stands compared to other
programming language levels. You can skip to Part 2 if you like, where the
practical stuff begins.


   LANGUGE LEVELS
   ==============

So, what do you think programming is all about? This sort of thing?

�ܜQ�

Have you ever opened up in the past, say an .EXE for example in Editor in the
hopes to see how it works, only to see thousands of characters like the stuff
above? Don't sweat, even I don't understand how that jargon works!

That's called machine language. It's in the form of ASCII characters, and it
is in a form that talks directly to the computer. Actually, the next step
below ASCII is binary, which is the lowest level computer data possible, 0
and 1.

ASCII? Binary? What the...

You'll be happy to know that we will not be doing anything of this nature.
QuickBASIC is much simpler to use. The different complexities of programming
languages are called LEVELS. This bit isn't important to know, but it does
help you understand exactly where languages like BASIC stand in the computer
data heirarchy.


   LOW LEVEL
   =========

You've got Low Level Data, and High level data. Low level data is the nitty-
gritty of computer data. The lowest is binary, that is 1's and 0's. You
know bits and bytes right, the size of files? One binary character is one bit.

Machine language is the next step up from binary and it is represented by
255 different ASCII characters. You know all those smiley faces,
accented letters, odd shapes and arrows? That's ASCII, and one ASCII character
takes one byte of memory. One byte is 8 bits, so each ASCII character is a
different combination of 8 bits. This means binary information can be
represented with 1/8 the amount of characters.

Then there's assembly, which uses alphabetic characters as keywords to perform
CPU operations or something like that. There are many tutorials on assembly
around but if you're a newbie I'd forget it until you truly master QB...
Trust me!! ;)


   HIGH LEVEL
   ==========

BASIC and all it's descendants are classed as HIGH LEVEL langauges to
VERY HIGH. BASIC uses english-like words to perform computer tasks. Now
computers donot understand these english keywords, it only understands
bits and bytes of bits.

High Level languages are created so as it is much easier and less time
consuming for the programmer to code. All QB's syntax uses is letters,
numbers and some punctuation including mathematical symbols.

Raw High-level code cannot be understood by the machine, therefore it is
not executeable. Don't believe me? If you really dare, just try coding a
program in QB, renaming it in DOS from a .BAS to an .EXE and just see what
happens, but do so at your own risk!

What your coding needs is an interpreter. All High Level programmers and/or
compilers have an interpreter. That is, when you run it it interprets your
source code into machine language that it can understand. QB 4.5, unlike 1.1
serves as an actual compiler that can actually make your programs into
runable .EXE files by turning it into machine language... How cool is that?

High-Level Languages come at a cost though. Firstly, becuase they must run
through an interpreter it is slower than straight assembly or machine code.
Secondly, when using High-Level programming languages you are limited to
the keywords or key-functions that language has.

I mean, QB does not really on its own have the power to program a proper
operating system. You might be able to make a GUI that resembles Windows
icons, but thats about as far as it goes, you haven't got full harware and
resource control, or the speed.

But that's not to say you can't do much with QB...


   Where can I get QuickBasic?
   ===========================

Bill Gates was kind enough to include QBasic 1.1 with some of the later MS-DOS
versions up to 6.x. QBasic came in a file called QBASIC.EXE . It even came
with a couple of sample programs, including the classic 'Gorillas', where it
ALL began.

So, you know this filename? But let me guess, you've heard this story time and
time again but you've never found the mysterious QBASIC amongst your DOS
files. Let me guess - You have a Windows 95 or better operating system.

Damn. Unfortunately for you, QBASIC was never inlcluded with MS-DOS again.
I guess by 1995 they were beginning to focus concentration on Visual Basic
instead, as a separately sold, expensive product (and useless for making
decent graphics games).

There is hope yet. Because of it's age, QBasic 1.1 has in Microsofts eyes
passed its use-by date completely so it is now classed freeware, many sites
have it downloadable on the net. But although a professional edition -
QuickBasic 7.0 exists, it seems QuickBasic 4.5 is still not old enough, as it
is not yet freeware and illegal to distribute haphazardly. If you look around
you'll still find 4.5 around to dowload but LEGALLY you must delete any
copy of it you posess within 24 hours. Don't ask me how they manage to police
that, but that's the law!!

If you do only have 1.1 don't sweat for now, it's all you need... But it
cannot compile into .EXE's or use libraries like 4.5, and it isn't quite as
fast.


   THE QUICKBASIC IDE
   ==================

QuickBasic is classed as an IDE (Internal Development Editor), which means
the programming can be done inside QuickBasic itself. Some Compilers are not
IDE's (They only convert files to .EXE) so the actual programming has to be
done and saved elsewhere, in the Windows Notepad for instance, or MS-DOS
Editor.

IDE's have many advantages. The main one is no exiting or entering from DOS
or Windows is needed, you can do it all in the one program. Secondly, an IDE
can show errors for you before it is even compiled, so it saves a lot of
debugging time.

Obviously QuickBASIC is a DOS-based program, and you will perhaps notice its
close resemblance to the MS-DOS editor, as you have a menu bar at the top,
and a text window to type in. Lets run over some of the menu options.
(Only main options will be mentioned, there are numerous more):


File:
- Contains New, Save, Load File options and more. QB 4.5 even has a DOS Shell
which is handy for changing the directory and managing files without having
to exit at all.

-!*NOTE*!: I suggest you save your files as text at ALL times. Saving in QB
format may result in faster code, but you can save that for making .EXE's.
Also, if need be you can view and edit your code elsewhere like MS-DOS Editor.


Edit:
- The Standard functions like Cut, Copy, Paste etc

Search:
- Find or replace many of the same string in your file. Handy for changing
the name of a widely used variable.

View:
- View your modules, Split the screen into two working areas etc

Run:
- Run your source code.
- Compile into .EXEs
- Make Libraries.

Help:
- Contains a large Help section including an index of all keywords, operators
etc. Has some examples for many. You will also find ASCII and keyboard scan
tables for reference.

QB's interface is very easy. The code simply goes in the text window at
whatever line and position on line your flashing cursor is located.



                     PART 2 - PRACTICAL STUFF
                     ========================


   Outputting text to the screen:
   ==============================

We are now ready to begin some simple programs to get you aquainted with
QuickBasic. Are you in QB now, with a new, empty file waiting? Good stuff.


In learning all high-level programming languages this is probably the most
fundamental of fundamental commands: Writing text to a screen. The command
for almost all kinds of BASIC languages is PRINT .
Try entering the below into your main window and press ENTER.


PRINT "Hello, World!"             'Prints the message in quotes to screen


The command is PRINT, which tells the computer to display some text to the
screen. Whatever is in between the double quotes, it will output to the screen
when you run it, (but not the quotes themself). See? It's not that difficult,
it represents plain english. That's how QB does things, by English-like
keywords along with maths.

And see that message to the right? That's called a COMMENT. This is a short
piece of information you may wish to add to explain what the code is doing.
It must be preceded by the ' so QB doesn't try to run it! You really don't
need to add comments at all, they are mainly so you can look back and see
how your code works and so other people know what it does.

In future lessons you will probably see more of these as QB lines become more
complex. Instead of a ' you can use REM, which means remark. REM is merely one
of those relics from older BASIC languages and I prefer not to use it as it
takes space and can be confused with whatever text follows.


   Running your program:
   =====================

Go up to the menu bar and select run/start. This is how you run every QB
program. You should now have a dark screen like DOS with the message above.

Woohoo! I've made my first program!
it doesn't matter whether you have caps lock on or off, QB has a syntax
checker that automatically makes all commands uppercase when you finish
editing a line, save or run a program. The text in the quotes will not be in
uppercase unless you type it that way, as it isn't a command, QB basically
doesn't give a damn about what you put in there, well yet anyway.


   SEMI-COLON:
   ===========

Now try this. Change "Hello, World!" to just "Hello, "   (delete "World!")
Then, on a new line below, type  PRINT and then in quotes your name. Now
the program should look like this for example:


PRINT "Hello, "
PRINT "Jack"


You want the whole thing to appear on one line? Why don't I just put my name
in with the quotes after the hello? I hear you ask. Well, you can do it that
way but the purpose of this exercise is to teach you another trick: the
semicolon(;). Putting the semicolon at the end of a print line (OUTSIDE the
quotes) it means anything printed afterwards follows on the same line.
try adding a semicolon after the first line, like this:


PRINT "Hello, ";
PRINT "Jack"


Run it. Amazing!! By some miraculous means, Jack follows straight after Hello!
Do I detect carcasm there? I suppose you're wondering, why do I need to know
how to make my computer talk to me to make an action game? I don't need
imaginary friends. Sorry guys, if you are super new to programming this boring
pointless sorta stuff has to be gone through...

You can even use the PRINT command as a one-line calculator program. That is,
you can use PRINT to print the result of a maths procedure. behold:


PRINT 10 * 2


Note you do NOT use quotes, otherwise when you run it the program will put
10 * 2 to the screen. Hey... Computers are dumb. I mean *really* dumb. They
will do exactly what you program to do, it won't assume you're trying to do
maths if you use quotes, it will just print it away as a string of text
without second thought.

So what does the line above do? It multiplies 10 by 2. The asterisk(*) is the
QB operator for multiplication, NOT x. When you run it, you should see the
number 20 displayed. Here are the main maths operators:

      + add
      - subtract
      * multiply by
/ or \  divide by

Try all kinds of different math commands, just to make sure you've got it.

   Clearing the Screen:
   ====================

Is it starting to get on your nerves that everything we've done so far
remains above on the screen? Well, there is a way to start with a blank screen
every time you start a program, and that is CLS. It simply tells the computer
to blank the screen and then continue. Comes in very handy. Try putting CLS
into the top of your program as the first line, and see what it does.
Example:


CLS


   Variables:
   ==========

It is impossible to make any practical program without variables.
What are variables? Variables are little pockets of RAM that can be used
to store data such as numbers or text. If you don't know yet what RAM is,
you're in big trouble - I suggest you go and do your homework on computer
terminology immediately. Otherwise, carry on.

variables can be given any kind of name, preferably using only letters, upper
of lower case it don't matter.

Let's put some information into a variable called simply A. Enter the
following:


A = 12
PRINT A


That's pretty obvious what it does. When used in this context, the equals
sign puts the right value into whatever variable is stated to the left. So,
It is saying 'Give variable A a value of twelve', and the next line means
'Print the value of A'. Again note, donot use quotes around A when you print
it, otherwise QB will take it literally and display A on the screen when
you run it, which is like pretty pointless. Another method of setting values
is to say LET A = 12. However you can get away with not including the LET
command at all, it saves time and space.

You can use maths with variables as well, example:


A = 3 + 8
PRINT A


The above is basically high school algebra, as A must equal 3 + 8 which means
A must equal 11. Now next is where QB defies standard laws of algebra.
Remember in algebra you are trying to find the value of the unknown. In
QB it is sort of the opposite: You give it a value instead. So the line:


A = A + 1


... It does NOT mean A is equal to itself plus one. How can that be possible??
That's like saying the long-haired bloke is bald as a badger. No, this is
NOT algebra. What this line is saying is 'Get the value of A, then return to
A the value of itself plus one.' See? So if A already equals 0, it will become
1. If it is already 45, it will become 46. You get the idea. Try adding PRINT
A before and after this line to see how A's value is different.

Again, try different stuff like subtracting an amount from A, multiplying,
everything, just to grasp this concept which might not be easy if you know
your algebra. Maths and variables is essential knowledge for games. Say you
have your score represented as a variable called score. Every time you kill
a monster you increase the score value by 1 or however much you want, the
above is basically how you would go about it.


   Text variables (strings)
   ========================

Variables don't just hold numbers, they can hold letters and words too, but
some punctuation can't be accepted or it'll spew.


A$ = "This is a string."
PRINT A$


Notice the dollar sign? Dollar signs at the end of variable names indicate
this variable can hold text. You can store numbers in this variable but not
in the same way as a normal number variable, you can't do maths with it.
A string is a group of characters, like a sentence. This is a common term in
programming, so remember it well.


   User Input:
   ===========

There's hardly any point making a program unless you can interact with it to
some degree, and this is done by getting the user's input, that is whatever
you enter to the keyboard. Try this program below.


PRINT "Hello, what is your name?"
INPUT name$
PRINT "How old are you?"
INPUT age
PRINT "So, you're "; age ; " years old, "; name$


There's a new command, the INPUT command. What it does is prompts you with
a flashing cursor to enter some information. In this program you enter
a value into both a string and numerical variable. We've called them name$
and age, because having a variable name that describes what it is for helps
when you write big programs later on so you can keep track of what's going
on. The program prompts you for your name age and age, then outputs what
you've entered amongst a sentence.

Get it? On the last line, age is replaced
with the value for age that you inputted, and name$ prints whatever name
you inputted, however crude or humorous. Remember, donot put the variables
in quotes as well, and the semicolon's purpose is to keep it all on the
one line.



   Loops:
   ======

Very important. If you want to make a real-time game you need the same
code to be processed again and again. You'll get what I mean if you don't yet.
Try adding this to your program:


DO
   PRINT "Hello, ";
   PRINT "Jack  ";
LOOP


DO and LOOP are two keywords that always work together, and in that order.
What is does is process the code between DO and LOOP then repeat over, and
over, and over again.


   USING TAB INDENTATIONS
   ======================

See how the code between is now indented(tabbed in)? This isn't essential, but
comes in handy when you're trying to read your code and find places,
especially when your programs get big. It's just a way of visually organising
the code. You should always indent the data inside a new LOOP or other similar
procedure.

Ran it? It's pretty obvious what that was going to do. Uh oh. The computer's
buggered now... Actually it's fine. Whenever you're in a situation during
run-time when you can't get out, press CTRL+BREAK to abort the program, it
returns you to the QB interface. Don't worry, we'll start making programs
that are actually *useful* soon. ;)


   FOR/NEXT LOOPS
   ==============

We'll come back to DO/LOOP later and what else you can do with it. Another
kind of loop is FOR/NEXT. This increments the value of a numerical variable
bit by bit until it has reached the amount you have specified.


FOR x = 1 to 10
   PRINT "This is line number "; x
NEXT x


what FOR/NEXT does is starts off with x as a value of 1, and keeps repeating
the code in between, adding 1 to the value of x every time around then stops
repeating only once x has reached a value of 10. This is a simple program
that will repeat the PRINT command in-between, in this case 10 times. You
can also refer to the FOR/NEXT variable and return its value in the mean
time. It should be obvious by now that every time the line is re-printed,
the number increases by one.


   DECISION MAKING 1 (IF/THEN)
   ===========================

So far our programs have been pretty linear, that is they only flow in one
path like a book, there are no options. Well, using the book as a comparison
again, let's compare decisions in programming to say, a 'Choose your own
Adventure' story. (Cars, books, what next?)

Depending on the value of specific variables or the status of other computer
devices we can make the program react one way or the other way. The way we
do this is with IF/THEN. These two keywords go together like DO/LOOP and
FOR/NEXT, except IF/THEN does not loop. Here's a program that uses IF/THEN
to respond to user input.


anumber = 46
INPUT "Enter a number.: ", user
IF user = anumber THEN PRINT "The numbers match."
IF user <> anumber THEN PRINT "The numbers are different."


See how it works? The third line checks to see IF variables anumber and user
match and if they do, it prints the message that follows. Notice how the
equals(=) sign has a different usage here. It is not giving the variable
to its left(user) the value of anumber, instead it is checking to see if it
already has the same value as 'anumber'.

The second line is just as simple. The two arrows(<>) mean greater/less than
and are also used to compare the values of two entities. In this example it
means IF user is greater than or less than anumber THEN print ...etc.
In other words, <> represents 'NOT equal to'.


   Decision making 2 (More on IF/THEN)
   ===================================

The above method is only suitable when you need to find out only one, perhaps
two possibilities. More, and coding becomes a bit messy.
Behold another way of using IF/THEN in the program below:



CLS
INPUT "Enter your age: ", age
IF age = 18 THEN
  PRINT "Hey, that's my age too."
ELSEIF age < 0 THEN
  PRINT "Whoa, I'm confused ??!!."
ELSEIF age <= 10 THEN
  PRINT "Kid"
ELSEIF age <= 19 THEN
  PRINT "Teenager"
ELSEIF age <= 44 THEN
  PRINT "Adult"
ELSEIF age <= 59 THEN
  PRINT "Middle-Aged"
ELSEIF age <= 100 THEN
  PRINT "Elderly"
ELSEIF age >= 101 THEN
  PRINT "Six foot under"
END IF

PRINT "End of program."
END


This may seem a bit daunting at first but really the concept is so simple.
What this IF/THEN procedure is doing is going through the options one by one,
comparing the age variable to what range it must match. If it matches, the
action related is carried out and the IF/THEN procedure is exited, past the
END IF line and continues running the lines below.

To clear up some possible confusion on the math symbols, look here:

   <=    Less than or equal to
   >=    Greater than or equal to

For example. Say you enter 50 as an age. Here is what it is doing.

Is age equal to 18?                    FALSE
Is age less than 0?                    FALSE
Is age equal to or less than 10?       FALSE
Is age equal to or less than 19?       FALSE
Is age equal to or less than 44?       FALSE
Is age equal to or less than 59?       TRUE
Alright, display the message then.



By the way, remember to indent, much easier to read!

   END
   ===

As for the last two lines, this is just to illustrate where the program goes
afterwards. It simply finishes the IF/THEN procedure then goes here. END
is an important command, it stops the flow of the program and exits it for
you. Yes, I know it exits anyway but using END is a good habit to get into.
When you have larger programs you may want the program to end at a certain
line but there may be code below. Using END means the program ends before the
rest of the code is run. The line   PRINT "End of program"  isn't at all
necessary, it's just there to show you.

Here is how the program responds to age 18:

Is age equal to 18?                    TRUE
Alright, display message then.


That's fair enough you say, but notice how 18 also fits into the <= 19 range
but does NOT display "Teenager"? Well, this goes to show that the IF/END IF
does not continue once one value has been matched, it exits straight after.
However, if you did it the 'old' way, with THEN at the end of each IF line
it is not modulated and any line below that matches will still be displayed,
that is:

IF age <= 19 THEN PRINT "Teenager"

... Would still be displayed. Well, try it out yourself to see.




   Decision making 3 (SELECT CASE)
   ===============================

When it comes to dealing with a large range of possibilities to react to,
I much prefer avoiding IF/THEN: I use a similar statement, SELECT CASE.
Here's exactly the same program using SELECT CASE:

CLS
INPUT "Enter your age: ", age
SELECT CASE age
CASE 18
  PRINT "Hey, that's my age too."
CASE IS < 0
  PRINT "Whoa, I'm confused ??!!."
CASE IS <= 10
  PRINT "Kid"
CASE IS <= 19
  PRINT "Teenager"
CASE IS <= 44
  PRINT "Adult"
CASE IS <= 59
  PRINT "Middle-Aged"
CASE IS <= 100
  PRINT "Elderly"
CASE IS >= 101
  PRINT "Six foot under"
CASE ELSE
  PRINT "Not describable"
END SELECT

PRINT "End of program."
END

Notice the significant differences. Unlike IF/END IF, SELECT CASE/END SELECT
does not check for any value matches. All the first line (SELECT CASE) does
is name the entity being checked, and it does not have to be named again as
you can see.

Here's another thing to watch out for. When you are using < or > signs at all,
you must have CASE IS. If it is just equals(=), use CASE only. The first value
checked for(18) is a good example. again this is easy to remember once
you've put it to use a few times.


   CASE ELSE
   =========

Notice how I chucked in that CASE ELSE in, but there is no equivalent response
for IF/END IF? Well, that's one advantage SELECT CASE has. Unlike IF, it can
give a response to an entity only if all others prove false. In this scenario
it is impossible to get to CASE ELSE because all numbers from less than 0
and greater than 101 are covered, including in-between. But CASE ELSE may
come in handy in other situations.


Here is a good thing to keep in mind for good programming...

Checking for one, maybe 2 possibilities, use
   IF x = y THEN ...

Checking for multiple possibilities, use
   SELECT CASE x
      ...
   END SELECT


   Label Subroutines
   =================

First of all, let's discuss what a label is. In QB, a label is a line that
can be referred to by a GOTO or GOSUB command. This is pretty old-school now,
not many QB programmers use this but seeing as it's one of the most simple and
more well-known QB statements we'll cover it anyway.

Even further back, In the old...*OLD*...school QB days, each line was given a
number, like this:

10 CLS
20 PRINT "I can't believe I'm still putting line numbers in."
30 END

Their theory was to 'Increase each number by 10 so you have room to program
new lines in-between'. Then I guess one day, somebody just suggested,
'Why don't we just stop using lines numbers altogether?' So they did. The
latest BASIC interpreters like QB still accept line numbers, but it isn't at
all necessary.


   GOTO
   ====

The only reason why lines were numbered was so they could be found and used.
This is where non-linear programming in QB all begun... With GOTO:

10 CLS
20 INPUT "Enter a number:", number
30 IF number <= 99 then GOTO 70
40 IF number >99 then GOTO 90
50 PRINT "The proper end."
60 END
70 PRINT "2 digit number"
80 END
90 PRINT "3 digit number"
100 END

Ugh, how ugly is that? Keep programming this way and you will see how
useless, hard-to-follow and frustrating it becomes. It looks cramped...
What if you run out of 'in-between' numbers, do you have to redo the
whole remaining program's line numbers?... And many, many more problems too...

Okay. Forget I ever showed line numbers to you. Let's go from the old-old-
school method to just the more recent, slightly improved mere old-school:

CLS
INPUT "Enter a number: ", number
IF number <= 99 THEN GOTO twodig
IF number > 99 THEN GOTO threedig
PRINT "The proper end."
END

twodig:
PRINT "2 digit number"
END

threedig:
PRINT "3 digit number"
END


Can you follow that better at all? For starters those line numbers are
extinct. Kaput. Why give all lines labels when only two labels are referred
to by GOTO? So the two lines that need labels still, 70 and 90, QB can allow
you to give them text names, whole words, so you know what they do because
you can call them what you want and need, (excluding numbers at the start,
spaces, and some punctuation)

So, 'GOTO twodig' means go to the label 'twodig:' and continue running the
code under there. Notice labels need a semicolon afterwards, this shows to QB
that it is indeed a label.


   GOSUB
   =====

What if you want to do that bit of code below the label then go back to where
you were before, so you can get to the code that has 'The proper end'? Of
course, you could easily give that line a label as well and at the end of
the twodig and threedig code have GOTO's taking you there.

Bad idea. Can get very messy. What if you want to access the two labels from
elsewhere if it's a larger program... You may then not want to go to the
'proper ending' part. Here's how you do it:

CLS
INPUT "Enter a number: ", number
IF number <= 99 THEN GOSUB twodig
IF number > 99 THEN GOSUB threedig
PRINT "The proper end."
END

twodig:
PRINT "2 digit number"
RETURN

threedig:
PRINT "3 digit number"
RETURN

Notice how GOTO has become GOSUB? This means 'Go to sub-procedure'. Now, a
sub-procedure is a section of code that can be called and ran, then the
program returns to where the GOSUB was requested and continues. The RETURN
statement in a sub means to return. If you donot put RETURN in a sub, or
you put RETURN where no GOSUB has been used you can have some problems,
and eben this can get messy. But nevertheless it is a sure improvemnt. After
visiting whatever sub is necessary, the control returns to the spot of the
GOSUB and continues. In this case, you will safely roll on into 'The proper
end'.

Another thing. See the semicolon(:) following a label? Well, semicolons have
another use... You can use them to separate two lines of QB code while having
them together on the one line, to save space. For example:

PRINT "This line includes END also." : END

Unless you make a major syntax error somewhere, QB should be able to
automatically tell the difference between its two uses, which one you intend.

Now, I don't even truly recommend GOSUB as a good way to program, so why did
I just teach you all that? To give you some brief background on how BASIC has
evolved and improved, and so you just have knowledge of these simple
statements, it just helps although you'll probably disregard GOSUB and RETURN
fully after what you learn in future lessons. ;)



                  PART 3 - PUTTING IT ALL TO GOOD USE
                  ===================================

So, what can be done with that stuff you've just been taught? Well from all
that if you use your imagination, you now know *just* enough to make a good
old 80's style text adventure, albeit a very redundant one if you make it big!

That is, eventually you'll have little sub-procedures floating around
here there and everywhere repeating similar processes in each one. It can
become very hard to follow on a large scale.

The only things not included in it is FOR/NEXT, DO/LOOP and IF/THEN, because
nothing has required any repeated processing that FOR/NEXT can give we are
not advanced enough yet to make realtime games involving DO/LOOP, and I found
SELECT CASE to be the best alternative to IF/THEN in every situation.

Well, let me finish off by giving you a short example program on a text-based
adventure using most of the stuff we've covered in this document alone. Extend
this one if you like, or even make your own!


'                     Text Adventure Demo for QBasic 1.1+

money = 0

CLS
INPUT "What is your name?: ", name$
CLS

start:
PRINT ""
PRINT "It is a stormy night. Your car has broken down so you get out and walk"
PRINT "to find help."
PRINT "Eventually you follow the road's path over a hill and you see an old,"
PRINT "abandoned mansion. It gives you the creeps just looking at it. What do"
PRINT "you do?"
PRINT ""
INPUT "1=Run back to car and hide   2=Venture bravely into mansion: ", choice

SELECT CASE choice
CASE 1
  CLS : GOTO car
CASE 2
  CLS : GOTO inmansion
CASE ELSE
  GOSUB abuse
  GOTO start
END SELECT

car:
CLS
PRINT ""
PRINT "You crap yourself and run back to your car, hiding under the seat and"
PRINT "stay there until dawn. It's too scary out there at night for wimps!"
GOTO endit

inmansion:
PRINT ""
PRINT "You walk up to the door and knock. No answer. So you slowly open the"
PRINT "large, creaky door and sneak inside. It is pitch black, so you light"
PRINT "your torch."
PRINT ""
PRINT "There on the ground is a gold coin. Hmm, change for a call!"
GOSUB findmoney
PRINT "Now all you need is a phone to use it with. Where to now?"
PRINT ""
PRINT "1=Back out to the road to look for a phone-booth"
INPUT "2=Explore the house some more: ", choice
SELECT CASE choice
CASE 1
  CLS : GOTO door
CASE 2
  CLS : GOTO inmore
CASE ELSE
  GOSUB abuse
  GOTO inmansion:
END SELECT



findmoney:
money = money + 1
PRINT ""
PRINT "You have "; money; " dollar/s."
PRINT ""
RETURN

abuse:
PRINT ""
PRINT "What, you're just gonna stand here all night?"
PRINT ""
RETURN

door:
PRINT ""
PRINT "Uh oh. The door has slammed shut and won't open. The windows are boarded"
PRINT "up. There's no other way but to look further around the dark mansion."
INPUT "Press ENTER to go on... ", choice


inmore:
CLS
PRINT ""
PRINT "You hear a deep, eerie voice wailing your name nearby..."
PRINT name$; "..... "; name$; "!!....."
PRINT ""
PRINT "AAAAAAAAAAAAAAAGGGGGGGGGGHHHHHHHH!!!"
PRINT "Your torchlight barely shines upon a dark figure springing out of a"
PRINT "passage to your left. You briefly see the silhouette of an axe-shape,"
PRINT "then you feel the gruesome weapon meet your throat..."
GOTO endit

endit:
PRINT ""
PRINT "THE END"
END
