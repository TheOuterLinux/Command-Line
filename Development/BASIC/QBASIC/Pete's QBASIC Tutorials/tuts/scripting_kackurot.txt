Scripting in qb

March 26,2002





NOTE: not for beginners who are not famialar with QB commands



	Hey all!, Kack here.. today we will be discussing how to make a scripting engine in qb. Firet of all you need to know what scripting is. Scripting is a set of commands( like keywords in qb) that tell something what to do. Qb,basic uses scripting... Well now thats said and done, lets get to the code and explainations.



	First of all you need to decide what kind of symbals or charaters to tell the difference between a command or some text. It is needed so your engine can figure out what is what. The kind of scripts i seen usually used the dollar sign( "$" ). Say for example you want to make a if command, it would look like this "$if". Another way is using tags like in a html file "<if>". Thats crappy in my opinion. Teh best is the dollar sign,, so that is what we are going to use in this tut.

Ok lets do a little script! I will put comments by each line. Oh before we begin I forgot to tell you that your scripts go in a text file not in qb! We will name this script scr.txt .





Script1:			// Script1

*Jonny			// Our character

$talk				//the talk command

Hay coders whats up!		// say something

Im in the house			// say something

$endtalk			// Shuting jonny up! heh....

$move $X 40 $Y 50		// Moving jonny X Y

*Kate			// Our new charater

$talk			// talk command

Whats up jonny!		// say something

$endtalk		// shuting kate up

$endscript		// ending the script

 

	That wasn't so hard eh? Anyway, Our first charater was jonny.

We used the "$talk" command to tell the engine that we are about to have jonny say somethin. Then we use the "$endtalk" command to tell the engine that jonny is done talking. The "$move" command is is used to tell the engine that we want jonny to move at "$X" and at "$Y". Then the last command "$endscript" ... You should realize this one. But anyway, if you don't understand this don't worry, when we write the engine it will all make pefect sense. And now let's make a engine that will interpret all this script!





ok! Lets begin.





Sub Engine.Script(FileName$,Script$)

Open FileName$ for input as 1		'Open the script file

do					'search for a script

input #1,temp$				'input text into string

loop until temp$=Script$		'loop until temp=script

for y= 1 to lof(1)		'start interpreting

input #1,command$	'get first command

for X=1 to Len(command$)	'search tru string



if MID$(command$,X,1)="*" then   ' "*" a person

if MID$(command$,X+1,5)="jonny" then charater$="jonny"  'ch is jonny?

if MID$(command$,X+1,4)="Kate" then charater$="Kate" 'ch is Kate?

endif

if MID$(command$,X,1)="$" then  ' "$" a command

if MID$(command$,X+1,4)="talk" then Gosub talk  'talk command

if MiD$(command$,X+1,4)="move" then Gosub move   'move command

if MiD$(command$,X+1,9)="endscript" then goto donescript ' end script

endif

next X,y

close

exit sub

talk:

print charater$         ' print the person

do			' begin the loop

input #1,say$		' gets its say

if say$="$endtalk" then exit do		'loop until done

print say$		'print its say

loop 

return

move:		' for the move command

if MID$(command$,X+7,2)="$X" then PlX=VAL(MID$(command$,X+9,2)) 'get x

if MID$(command$,X+11,2)="$Y" then PLY=VAL(MID$(command$,X+13,2)) 'get y

if PLX and PLY then Engine.MoveCh(charater$,PLX,PLY) 'PLY and PLX <>0

return

donescript:

close

exitsub



' this a little move sub

'NOTE: just for ex don't use if your making a good game :p



Sub Engine.MoveCh(Person$,X%,Y%)



if Person$="Kate" then

for X=KateX to KateX+X%

put(X,KateY),Kate,pset

next

for Y=KateY to KateY+Y%

put(X,Y),Kate,pset

next

endif



if Person$="jonny" then

for X=jonnyX to jonnyX+X%

put(X,jonnyY),jonny,pset

next X



for Y=jonnyY to jonnyY+Y%

put(X,Y),jonny,pset

next Y

endif



end sub







	Now if you haven't got it yet, then i suggest going over it. What we did here was open the script file and inputed the text commands into the string command$. Then we searched throu the string for the commands. And excuted them how we wanted them to be excuted. If you don't know how to use the MID$ command then i suggest you  find a tut on that cause explaining that will just amke this tut a little longer than it should be. Well that sums it up for now. I migth just write a advanced script tut that uses  if then for and do statements. Until then... heh cya



If any probs(note: no simple probs that concern with qb commands) e-mail me at kack@QBRPGS.com



-Kackurot