+------------------------------------------+
|  RPG Programming Tutorial                |
|                                          |
|  June/July 1997                          |
|  Issue #3 - Part II                      |
|  Released: July 30th, 1997               |
|  Enemy Encounter Engine - Addons         |
+------------------------------------------+

     Well, time for yet another RPG tutorial.  Before we begin, I'd like to
point out that, with these tutorials, I explain how to do individual parts of
a (fairly basic) role playing game.  I am, in no way, attempting to explain
how to create a whole RPG (That would be pointless), rather, I am writing
these to show how to create the vital parts of a role playing game.  It is
ultimately up to you, the programmer, to put these together.

     Now that that's out of the way, in this tutorial, I'll explain how to
add nifty little things to your enemy encoutners such as magic, intelligence,
and multiple enemies/characters.

4.1 MAGIC
------------------>>>>

     Magic is fairly simple to incorporate.  Here, I'll show how to do magic
for one character.  It's up to you to incorporate this for more characters and
enemies.

     First off, it's a good idea to make a list of all of the maigcs on paper
(What? You thought you could do everything in front of the screen?), what they
do, and what characters can use them.  For the sake of this tutorial, here's a
condensed (and simple) list of magics we'll be using.  The MP in the list is
the Magic Points which a player needs to cast the spell and LEVEL is the level
at which a character learns it, I won't bother going into different types of
characters here.  Of course, this list is by no means set in stone, it is just
an example.

+-------------------+------------------------------------------+-----+------+
| MAGIC             | DESCRIPTION                              | MP  | LEVEL|
+-------------------+------------------------------------------+-----+------+
| Fire              | Casts a fireball at an enemy.  Some of   | 5   | 2    |
|                   | the enemies can have weakness/resistance |     |      |
|                   | to this spell.                           |     |      |
+-------------------+------------------------------------------+-----+------+
| Cure              | Regain a player's HP.  This spell does   | 5   | 2    |
|                   | not have to be used in combat.           |     |      |
+-------------------+------------------------------------------+-----+------+
| Death             | Powerful spell which kills an enemy in   | 25  | 8    |
|                   | one blow.  Some enemies can have         |     |      |
|                   | resistance to this.                      |     |      |
+-------------------+------------------------------------------+-----+------+
| etc...            | etc...                                   | etc | etc  |
|                   |                                          |     |      |

     There we go.  That's a small list which we can work with for now.  It is
up to you to add on to this list and expand it, change it, or do whatever with
it.  Now for the magic attack.

     There are some things you should check for before you allow a character
to attack with magic.  Have they learned the spell, do they have enough Magic
Points, did the player wish to cast a spell?  Here's some code which will
(hopefully) help you out.

     First, we should have variables set up which hold each character's magic.
So somewhere in your program you can have:

MaxSpell = 10
DIM SHARED Spell$(MaxSpell)                     ' Assume that there is only
                                                ' one character and they can
                                                ' learn a maximum of 10 spells.

     Now that you have your spell array set up, you must create a routine that
will check if a player has reached a high enough level to learn spells.  The
routine would look something like this:

<<<<---------- Qbasic code, begin cutting here.
IF Level = 2 THEN
        Spell$(1) = "Fire"
        Spell$(2) = "Cure"
    ELSEIF Level = 8 THEN
        Spell$(10) = "Death"
END IF
End cutting here ---------->>>>

     The best place to put such a routine is right after a fight when your
character's experience is tallied.  If the character goes up a level, then
make your program execute this routine.  That way, the player won't keep
'learning' the spells each time a fight is won.

     Okay, the player knows some spells.  So how can they use them? Here's a
way to do it.  First, you must give the player the option to attack an enemy
or use a spell.  This would be done like so.

<<<<---------- Qbasic code, begin cutting here.
CONST True = -1, False = 0                      ' Decision making constants.
                                                ' These would normaly be put
                                                ' at the beginning of a
                                                ' program.

PRINT "(A)ttack ";                              ' Print the user's options on
PRINT "(M)agic  "                               ' the screen.

DO
    Choice$ = LCASE$(INPUT$(1))                 ' A great way to get single
                                                ' CHR$ input from the keyboard.
    IF Choice$ = "a" OR Choice$ = "m" THEN MadeChoice = True
    ' The line above checks for input an exits the loop if the user has
    ' chosen a valid option.

LOOP UNTIL MadeChoice

MadeChoice = False                              ' Have to set this back to 0
                                                ' so we can use it again.

IF Choice$ = "a" THEN
        ' Insert your 'attack enemy' code here.
    ELSEIF Choice$ = "m" THEN
        ' This is where the 'magic attack' code goes.  Keep reading for a
        ' short and simple magic attack routine.
END IF
End cutting here ---------->>>>

     Well, now the player can attack with either a weapon or magic.  So how
can we tell what magic to attack with?  Here some code and an explanation
which should help.

     We must give the player the option of choosing a magic.  We can use our
Spell$() array to aid us in this.  Here is some code which shows how this is
done.

<<<<---------- Qbasic code, start cutting here.
FOR ShowSpell = 1 to MaxSpell                   ' We use a FOR...NEXT loop to
                                                ' go through our array.
    
    IF Spell$(ShowSpell) <> "" THEN             ' We test our Spell$() array
                                                ' to see what spells the
                                                ' player has learned.
        
        PRINT ShowSpell; " "; Spell$(ShowSpell) ' Then we print them on the
                                                ' screen, using the ShowSpell
                                                ' variable as a handy number
                                                ' generator to give the player
                                                ' simple menu options.  So if
                                                ' Fire is the first spell in
                                                ' the array, it would be printed
                                                ' on the screen as: 1 Fire
                                                ' and so on...

    END IF

NEXT ShowSpell

DO

    Choice$ = LCASE$(INPUT$(1))                 ' Again, a good way to read
                                                ' input from the keyboard.

    IF Choice$ = "1" AND Spell$(1) = "Fire" THEN' This is how we can check to
                                                ' see if a player has learned
                                                ' the spell they are attempting
                                                ' to cast.
            ' Stick in your stats for the amount of damage fire will take off
            ' here.  you can also put in routines for fire animation here.
            MadeChoice = True

        ELSE IF Choice$ = "2" AND Spell$(2) = "Cure" THEN

            ' Stick in your stats for the amount of HP cure will restore here.
            ' You can also add some routines for animation here.
            MadeChoice = True

        ELSE IF Choice$ = "10" AND Spell$(10) = "Death" THEN

            ' Stick in your stats for how well the 'Death' spell worked here.
            ' You can also add some routines for animation here.
            MadeChoice = True

    END IF

LOOP UNTIL MadeChoice
MadeChoice = False
End cutting here ----------->>>>

     Well, that's about all the time that I'll spend on magic here.  As you can
see, the code and explanation I gave isn't the whole thing.  But what it is,
is a skeleton which you can use to start off with if you are stuck.

4.2 MULTIPLE CHARACTERS/ENEMIES
------------------>>>>

     Creating code for multiple characters and enemies is very easy.  About
the only thing you must do is run through your character and enemy attack
routines multiple times.  Here's a little frame work to show you how this goes.

DO

    ' Create a routine here that will determine how many characters are alive
    ' and how many can attack.  Then, allow the player to chose the action for
    ' each of these characters, ie. will they guard, attack, use magic, or
    ' attempt to run away.

    ' Create a routine here which will determine how many enemies are alive
    ' and how many can attack.  Basically, follow the same process as above,
    ' only make sure that the computer decides what each enemy does.

    ' Finally, create a FOR...NEXT loop which will allow each character and
    ' enemy to do what they were set up for.  Don't forget to keep track of
    ' whether someone is dead, or the battle is over.  If it is, you can
    ' make a variable souch as 'Done' be 'True' so the DO...LOOP may be quit.

LOOP UNTIL Done

     As you can see, it's up to you to put in the code.  At these stages, most
of the coding that can be done depends on the programmer, and the style of
game which they are programming.  Besides, you should really figure stuff out
for yourself as well!

4.3 INTELLIGENCE
------------------>>>>

     One thing which many RPG's seem to lack is intelligent enemies.  Granted
that a Slime or a Skeleton doesn't necessarily need to be very smart, but it's
often good to have bosses which show a little sign of life!

     Some things to consider for this would be:

     a.) Create a routine which will alow an enemy to heal itself when that
         enemy is running low on hit points.  This would make the enemy much
         harder to defeat, since it can rejuvinate itsef.

     b.) Create a routine which will determine what kind of attack would be
         best for an enemy to use.  Ie. if your enemy has ice magic and one of
         the player's characters is weak against ice, make that enemy cast ice
         on this character.  Also, if there is a weak character in the player's
         party, make the enemy try to kill that one first.  Or, you could make
         the enemy try to kill the character which is causing the most damage
         to it.

     c.) Give the enemy the ability to run! So what if the bad guy will look
         like a chicken, it can add an interesting element to the game.
         Especially if your enemies are on the map screen!

4.4 NEXT MONTH
------------------>>>>

     I should take this time to thank everyone who's supported (badgered?) me
with these RPG tutorials.  Without your help and questions, it would have
been easy for me to just forget about these, or do a half a$$ed job, as I'm
often tempted to do! Anyways, next month, I'll do a tutorial on the one thing
which all of you have been asking me about: GRAPHICS! It will be an in depth
look at some of the best graphics programs out there, plus many tips on how
to make bigger, cooler, and better graphics for your RPG.  See ya...

                                Hail from,       
      
                                        DarkDread
                                        \       /

+----------------------------------------------------------------------------+
| RPG Programming Tutorials (Programming RPG's in Qbasic)                    |
| Issue #3 - Part II, June/July 1997                                         |
| Copyright (c) 1997 DarkDreams                                              |
| Written by: DarkDread                                                      |
|                                                                            |
| Special thanks: All who e-mailed me with ideas and suggestions (Too        |
|                 numerous to mention here)                                  |
|                                                                            |
| This tutorial may be posted anywhere as long as it is left unmodified and  |
| the author is given credit.  The author is not responsible for the use of  |
| these tutorials.                                                           |
+----------------------------------------------------------------------------+

