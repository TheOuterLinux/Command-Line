=============
 4. Graphics
=============
Every computer program involves graphics, from the character-based screens of DOS to the one 
that most people use, SCREEN 13.
To set the graphics mode to SCREEN 13 under QBASIC use the command SCREEN 13, but under 
PowerBASIC you should use
! MOV AX,&H13
! INT 10
this just does the same thing, but in assembler.

SCREEN 13 is the graphics mode used by most programmers because it is fast and uses 256 colours,
the reason that it is fast is because it uses 1 byte for every pixel, in comparison for other 
16 colour modes which use 1/2 a byte and are difficult to use because of this. SCREEN 13 stores
it's pixels at memory location A000h or &HA000 because the screen is 320 pixels wide and 200 
pixels high then 64000 bytes are used to store one screen of information(320 x 200 = 64000), 
this is why there is only one screen page avaliable to the programmer when he/she uses this 
mode.

To place a pixel on the screen using SCREEN 13 you would use the PSET command or in our case we
are going to write a SUB which will be used instead of PSET.

To write a pixel to the screen we muist set the screen mode then POKE a pixel to an offset of 
memory location &HA000. The equation for working out the offset to write the pixel to is simple 
X% + (Y% * 320)
but we need to change the equation slightly X% + (Y% * 320&) the & simbol is needed after 320 
becuase this is to tell the programming language that the result should be stored in a long 
integer to stop the overflow error(I used to use long integers for X and Y to get over this, 
but Squeak told me to do it like this and its faster)
'------------------- Start Program
'COL% is the colour you want to use
'X% is the X location of the pixel you want to place
'Y% is the Y location of the pixel you want to place
SUB PUTPIXEL (X%, Y%, COL%)
DEF SEG = &HA000              'Change the moemory SEGMENT
POKE (X% + (Y% * 320&))       'POKE the colour into the memory OFFSET
END SUB
'------------------- End program

If any PowerBASIC programmers know how to do this is inline assembler, I would like to see it.
Next issue changing the Palette using an RGB Palette function.


----------------------------------------------------------------------------------
This tutorial originally appeared in the BASIX Newsletter, Issue 1 from June, 1999.
It was written by Peter Johnson (a.k.a. Screech)