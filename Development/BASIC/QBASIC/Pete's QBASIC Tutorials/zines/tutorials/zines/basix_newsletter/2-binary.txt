===================================
 4. Hexadecimal and Binary numbers
===================================

I have decided that I will try to tackle these two in the same article. They are basically two 
different number systems that are used by the computer and are needed to be understood by the 
programmer.

The numbering system that we use from day to day is called decimal, this is becuase is uses base 
10 (there are only ten characters involved 0-9), BINARY is base 2 and hexadecimal is base 16, 
this is because binary only uses two characters: 0 and 1, hexadecimal uses 16 characters: 
0-9,A,B,C,D,E and F.


BINARY

Binary only uses two characters to represent its numbers because computers are digital and their
circuits only have two states ON and OFF binary has 1 and 0. Binary numbers are a series of 0's 
and 1's which are then translated by the computer into the appropriate number.
Binary numbers seem complicated at first, but then they get much simpler, here is an example of 
a binary number: 10011, this number is actually 19.

To change a binary number into decimal you need to know what the number means, the numbers go 
from right to left, and each 0 or 1 means OFF or ON respectively. The number farthest to the 
right actually means one and the number on its left means two and its left means four and its 
left means 8 and they keep on doubling for ever.

|-----|-----|-----|-----|-----|-----|-----|-----|
| 128 |  64 |  32 |  16 |  8  |  4  |  2  |  1  |
|-----|-----|-----|-----|-----|-----|-----|-----|
|  0  |  0  |  0  |  1  |  0  |  0  |  1  |  1  |
|-----|-----|-----|-----|-----|-----|-----|-----|

As you can see from the chart the numbers 1,2 and 16 are ON so we add them together to get 19!

Notes:
+ A binary number should always start with 1 because even though the decimal numbers 05 and 5 are
  the same we always write 5 for simplicities sake.
+ Only the numbers 0 and 1 are used in binary anything else is wrong.
+ If the binary number ends with 1 then the decimal equivalent is an ODD number.
+ 0 and 1 are the only numbers that are represented the same in Decimal, Hexadecimal and Binary
 |--------|---------|-------------|
 | Binary | Decimal | Hexadecimal | 
 |--------|---------|-------------|
 |   0    |    0    |      0      |
 |   1    |    1    |      1      |
 |--------|---------|-------------|

HEXADECIMAL

As I mentioned before hexadecimal numbers use A-F in their numbering system, they do this to keep
the number short and so that larger numbers can be represented in a smaller amount of memory.
BINARY Numbers and HEXADECIMAL numbers have a good relationship in that they can be easily 
translated from one to the other, whilst it is slightly more complicated with Decimal and 
Hexadecimal.
Hexadecimal numbers use 0-F so there are 16 characters, to make 15(0 makes the total 16) we use 
4 bits(Binary Digits) so F is 1111 and 0 is 0000(but we just say 0)
		
	|--------|-------------|
	| Binary | Hexadecimal |
	|--------|-------------|
	|  0000  |      0      |
	|  0001  |      1      |
	|  0010  |      2      |
	|  0011  |      3      |
	|  0100  |      4      |
	|  0101  |      5      |
	|  0110  |      6      |
	|  0111  |      7      |
	|  1000  |      8      |
	|  1001  |      9      |
	|  1010  |      A      |
	|  1011  |      B      |
	|  1100  |      C      |
	|  1101  |      D      |
	|  1110  |      E      |
	|  1111  |      F      |
	|--------|-------------|
        
So if we have the number 10011 we split it up into four digit binary codes, 1 and 11(0011) we 
then know that Binary 1 is Hexadecimal 1 and Binary 11 is Hexadecimal 3 so we put them together 
to get 13, the Hexadecimal equivalent of 19.
If you really want to convert Hexadecimal into Decimal then you must:

1. Divide the number by 16

2. If the number is bigger than 15 then repeat step 1

3. PUT the numbers together

eg. 
65 / 16 = 4 remainder 1
4 = 4 and 1 = 1 
4 and 1 is 41
65 = 41

154 /16 = 9 remainder 10
9  = 9
10 = A
9 and A is 9A
154 = 9A

175 / 16 = 10 remainder 15
10 = A
15 = F
A and F is AF
175 = AF

300 / 16 = 18(ignire this number) remainder 12(this is still the last digit)
18 / 16 = 1 remainder 2
1  = 1
2  = 2
12 = C
1 and 2 and C is 12C
300 = 12C

They should be enough for you.


----------------------------------------------------------------------------------
This tutorial originally appeared in the BASIX Newsletter, Issue 2 from July, 1999.
It was written by Peter Johnson (a.k.a. Screech)