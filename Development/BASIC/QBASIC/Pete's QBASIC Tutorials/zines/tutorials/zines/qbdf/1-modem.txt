--------------------------------
"Ground Control to Major Tom..."
--------------------------------
	THE MODEM. ohhhh, its scary. To most, its a little magic boxes that        
opens our computer to the world. In reality, it is very simple to use. The
modem takes bytes of info, turns it into a noise, and sends it over a phone
line. Theoretically, if you could whistle fast enough and at the right 
pitches, you could send a file with the sounds you make. See, no voodoo magic
stuff. As for sending files, that's easy to. The modem sends a ASCII character
to tell the other computer it is sending a file. It then breaks up that file 
into chunks and sends it in hunks of bytes (128, 1k, it depends). The modem 
then waits for a ASCII character from the other computer telling it that all is
well, and to send the next hunk. 

	Ok, I've had my history lesson now, you say, but how do I do it in
Qbasic? SIMPLE. 4 commands are all you need for simple communications.
They are:

OPEN COM.........Open up the modem to get stuff from BASIC
INPUT$...........Gets stuff from the modem
LOC..............Tells you if the modem has any stuff from an outside source
PRINT #..........Send stuff to the modem

	To start work with the modem, you have to open a "path" to it. This        
is just like opening a file, except you use the "OPEN COM" statement. Here
is a sample:

OPEN "COM2:2400,N,8,1,RB2048,TB2048" FOR RANDOM AS #1

	What the #&*! is that?!?, Just like opening a file he says? BULL!
Well, here is a secret, Opened Devices (Your modem) act just like opened
files.

	Don't worry, the only thing you need to worry about is the number 
between "COM" and ":" and the number between ":" and ",N". All the other 
stuff deals with transmission settings and the RB TB things deal with 
Uploading and Downloading. The first Number is COM port number your modem 
is on. Because BASIC was created so long ago, you can only access ports 1 
and 2. If you modem is not on COM port 1 or 2, there is a way around that, 
but it can cause so big time errors with other programs because you manually
switch the memory addresses of the COM ports. Don't worry about that now,
I will go into that in another issue.


	The 2nd number is the Baud. BAUD is the speed of the Modem. Qbasic 
can't access COM ports at any higher speed than 9600, so if you have a 14.4 
Modem, Qbasic can still use it, but it won't go any faster than 9600. Note 
this restriction is for GW-BASIC, BASICA, and QBASIC. I don't know if 
compiled QuickBasic programs have this restriction but I don't think they do.

	To send stuff to your modem, you use the PRINT #n command, where n 
is the file number, in this case 1. But there is no point sending stuff like 
"Hello Chris" to your modem right now, because you are not connected to 
anything. All you have done with the "OPEN COM" statement, is made a little 
path from you modem to your program so they can talk to each other. 

	But you want to talk to an outside source, like a friend or a BBS, 
you have to tell the modem to dial a number. To do this you must know that 
ALL modems have a set of commands echoed on to their memory chips that make 
them do stuff. You can't just say "Hey modem, dial up 770-555-9806", you have 
to talk in the modem's language. This isn't as bad as it sounds. All commands 
begin with "AT".  Here are some common ones:

MODEM SPEECH             TRANSLATION
-------------------------------------------
"ATDT###-###-####"    | "Hey Modem, dial ###-###-####"
"ATZ"                 | "Hey Modem, This sucks, hang up the phone"
"ATS0=#"              | "Wait until you someone calls and the phone rings  
		      |  # number of times, then try to connect modems"
"ATM2H1L#"            | "Set your speaker Volume at # (1-3)"

	So, if you wanted to call someone first you would use an OPEN COM 
statement to ready the modem then you would use an INPUT statement 
to get the phone number to dial as a string, than use PRINT #n to talk to the 
modem. Here is an example of a simple phone dialer:
(replace COM# with the COM port your modem is on)

CLS
PRINT "Opening a Path to your Modem..."
OPEN "COM2:2400,N,8,1,RB7048,TB7048" FOR RANDOM AS #1   
PRINT "Please Enter the Number you wish to Call"
INPUT PhoneNumber$
PRINT "Talking to your modem..."
PRINT #1, "ATDT" ;PhoneNumber$
PRINT "There you go, pick up the phone and talk"
PRINT "Press [ESC] to hang up
DO
LOOP UNTIL INKEY$ = CHR$(27)
PRINT #1, "ATZ"

	But here comes the biggest problem of Modem control with Qbasic, HOW 
DO I READ WHAT COMES FROM THE MODEM? Well there is a little Function called 
LOC that does this. The syntax is:

LOC(n)
n is the file number which if you used my sample, would be 1

	LOC tells you where in a file you are. File? I am not accessing a 
file! you exclaim. As I said before, files and devices work the same way. 
But with a modem, LOC tells if it has received anything. Fine, now you know 
if the modem is getting stuff, put how do you know what it is getting? For
that you use the INPUT$(x,y) function.

INPUT$(x ,y)
x is the number of bytes to get from a file/device
y is the number of the opened file/device

	"x" Should ALWAYS be 1. I know this means that only 1 character can
be read on each pass, but this way EVERY character is read, and none are 
skipped. If you were getting an 11 byte transmission, and "x" was 2, only
the first 10 characters would be read (because it is a multiple of 2) the 
last part would be skipped. This is way for NORMAL communications, keep "x"
as 1. I will going into Downloading in a later newsletter, in which "x" is
not 1. One last thing I will talk about is the "ATS0=#" command. You can        
use this to wait for a call
	
	Alright, I will put this all together give you a fully commented
communications program. You can use this to call up any BBS and interact
with it. Note I have not included any Downloading Abilities. I will Address
this as well as uploading through Qbasic using the Xmodem and Zmodem 
protocols later:

'-------------------------

'Conn-X ver 1.0
CLS
PRINT "Conn-X Ver 1.0           Acidus Software(tm)"
PRINT "What COM port is your Modem on?"
INPUT ">", port$
baud$ = "9600"          '9600 should work with most computers. If you have
			'an older one use "2400"
'Open up that com port
OPEN "COM" + port$ + ":" + baud$+ ",N,8,1,RB2048,TB2048" FOR RANDOM AS #1 
			
PRINT "You can:"
PRINT "1-Call someone"
PRINT "2-Wait for a call"
PRINT "3-Quit"
DO
a = VAL(INKEY$)
LOOP UNTIL a >= 1 and a <= 3    'Get choice
IF a = 3 THEN CLOSE : SYSTEM
IF a = 2 THEN GOTO wait

PRINT "Number to call?"
INPUT ">", number$
PRINT #1, "ATDT" + number       'tell the modem to dial the number
GOTO chat
wait:
PRINT #1, "ATS0=1"              'tell modem to connect after 1 ring

'When a modem connect it returns "CONNECT ####"
'The next hunk of code waits until the modem connects before moving on

a$ = ""
DO
IF LOC(1) THEN a$ = a$ + INPUT$(1, 1)   'if anything in modem add it to a$
LOOP UNTIL INSTR(a$, "CONNECT")         'Wait until modem have Connected

chat:
'If you where waiting for a call, a lot of ASCII character will be printed 
'on the screen. Don't worry, that just the computers getting in sync and
'talking
'You also will not see what you type

CLS
PRINT "Conn-X CHAT, press [ESC] to quit
DO
t$ = INKEY$
IF LEN(t$) THEN PRINT #1, t$    'if you typed something send it to the modem
				'this will be send by the modem to the other
				'computer
IF LOC(1) THEN r$ = INPUT$(1, 1)'if the is something to get, get it and save
				'it as r$
IF LEN(r$) THEN PRINT r$;       'if r$ <> "" then print it. the ";" means a
				'line is not started
LOOP UNTIL t$ = CHR$(27)        'keep doing this until [esc] is pressed
PRINT #1, "ATZ"                 'tell the modem to hang up
CLOSE                           'close the open com statement

'-------------------------
       There you go, a simple communications program through Qbasic that
lets you talk to another computer. As I said Uploading/Downloading will be
discussed in a later newsletter

--------------------------------------------------------------------------------
This tutorial originally appeared in the QBasic Developers Forum, Issue #1.
This was written by Lord Acidus.