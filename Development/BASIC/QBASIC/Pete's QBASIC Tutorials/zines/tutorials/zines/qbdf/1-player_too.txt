----------
PLAYER TOO
----------

	Games. They pass the time. But think video games. What kind are 
a lot of fun? The kind where you play with/against another human player.
Human players add an element of surprise to each game. They learn from
they past mistakes, and adopt new styles. Most Computers have a RANDOM
type feature in them somewhere, no matter how complex the Artificial
Intelligence is. So, to build multi-player, and thus, more enjoy programs,
Game coders MUST learn how to program for to players.

	There are only 2 ways to have multi-player games in Qbasic, as on any        
computer. One way is to play over a Modem/Network. The other way is to have
both characters play on the same computer.
	
	Many people overlook how hard it is to support 2 players evenly
in Qbasic. A method I always see is both players use the same keyboard. This
method looks good, but doesn't work at all. Look at this example from a never
released Acidus Software game "Fighter's DANCE":

StartOfGameLoop:
...blah...        
a$ = UCASE$(INKEY$)
if a$ = "4" THEN 'move player 1 left
if a$ = "6" THEN 'move player 1 right
if a$ = "0" THEN 'player 1 punches

if a$ = "A" THEN 'move player 2 left
if a$ = "D" THEN 'move player 2 right
if a$ = "\" THEN 'player 2 punches
...blah...        
...blah...        
GOTO StartOfGameLoop
	
	There are BIG TIME flaws to this method. First, only one player can 
move only once with ever GameLoop pass. Second, if say, Player 1, holds down 
[6], then his character will move and Player 2 doesn't get the chance to do 
anything.

	The ONLY way to have 2 players play the keyboard is to use a lot of 
"ON KEY(n) GOSUB label" statements. Another way to have 2 players play on 
the same computer is if you have  2 joysticks. Use the STICK(n) and STRIG(n) 
functions. The only way to Avoid the problem of one player hogging the input
such as in "FIGHTERS DANCE", you have to use these 2 methods.

	A final way to have multi-player games is to use a Modem/Network.
Now, Networks are hard as #%&* to deal with. Since I don't have one at home
I can fool around with, I will not go into any form of Network programming
right now. Modems, however, I know quite a bit about. If you don't, read the
article entitled "Ground Control to Major Tom..." in this issue.

	There are really 2 forms of Multi-player games using a Modem.

1-Real time
	This means the same program is running on 2 different machines. Both 
players go at the same time, and the Computer constantly talk to each other 
updating each players status. This is how almost all the newer games are.
"Warcraft", "Command & Conquer", "Doom", and "Quake" are all examples of 
Real-time modem games.

	Real-time modem games are fun to play, because you must react 
instantly to the other players moves. Real time also makes your game run
slower, because it must often (Normal once through each game loop), send and
receive game information. In complex games, transporting large strings of info
becomes a problem. I talk later on how to compress data, but I am currently
working on saving the game data as a binary file and sending the file across
the modem. I will go more into sending game data in file for in a later issue.

2-Turn based
	This means a player does all his moves, then another player does all        
his moves.
	
	This is method is a older way of multi-player programming, but is 
still quite good in the right situations. It is also faster because the modem
doesn't have to transmit ever second. It is only suited for some games because
you don't see what your opponent did until your next turn. There are 2 ways to 
do this method.

2a-Turn based dual action
	In this Both player are playing their game at once at there on 
separate machines. The players do their stuff then select the end of their 
turn. The computers then transmit all the new data. The next turn begins with
both players see what the other did last turn, and react. If one player ends
their turn before another player, they must wait until the other is done
before the new data arrives.

2b-Turn based single action
	Each player moves one at a time, and then end their turn. At the end 
of the turn, The computers transmit new data, and the next person can go. This
was the first kind of multi-player games, and are very slow because you have 
to wait until the other person is done before you can move. This method
is good for games like chess, and other board-game type games. Simple war 
games are also programmed using this method.

	To transmit game information over a modem, you must have a way to
tell where a number ends, and where it begins. Else "8210" could be a 8 and
210, 82 and 10, or 821 and 0. I don't recommend sending strings of actual
text across a modem, because of the extra time it takes. Now for an
example. Lets say I am playing a racing game and I need to tell send only
my current place, speed, and distance traveled to the other computer. I would
use my system of make all these number 3 digits, so that way since all the
numbers are 3 digits long, the other computer can extract them corrected.
lets say I'm in 3rd place, going 127mph, and I've gone 41 miles so far. 
I would make 3 be 003, the speed can stay as is because it is already 3 
digits, and distance would become "041"
	This FUNCTION returns a string representation of any number pumped 
into it as a 3 digit string.

FUNCTION MakeString$(x)
if x>=1000 then 
print "Number more than 3 digits"
MakeString$=""
exit function
end if
if x>=100 then  '3 digit
MakeString$=mid$(str$(x),2,3)   'the mid$ function is used because when ever
				'you convert a number to a string, a space is
				'automatically added to the beginning by BASIC
				'The mid$ is used to bypass it so your string
				'contains only numbers
exit function
end if
if x>=10 then   '2 digits
MakeString$="0"+mid$(str$(x),2,2)
exit function
end if
'Must be 1 digit
MakeString$="00"+mid$(str$(x),2,1)
end function

	I then would piece these 3 integers into a single string of numbers to 
transmit. I would use a code tidbit thing this:

SendMe$=MakeString$(place)+MakeString$(speed)+MakeString$(distance)
	
	Using the numbers I gave before, you would send a string that looked 
like this:
"003127041"
	
	You than send it over the modem using PRINT #n,SendMe$. The receiving
computer gets the values you sent by a code hunk similar to the following.
receive$=""

Do
if loc(1) then receive$=input$(loc(1),1)
loop until len(receive$)=9              'wait until all numbers are received
'Extract the values from ,the received string

OtherPlayersPlace=val(mid$(receive$,1,3))
OtherPlayersSpeed=val(mid$(receive$,4,3))
OtherPlayersDistance=val(mid$(receive$,7,3))

	There you go, a simple way to transmit game information over the 
modem thus created multi-player games.

--------------------------------------------------------------------------------
This tutorial originally appeared in the QBasic Developers Forum, Issue #1.
This was written by Lord Acidus.