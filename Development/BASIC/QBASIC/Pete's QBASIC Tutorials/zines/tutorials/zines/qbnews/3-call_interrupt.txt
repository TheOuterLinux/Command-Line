     The QBNews                                                     Page  5
     Volume  1, Number  3                                      May 22, 1990



     ----------------------------------------------------------------------
        W h o   y a   g o n n a   c a l l ?   C A L L   I N T E R R U P T
     ----------------------------------------------------------------------

     Directory Swapping by Hector Plasmic
     
          You've  all seen professional programs that let you Shell to  DOS
     and  always return you to the proper drive and subdirectory  when  you
     Exit.  It's not too hard to implement something similar for  your  own
     QuickBASIC programs.  QB doesn't have all the functions you need to do
     it, but DOS can lend a hand via Interrupt.
     
          What we need to do is handle all SHELL calls from a common  point
     (Sub).   In   this  Sub,  we'll  determine  the  current   drive   and
     subdirectory,  perform  the  SHELL, then restore  the  old  drive  and
     subdirectory  before  Exitting  the  Sub.
     
          Interrupt 21h function 19h returns the number of the current disk
     drive  in  register  .al as a number (0=A,  1=B,  etc.).
     
          Interrupt  21h function 47h copies the current directory  of  the
     drive  #  in  .dl  (0=default, 1=A, 2=B,  etc.)  as  an  ASCIIZ  (null
     terminated) string into a buffer pointed to by .ds:.si.  The  pathname
     does  not  include the drive identifier or a  leading  backslash.   An
     error can occur if you use an invalid drive specification; this is not
     likely since we are using the default drive, but if you are using this
     function to attempt to read the default directory on a drive that  may
     not   exist,   check   the  carry  flag   (IF   OutReg.FLAGS   AND   1
     THEN...oops!).  .ax will contain 0Fh (invalid drive spec) if the error
     occurs.
     
          Finally, Interrupt 21h function 0Eh sets the default drive to the
     drive  number passed in register .dx (0=A, 1=B,  etc.).This  completes
     the  list of DOS functions we'll need to perform our switching.   What
     follows  is some sample code to show a practical application of  these
     interrupt  functions:
     
     DEFINT A-Z
     
     TYPE RegType2
          AX    AS INTEGER
          BX    AS INTEGER
          CX    AS INTEGER
          DX    AS INTEGER
          BP    AS INTEGER
          SI    AS INTEGER
          DI    AS INTEGER
          Flags AS INTEGER
          DS AS INTEGER
          ES AS INTEGER
     END TYPE
     
     'You must link with QB.LIB (QB.QLB) to use Interrupt functions
     
     DECLARE SUB InterruptX (Intr%, InReg AS RegType2, OutReg AS RegType2)
     
     The QBNews                                                     Page  6
     Volume  1, Number  3                                      May 22, 1990

     DECLARE SUB DoShell (Before$, Args$, After$)
     
     DoShell "Type EXIT to return", "", "Welcome back"   'Just to test it
     END
     
     SUB DoShell (Before$, Args$, After$)'Declare some stuff to use
     
     DIM InReg AS RegType2
     DIM OutReg AS RegType2
     DIM CurrentDrive AS INTEGER
     DIM CurrentDir AS STRING * 64
     
     'Get current disk drive
     
     InReg.AX = &H19 * 256
     InterruptX &H21, InReg, OutReg
     CurrentDrive = OutReg.AX MOD 256
     
     'Get current directory
     
     InReg.AX = &H47 * 256
     InReg.DX = CurrentDrive + 1  'Note adding one to drive for this, or
                                  'could use 0 for default drive
     InReg.DS = VARSEG(CurrentDir)
     InReg.SI = VARPTR(CurrentDir)
     InterruptX &H21, InReg, OutReg
     
     'Do the shell
     IF Before$ <> "" THEN CLS : PRINT Before$  'Optional
     SHELL Args$
     IF After$ <> "" THEN CLS : PRINT After$    'Optional
     
     'Change to old disk drive
     InReg.AX = &HE * 256
     InReg.DX = CurrentDrive
     InterruptX &H21, InReg, OutReg
          '(InReg.AX MOD 256 is the # of logical drives in the
          'system if anyone is interested)
     
     'Change to old subdirectory (Could use Int &H21 func &H3B instead)
     ToDir$ = CHR$(ASC("A") + CurrentDrive) + ":\" + LEFT$(CurrentDir,_
          INSTR(CurrentDir, CHR$(0)))
     CHDIR ToDir$
     
     END SUB
     