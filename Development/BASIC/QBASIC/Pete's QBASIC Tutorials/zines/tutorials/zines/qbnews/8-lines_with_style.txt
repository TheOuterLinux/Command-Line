     The QBNews                                                     Page 45
     Volume  2, Number  3                                September 15, 1991



     ----------------------------------------------------------------------
                            F u n   a n d   G a m e s
     ----------------------------------------------------------------------

     Lines With Style by Larry Stone and Charles Graham
     
        During the Winter of the Big Freeze, with nothing to  do  and  much
     time on his hands, Charles Graham, during a state of extreme  boredom,
     found himself alone with a glass of wine in one hand and  his  fingers
     at the keyboard with his other hand.  Thus was born an algorithm later
     to be shared with us all as source code for his program, "ENERGY".
     
        Aside:  Charles' called to complain that the glass of  wine  wasn't
     in hand until AFTER he created his code <GRIN>.
     
        FUSION, based upon Charles' original algorithm, produces a kaleido-
     scope of colors, design, music and sounds that will provide  hours  of
     delightful fascination.
     
        Before we get much further, you should know that FUSION  only  runs
     in EGA screen 9 or VGA screen 12.   Even if your monitor cannot handle
     these modes you will still benefit from FUSION's  many  other,  useful
     routines.
     
        For example, the PrintROMtable subprogram gives you pixel by  pixel
     control for the ASCII characters 1 through 128. See the article Fonts
     in a Can earlier in this issue for more on this subroutine.
     
        If you study FUSION.BAS, you will notice that except for one  PAINT
     and a couple of PALETTE commands, all graphics are  produced  via  the
     BASIC LINE or CIRCLE statement.  Closer examination reveals the  heart
     of the program is the LINE statement.   QB's LINE statement is one  of
     the most powerful graphic commands available.   Conversely,  it is may
     be one of the least understood graphic command.  Although entire books
     could be written about the LINE statement, only  one  of its  optional
     variables will be examined here -- the "style%" argument.
     
        Simply put, the integer variable, style%, is a mask or stencil laid
     over the line.  When we "paint" the line, the paint falls through  the
     holes of the "stencil" and is held back by the "mask".   Probably  the
     easiest way to show this is by following the logic of  the  subprogram
     called, PrintROMtable.
     
        One final note to you Tech-Heads.  The algorithm that computes  and
     displays "energy" and "plasma" drawings is derived by calculating  the
     polar equations for  each  ellipse  generated  from  weighted,  random
     numbers.  Random numbers are weighted by expressions such as:
     
        g = INT(RND * 590) + 50   '50 - 640 possible points to each drawing
        w = INT(RND * 1.9) + .1   'weighted factor used for yavg, etc.
     
     Where g is a random number whose value is forced to lie between 50 and
     640 and, where w is a random number whose value  is  forced  to  range
     between 0.1 and 1.9, respectively.   Albert Einstein would have  loved
     
     The QBNews                                                     Page 46
     Volume  2, Number  3                                September 15, 1991

     this algorithm - the programmer's universe is totally random  but  the
     programmer is using "dice" that are loaded.  The effect is random, yet
     symmetrical designs.  My additions to Charles' algorithm continue this
     mix of order and chaos - "muon" and "quark" trails  are  produced  and
     displayed with weighted, random numbers.  The same mix of symmetry and
     randomness is employed with color and with the number of drawings that
     display at one time.   Thank you,  Charles Graham,  for the  wonderful
     algorithm.
     
     [EDITOR'S NOTE]
     All code for this article can be found in FUSION.ZIP
     
     **********************************************************************
          Larry  Stone is President of LSRGroup and is involved in  writing
     instructional and large data base application systems for business and
     institutional clients.  He is also the author of SERVICES, a shareware
     application program rated a trophy by "Public Brand Software". He  can
     be  reached  at LSRGroup,  P.O.  Box 5715, Charleston, OR 97420, or in
     care of this newsletter.
     **********************************************************************
     
     **********************************************************************
     Charles Graham is a division head for a local government agency in St.
     Louis County, Missouri.   He  also  teaches  QuickBASIC part time at a
     local community college.   He  is  the  author  of  several  shareware
     products including MOVIES . ON . LINE  and,  Quick Dial.   He  can  be
     contacted at Post Office  Box 58634, St.  Louis, MO 63158, and on  the
     National QuickBASIC Conference.