     The QBNews                                                     Page 20
     Volume  2, Number  3                                September 15, 1991



     ----------------------------------------------------------------------
        W h o   y a   g o n n a   c a l l ?   C A L L   I N T E R R U P T
     ----------------------------------------------------------------------

     A Bug Free CALL INTERRUPT by Cornel Huth
     
     There is a problem with the CALL INTERRUPT routine in QuickBASIC
     and BASIC PDS 7. This table highlights the problems.
     
       Problem                   4.0  4.0b  4.5  CH   6.0  7.0  7.1
     -----------------------------------------------+--------------
     1. DI parameter              X                 |  .    .
     2. INT25/26                  X    X    X       |  .    .    X
     3. INT24 environment         X    X    X    X  |  .    .
     4. INT24 EXE ON ERROR GOTO                     |  .    .    D
     5. INT24 EXE NO ERROR TRAP   X    X    X    X  |  .    .    D
                                                    |
        .   not tested
        X - error present
        D - DOS INT24 fatal error handler gains control
       CH - The CH version is the INTRPT.OBJ that's in QBNWS105.
     --------------------------------------------------------------
     1) typo causes the passed DI register parameter to NOT be used
     2) DOS INT25/26 will always report success even if it failed
     -- DOS fatal error (INT24) in:
     3) QB environment causes system crash
     4) EXE w/ ON ERROR GOTO handler causes system crash
     5) EXE with no ERROR handler causes system crash
     
     In the QuickBASIC 4.x environment any fatal DOS error during a CALL
     INTERRUPT crashes the system. In QB 4.x EXEs with ON ERROR GOTO
     handlers the error is trapped by the error handler. In QB 4.x EXEs
     without ON ERROR GOTO the runtime code starts to print the error text
     to the screen but only gets part of it printed before it locks up.
      
     In PDS 7, the evironment does not crash on a fatal DOS error because
     the BP register is saved (in b$SaveBP) before executing the interrupt.
     Apparently BASIC needs the original BP if a fatal DOS error occurs so
     that it can reference the return address of the caller.
     
     In PDS 7 EXEs, with or without ON ERROR GOTO handlers, fatal errors
     are simply passed through to the INT24 error handler. This brings up
     the Abort, Retry, Fail prompt. Pressing F returns to INTERRUPT.
     INTERRUPT then returns with the carry flag set and DOS error 83d in AX
     (83d=Fail on INT24). The ON ERROR GOTO handler is never invoked.
     Pressing A causes the entire program to immediately exit to DOS.
     
     Wouldn't it be great if CALL INTERRUPT worked predictably all the
     time?  Now it does. INTRPT2.OBJ is a direct replacement for either QB
     or PDS that traps fatal DOS errors and returns control, and decision
     making, to your program. When a fatal DOS error occurs, the carry flag
     is set and the DOS error code is returned in AX.
     
     For example, let's say you want to see if a file exists. What you
     
     The QBNews                                                     Page 21
     Volume  2, Number  3                                September 15, 1991

     could do is open the file and check for any errors. This works fine as
     long as the drive of the file is valid and ready. But if the drive is
     a floppy and the door is open, boom. In the QB4 environment the system
     would lockup, even if ON ERROR GOTO were set. In PDS EXE programs the
     default DOS INT24 handler pops up on a fatal error, even with ON ERROR
     GOTO. This messes up the screen with the infamous Abort, Retry, Fail
     prompt and lets the user abort with files possibly open.  My B-tree
     shareware libraries QBTree 5.5 and QBXDBF 1.0 (dBASE .DBF format) have
     support functions that make use of the INTERRUPT routines and I can't
     allow them to crash the system or present an Abort prompt.  No more
     surprises!
     
     To update your version of INTERRUPT(X) put INTRPT2.OBJ in your
     library directory and then do the following:
     
             C>lib QB.LIB -INTRPT +INTRPT2;
             C>link /qu QB.LIB,QB.QLB,nul,BQLB45.LIB;
     
     BQLB45.LIB pertains to QuickBASIC 4.5. Use BQLB40 for QB 4.00 and
     BQLB41 for QB 4.00b. For BASIC PDS 7.x replace references to QB with
     QBX.LIB and QBX.QLB, and use QBXQLB.LIB for the library prompt.
     
     The source to INTRPT2.ASM, a sample FileExists() function, a DOS
     error listing and INTRPT2.OBJ are in INTRPT2.ZIP file.
     
     *********************************************************************
     Cornel Huth is chief designer and programmer for ScanSoft. He can be
     reached at 6402 Ingram Road, San Antonio, Texas 78238. When he's not
     twiddling bits he's thinking of ways he could.
     *********************************************************************