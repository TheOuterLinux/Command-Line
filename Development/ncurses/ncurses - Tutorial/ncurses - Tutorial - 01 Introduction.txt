NCURSES Programming HOWTO
Prev		Next
1. Introduction

In the olden days of teletype terminals, terminals were away from computers 
and were connected to them through serial cables. The terminals could be 
configured by sending a series of bytes. All the capabilities (such as 
moving the cursor to a new location, erasing part of the screen, scrolling 
the screen, changing modes etc.) of terminals could be accessed through 
these series of bytes. These control seeuqnces are usually called escape 
sequences, because they start with an escape(0x1B) character. Even today, 
with proper emulation, we can send escape sequences to the emulator and 
achieve the same effect on a terminal window.

Suppose you wanted to print a line in color. Try typing this on your console.

echo "^[[0;31;40mIn Color"

The first character is an escape character, which looks like two characters 
^ and [. To be able to print it, you have to press CTRL+V and then the ESC 
key. All the others are normal printable characters. You should be able to 
see the string "In Color" in red. It stays that way and to revert back to 
the original mode type this.

echo "^[[0;37;40m"

Now, what do these magic characters mean? Difficult to comprehend? They 
might even be different for different terminals. So the designers of UNIX 
invented a mechanism named termcap. It is a file that lists all the 
capabilities of a particular terminal, along with the escape sequences 
needed to achieve a particular effect. In the later years, this was replaced 
by terminfo. Without delving too much into details, this mechanism allows 
application programs to query the terminfo database and obtain the control 
characters to be sent to a terminal or terminal emulator.
1.1. What is NCURSES?

You might be wondering, what the import of all this technical gibberish 
is. In the above scenario, every application program is supposed to query 
the terminfo and perform the necessary stuff (sending control characters etc.). 
It soon became difficult to manage this complexity and this gave birth to 
'CURSES'. Curses is a pun on the name "cursor optimization". The Curses 
library forms a wrapper over working with raw terminal codes, and provides 
highly flexible and efficient API (Application Programming Interface). It 
provides functions to move the cursor, create windows, produce colors, play 
with mouse etc. The application programs need not worry about the underlying 
terminal capabilities.

So what is NCURSES? NCURSES is a clone of the original System V Release 
4.0 (SVr4) curses. It is a freely distributable library, fully compatible 
with older version of curses. In short, it is a library of functions that 
manages an application's display on character-cell terminals. In the 
remainder of the document, the terms curses and ncurses are used i
nterchangeably.

A detailed history of NCURSES can be found in the NEWS file from the source 
distribution. The current package is maintained by Thomas Dickey. You can 
contact the maintainers at bug-ncurses@gnu.org.
1.2. What we can do with NCURSES

NCURSES not only creates a wrapper over terminal capabilities, but also 
gives a robust framework to create nice looking UI (User Interface)s in 
text mode. It provides functions to create windows etc. Its sister libraries 
panel, menu and form provide an extension to the basic curses library. 
These libraries usually come along with curses. One can create applications 
that contain multiple windows, menus, panels and forms. Windows can be 
managed independently, can provide 'scrollability' and even can be hidden.

Menus provide the user with an easy command selection option. Forms allow 
the creation of easy-to-use data entry and display windows. Panels extend 
the capabilities of ncurses to deal with overlapping and stacked windows.

These are just some of the basic things we can do with ncurses. As we move 
along, We will see all the capabilities of these libraries.
1.3. Where to get it

All right, now that you know what you can do with ncurses, you must be 
rearing to get started. NCURSES is usually shipped with your installation. 
In case you don't have the library or want to compile it on your own, read on.

Compiling the package

NCURSES can be obtained from ftp://ftp.gnu.org/pub/gnu/ncurses/ncurses.tar.gz or any of the ftp sites mentioned in http://www.gnu.org/order/ftp.html.

Read the README and INSTALL files for details on to how to install it. 
It usually involves the following operations.

    tar zxvf ncurses<version>.tar.gz  # unzip and untar the archive
    cd ncurses<version>               # cd to the directory
    ./configure                             # configure the build according to your 
                                            # environment
    make                                    # make it
    su root                                 # become root
    make install                            # install it

Using the RPM

NCURSES RPM can be found and downloaded from http://rpmfind.net . The RPM 
can be installed with the following command after becoming root.

    rpm -i <downloaded rpm>

1.4. Purpose/Scope of the document

This document is intended to be a "All in One" guide for programming with 
ncurses and its sister libraries. We graduate from a simple "Hello World" 
program to more complex form manipulation. No prior experience in ncurses 
is assumed. The writing is informal, but a lot of detail is provided for 
each of the examples.
1.5. About the Programs

All the programs in the document are available in zipped form here. Unzip 
and untar it. The directory structure looks like this.

ncurses
   |
   |----> JustForFun     -- just for fun programs
   |----> basics         -- basic programs
   |----> demo           -- output files go into this directory after make
   |          |
   |          |----> exe -- exe files of all example programs
   |----> forms          -- programs related to form library
   |----> menus          -- programs related to menus library
   |----> panels         -- programs related to panels library
   |----> perl           -- perl equivalents of the examples (contributed
   |                            by Anuradha Ratnaweera)
   |----> Makefile       -- the top level Makefile
   |----> README         -- the top level README file. contains instructions
   |----> COPYING        -- copyright notice

The individual directories contain the following files.

Description of files in each directory
--------------------------------------
JustForFun
    |
    |----> hanoi.c   -- The Towers of Hanoi Solver
    |----> life.c    -- The Game of Life demo
    |----> magic.c   -- An Odd Order Magic Square builder 
    |----> queens.c  -- The famous N-Queens Solver
    |----> shuffle.c -- A fun game, if you have time to kill
    |----> tt.c      -- A very trivial typing tutor

  basics
    |
    |----> acs_vars.c            -- ACS_ variables example
    |----> hello_world.c         -- Simple "Hello World" Program
    |----> init_func_example.c   -- Initialization functions example
    |----> key_code.c            -- Shows the scan code of the key pressed
    |----> mouse_menu.c          -- A menu accessible by mouse
    |----> other_border.c        -- Shows usage of other border functions apa
    |                               -- rt from box()
    |----> printw_example.c      -- A very simple printw() example
    |----> scanw_example.c       -- A very simple getstr() example
    |----> simple_attr.c         -- A program that can print a c file with 
    |                               -- comments in attribute
    |----> simple_color.c        -- A simple example demonstrating colors
    |----> simple_key.c          -- A menu accessible with keyboard UP, DOWN 
    |                               -- arrows
    |----> temp_leave.c          -- Demonstrates temporarily leaving curses 
                                    mode
    |----> win_border.c          -- Shows Creation of windows and borders
    |----> with_chgat.c          -- chgat() usage example

  forms 
    |
    |----> form_attrib.c     -- Usage of field attributes
    |----> form_options.c    -- Usage of field options
    |----> form_simple.c     -- A simple form example
    |----> form_win.c        -- Demo of windows associated with forms

  menus 
    |
    |----> menu_attrib.c     -- Usage of menu attributes
    |----> menu_item_data.c  -- Usage of item_name() etc.. functions
    |----> menu_multi_column.c    -- Creates multi columnar menus
    |----> menu_scroll.c     -- Demonstrates scrolling capability of menus
    |----> menu_simple.c     -- A simple menu accessed by arrow keys
    |----> menu_toggle.c     -- Creates multi valued menus and explains
    |                           -- REQ_TOGGLE_ITEM
    |----> menu_userptr.c    -- Usage of user pointer
    |----> menu_win.c        -- Demo of windows associated with menus

  panels 
    |
    |----> panel_browse.c    -- Panel browsing through tab. Usage of user 
    |                           -- pointer
    |----> panel_hide.c      -- Hiding and Un hiding of panels
    |----> panel_resize.c    -- Moving and resizing of panels
    |----> panel_simple.c    -- A simple panel example

  perl
    |----> 01-10.pl          -- Perl equivalents of first ten example 
                                programs

There is a top level Makefile included in the main directory. It builds 
all the files and puts the ready-to-use exes in demo/exe directory. You 
can also do selective make by going into the corresponding directory. 
Each directory contains a README file explaining the purpose of each c 
file in the directory.

For every example, I have included path name for the file relative to the 
examples directory.

If you prefer browsing individual programs, point your browser to 
http://tldp.org/HOWTO/NCURSES-Programming-HOWTO/ncurses_programs/

All the programs are released under the same license that is used by 
ncurses (MIT-style). This gives you the ability to do pretty much anything 
other than claiming them as yours. Feel free to use them in your programs 
as appropriate.

1.6. Other Formats of the document

This howto is also availabe in various other formats on the tldp.org site. 
Here are the links to other formats of this document.

1.6.1. Readily available formats from tldp.org

    Acrobat PDF Format

    PostScript Format

    In Multiple HTML pages

    In One big HTML format

1.6.2. Building from source

If above links are broken or if you want to experiment with sgml read on.


    Get both the source and the tar,gzipped programs, available at
        http://cvsview.tldp.org/index.cgi/LDP/howto/docbook/
        NCURSES-HOWTO/NCURSES-Programming-HOWTO.sgml
        http://cvsview.tldp.org/index.cgi/LDP/howto/docbook/
        NCURSES-HOWTO/ncurses_programs.tar.gz

    Unzip ncurses_programs.tar.gz with
    tar zxvf ncurses_programs.tar.gz

    Use jade to create various formats. For example if you just want to create
    the multiple html files, you would use
        jade -t sgml -i html -d <path to docbook html stylesheet>
        NCURSES-Programming-HOWTO.sgml
    to get pdf, first create a single html file of the HOWTO with 
        jade -t sgml -i html -d <path to docbook html stylesheet> -V nochunks
        NCURSES-Programming-HOWTO.sgml > NCURSES-ONE-BIG-FILE.html
    then use htmldoc to get pdf file with
        htmldoc --size universal -t pdf --firstpage p1 -f <output file name.pdf>
        NCURSES-ONE-BIG-FILE.html
    for ps, you would use
        htmldoc --size universal -t ps --firstpage p1 -f <output file name.ps>
        NCURSES-ONE-BIG-FILE.html

See LDP Author guide for more details. If all else failes, mail me at ppadala@gmail.com

1.7. Credits

I thank Sharath and Emre Akbas for helping me with few sections. The 
introduction was initially written by sharath. I rewrote it with few 
excerpts taken from his initial work. Emre helped in writing printw and 
scanw sections.

Perl equivalents of the example programs are contributed by Anuradha Ratnaweera.

Then comes Ravi Parimi, my dearest friend, who has been on this project 
before even one line was written. He constantly bombarded me with suggestions 
and patiently reviewed the whole text. He also checked each program on 
Linux and Solaris.

1.8. Wish List

This is the wish list, in the order of priority. If you have a wish or 
you want to work on completing the wish, mail me.

    Add examples to last parts of forms section.

    Prepare a Demo showing all the programs and allow the user to browse 
    through description of each program. Let the user compile and see the 
    program in action. A dialog based interface is preferred.

    Add debug info. _tracef, _tracemouse stuff.

    Accessing termcap, terminfo using functions provided by ncurses package.

    Working on two terminals simultaneously.

    Add more stuff to miscellaneous section.

1.9. Copyright

Copyright © 2001 by Pradeep Padala.

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, distribute 
with modifications, sublicense, and/or sell copies of the Software, and 
to permit persons to whom the Software is furnished to do so, subject to 
the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name(s) of the above copyright 
holders shall not be used in advertising or otherwise to promote the sale, 
use or other dealings in this Software without prior written authorization.
