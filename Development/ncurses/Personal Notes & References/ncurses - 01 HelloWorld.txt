########################
# ncurses - HelloWorld #
########################

In your main.c file:
-------------------

    #include <ncurses.h>
    
    int main()
    {
        initscr(); //Creates stdscr
        raw();
        printw("Hello World"); //Prints "Hello World" in top left corner
        getch(); //Waits for you to press a key
        endwin(); //Release initscr from memory and close ncurses library
        return 0; //Quit program
    }
    
Replace 'raw()' with 'cbreak()' if you need to be able to use keyboard
shortcuts to quit the program like 'CTRL+Q' and so forth.

Compile using:
-------------

    gcc -o HelloWorld "/path/to/main.c" -lncurses
    
Run:
---

    ./HelloWorld
