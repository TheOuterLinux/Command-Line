Use the following in the terminal on a Debian-based GNU/Linux system
to download a specific package and its immediate dependencies. However,
take note that this will not also download the dependencies of
dependencies and so forth.

sudo apt update
apt-cache depends -i PACKAGE | awk '/Depends:/ {print $2}' | xargs  apt-get download && apt-get download PACKAGE

You can then 'cd' into the directory with the packages and use 
'sudo dpkg -i *.deb' to install all of those packages.
