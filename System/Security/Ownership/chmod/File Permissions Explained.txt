##############################
# File Permissions Explained #
##############################

Symbolic Notation 	Numeric Notation 	English
---------- 	0000 	no permissions
-rwx------ 	0700 	read, write, & execute only for owner
-rwxrwx--- 	0770 	read, write, & execute for owner and group
-rwxrwxrwx 	0777 	read, write, & execute for owner, group and others
---x--x--x 	0111 	execute
--w--w--w- 	0222 	write
--wx-wx-wx 	0333 	write & execute
-r--r--r-- 	0444 	read
-r-xr-xr-x 	0555 	read & execute
-rw-rw-rw- 	0666 	read & write
-rwxr----- 	0740 	owner can read, write, & execute; group can only read; others have no permissions
-rwx---r--  0704    owner can read, write, & execute; group has no permissions; others can only read
-rwxrwxr--  0774    read, write, & execute for owner and group; others can read-only

Works like:

    sudo chmod -R 774 /path/to/file/or/folder
    
        * ignore the first 0
        * -R means "recursive"
