Use this Perl script to defrag files from a start directory.
This script requires root privileges to run.

Usage:

    cd /path/to/start/directory
    sudo /path/to/defragfs . -af
    
The "-af" part tells the script to automatically fix fragmented files.
The ability to run the 'filefrag' program is required.
Without the "-af" part, all it does is essentially uses the 'filefrag'
program, which reports file fragmentation but does not fix anything.
