WTF(6)                                   BSD Games Manual                                  WTF(6)

NAME
     wtf — translates acronyms for you

SYNOPSIS
     wtf [-f dbfile] [-t type] [is] acronym ...

DESCRIPTION
     The wtf utility displays the expansion of the acronyms specified on the command line.  If
     the acronym is unknown, wtf will check to see if the acronym is known by the whatis(1) com‐
     mand.

     If “is” is specified on the command line, it will be ignored, allowing the fairly natural
     “wtf is WTF” usage.

     The following options are available:

     -f dbfile
           Overrides the default acronym database, bypassing the value of the ACRONYMDB variable.

     -t type
           Specifies the acronym's type.  Simply put, it makes the program use the acronyms data‐
           base named /usr/share/games/bsdgames/acronyms.type, where type is given by the argu‐
           ment.

ENVIRONMENT
     ACRONYMDB  The default acronym database may be overridden by setting the environment vari‐
                able ACRONYMDB to the name of a file in the proper format (acronym[tab]meaning).

FILES
     /usr/share/games/bsdgames/acronyms       default acronym database.
     /usr/share/games/bsdgames/acronyms.comp  computer-related acronym database.

SEE ALSO
     whatis(1)

HISTORY
     wtf first appeared in NetBSD 1.5.

BSD                                       April 25, 2003                                      BSD
