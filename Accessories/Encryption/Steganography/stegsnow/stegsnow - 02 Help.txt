Usage: stegsnow [-C] [-Q] [-S] [-V | --version] [-h | --help]
	[-p passwd] [-l line-len] [-f file | -m message]
	[infile [outfile]]
