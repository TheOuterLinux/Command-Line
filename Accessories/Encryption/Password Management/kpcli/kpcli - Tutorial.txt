From: https://www.digitalocean.com/community/tutorials/how-to-use-kpcli-to-manage-keepass2-password-files-on-an-ubuntu-14-04-server

#####[How To Use kpcli To Manage KeePass2 Password Files on an Ubuntu 14.04 Server]#####

PostedMay 16, 2014 26.4k views System Tools Ubuntu
 
jellingwood
By:
Justin Ellingwood

Introduction
------------

Security is something we all have to be concerned about on the internet. Having
strong, unique passwords for your online accounts can go a long way towards
keeping your information secure and isolating security breaches.

With vulnerabilities like HeartBleed surfacing from time-to-time, and trusted
companies falling victim to security breaches, your account information can be
compromised regardless of whether you have been using strong passwords. If you
have been using the same password for different services, the impact can be
much more devastating.

Most of us have been told repeatedly about the importance of proper password
management. This can be made easier with the help of a password management
tool. KeePass is a password management solution that can be used to store and
manage a database of passwords on a machine that you control.

Typically, KeePass2 users interact with a local password database through a
graphical user interface, but in this guide, we'll talk about how to create and
manipulate these files in a server environment. The kpcli tool is a command
line utility for creating and editing KeePass entries and databases.

In the next guide, we will leverage our work by learning how to securely serve
the key database file with nginx. We can then connect to this remote database
from a local KeePass2 installation to retrieve our password information on any
of our internet connected devices.

We will be completing these steps on an Ubuntu 14.04 server.

Download the Necessary Components
---------------------------------

In order to get started, we'll have to get some software from the default
Ubuntu repositories.

We will be downloading the kpcli utility and some helper libraries. Since this
is our first time using apt this session, we will refresh our local package
index before the installation:

    sudo apt-get update
    sudo apt-get install kpcli libterm-readline-gnu-perl libdata-password-perl

Now that you have the components installed, we can get started learning how to
use the command line utility to manage our passwords.

Starting the Session and Getting Oriented
-----------------------------------------

At the prompt, start up an interactive session with kpcli by typing:

    kpcli

You will be given a prompt for interacting with a clean KeePass2 database file.
You can see some of your available options by typing:

    help

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

      cd -- Change directory (path to a group)
      cl -- Change directory and list entries (cd+ls)
   close -- Close the currently opened database
     cls -- Clear screen ("clear" command also works)
    edit -- Edit an entry: edit <path to entry|entry number>
  export -- Export entries to a new KeePass DB (export <file.kdb> [<file.key>])
  . . .

If you already have a KeePass2 database file, you can use sftp to transfer it
to your server and then you can open it in your session like this:

    open password_database.kdbx

Please provide the master password:

You will be asked for your database file's master password. This will open your
database file into the kpcli environment.

If you do not already have a database file, create one now by typing:

    saveas password_database.kdbx

Please provide the master password:
Retype to verify:

You will be asked to supply and verify a master password for the KeePass
database file. This password is used to secure all of your other passwords, so
it is essential that you choose a very good password and that you remember it.

Database files for KeePass2 are traditionally stored with the .kdbx file
extension, so this is a good choice to ensure interoperability between systems.

Now, we have our database file loaded into our kpcli environment. What do we do
and how to we get started?

First, if you checked out the help menu, you may have realized that there are
some commands that emulate basic *nix commands. However, they do not operate on
the filesystem. Instead, they work on the database structure created within our
file.

We'll get oriented by typing:

    ls

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

=== Groups ===
eMail/
Internet/

As you can see, our database was initialized with two groups, called "eMail"
and "Internet". KeePass stores passwords and the associated data in a structure
called an "entry". It also has a structure called "groups" that are used to
organize password entries. Currently, we have two groups, and no entries.

You can make additional groups by using the mkdir command:

    mkdir test

To remove groups, you can instead use the rmdir command:

    rmdir test

Create a Password Entry
-----------------------

Let's create our first entry.

We want to store our password for example.com. This is a website, so it would
be wise to store it in our "Internet" group. Move into this group by typing:

    cd Internet

Now, we can make our entry by typing:

    new

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

    Adding new entry to "/Internet"
    Title: example
    Username: demo_user

You will have to pick a name that will be used to identify the entry in the
database. Next, it will ask you for the username that you use to log into the
site.

    Password:                ("g" to generate a password)

Afterwards, you will be asked for a password. If you already have a password
that you want to use, you can enter it here. You will be asked to verify what
you type. If you do not have a password already, you can simply enter "g" to
tell kpcli to generate a strong password for you.

    URL: http://www.example.com
    Notes/Comments (""):
    (end multi-line input with a single "." on a line)

You will be asked to enter the site's URL next. You can then enter comments or
notes about the account.

For instance, if a site requires that you select security questions to recover
a password, this would be a good place to input the answers that you select.
End your input by pressing [ENTER], typing a dot ".", and then hitting [ENTER]
again.

You will be informed that the database was changed. Type "y" to save your
changes.

If you list the contents of the group now, you will see that you have a new
entry:

    ls

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

=== Entries ===
0. example                                 www.example.com

You can reference this entry by the leading index number or by the title you
gave it.

Working with Existing Entries
-----------------------------

Now that you have an entry, we can learn a bit about how to manipulate existing
entries.

To edit an existing entry, you can use the edit command, and call it by index
number or title:

    edit 0

You will be taken through the fields of the entry one-by-one, which will be
pre-populated with your previous selections. Just hit "ENTER" to accept the
values of any fields that you would like to keep.

To show the contents of an entry, you can use the show command:

    show 0

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

     Path: /Internet
    Title: example
    Uname: demo_user
     Pass:
      URL: http://www.example.com
    Notes:

When the output is shown on your screen, the password will be obscured by a red
(if your terminal supports colored output) block. Basically, this is red text
on a red background. This is used to keep people from seeing your password. You
can still copy and paste the text to enter it into fields.

If you want to show the password on the screen, you can add the -f flag:

    show -f 0

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

     Path: /Internet
    Title: example
    Uname: demo_user
     Pass: _OZ1oMyZiC4ceydUDVvC
      URL: http://www.example.com
    Notes:

You can also add the -a flag to print additional information like the creation
and last modification times.

To remove an entry, you can use the rm command:

    rm example

You will not be prompted about whether you really want to delete the entry, so
be careful. You will be prompted whether you want to save the changes though,
so if you accidentally remove an entry, try not to save. You can then use the
open command to re-open the file.

To move an entry to a different group, you can use the mv command:

    mv 0 /eMail/

To find an entry, you can use the find command:

    find example

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

    Searching for "example" ...
     - 1 matches found and placed into /_found/
    Would you like to show this entry? [y/N]

As the output suggests, the results will be available in a new group called /
_found. If you don't want to keep this group, you can always delete it. The
entries that were found will not be affected.

Other kpcli Commands
--------------------

There are some additional commands that you should learn if you are going to be
using kpcli.

To rename a group, you can use the rename command:

    rename Internet

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

Enter the groups new Title: internet

If you want to check the strength of the passwords you have selected, you can
use the pwck command:

    pwck /

This will recursively check all of your passwords. You could just point it at a
single entry though.

To see some statistics about your database file, you can use the stats command:

    stats

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

    KeePass file version: 2
    Encryption type:      rijndael
    Encryption rounds:    6000
    Number of groups:     2
    Number of entries:    1
    Entries with passwords of length:
      - 20+: 1

Finally, to end your session, you can explicitly save your changes:

    save

You can close the database file you are currently working on and continue to
use kpcli by typing:

    close

Or you can exit out of kpcli entirely by typing:

    exit

Conclusion
----------

In this guide, you learned how to use the kpcli tool to interact with and
create KeePass2 password database files. You should be able to manipulate
entries and files with relative ease from your server environment.

In the next guide, we will show you how to serve your database files with nginx
so that you can interact with your remote database with your local KeePass2
installation.

By Justin Ellingwood
 
jellingwood
By:
Justin Ellingwood


Creative Commons License
This work is licensed under a Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International License.
Copyright © 2018 DigitalOcean™ Inc.
