Usage: gnuplot [OPTION] ... [FILE]
for X11 options see 'help X11->command-line-options'
  -V, --version
  -h, --help
  -p  --persist
  -d  --default-settings
  -c  scriptfile ARG1 ARG2 ... 
  -e  "command1; command2; ..."
gnuplot 5.0 patchlevel 5
