TERMSAVER(1)                                                                         TERMSAVER(1)

NAME
       Termsaver - A simple text-based terminal screensaver

SYNTAX
       termsaver [SCREEN] [OPTIONS]

DESCRIPTION
       Termsaver  is a simple alternative for simulating command-line based screensavers right on
       the terminal, in text-based format.

       To start the screensaver, execute Termsaver command with proper options  as  described  in
       detail  below, and to close it, just use the standard Ctrl+C to interrupt it (or close the
       terminal window if you are on a desktop environment and no longer need it).

SCREEN
       The Termsaver application, to work properly, requires a screen as argument,  which  repre‐
       sents  the  name  of the screensaver to be loaded. Each screen, in turn, will have its own
       set of optional arguments, documented in its help option.

       Here follows a list of currently available  screens  (note  that  this  information  might
       become  outdated  if  updates  are  not  fully documented here, so always rely on the help
       option to display all screens available):

       quotes4all
              displays recent quotes from quotes4all.net

       asciiartfarts
              displays ascii images from asciiartfarts.com (NSFW)

       rssfeed
              displays rss feed information

       clock  displays a digital clock on screen

       randtxt
              displays word in random places on screen

       rfc    randomly displays RFC contents

       programmer
              displays source code in typing animation

       urlfetcher
              displays url contents with typing animation

       jokes4all
              displays recent jokes from jokes4all.net (NSFW)

       matrix displays a matrix movie alike screensaver

       sysmon displays a graphic of CPU/MEM usage over time

       Refer to help option for additional information for each screen usage and example.

OPTIONS
       This program follow the usual GNU command line syntax, with long options starting with two
       dashes (`-').  A summary of options is included below.

       -h, --help
              Displays this help message

       -v, --verbose
              Displays python exception errors (for debugging)

EXAMPLES
       Check all available screens to use:
              termsaver --help

       Run random text screensaver with extra arguments:
              termsaver randtxt --word HelloWorld --delay 1

SEE ALSO
       See more information about this project at:
       http://termsaver.info

COPYRIGHT
        Licensed under the Apache License, Version 2.0 (the "License"); you may
        not use this file except in compliance with the License. You may obtain
        a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

        Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
        WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
        License for the specific language governing permissions and limitations
        under the License.

AUTHORS
       Termsaver was written by Bruno Braga <bruno.braga@gmail.com>,
       and contributors.

BUGS
       Report bugs to authors at:
       http://github.com/brunobraga/termsaver

Termsaver                                 22 March 2012                              TERMSAVER(1)
