#################
# find - Basics #
#################

Looking for a file:
------------------

    find . -type f -iname "PartOfFileName"
    
    * . = starts in current directory
    * -type f = file
    * -iname = look for all containing
    
Looking for a directory:
-----------------------

    find . -type d -iname "PartOfDirectoryName"
    
    * . = starts in current directory
    * -type d = directory
    * -iname = look for all containing
    
Find a file and then execute something based on results:
-------------------------------------------------------

    find . -type f -iname "File.txt" -exec ... {} \;
    
    * -exec = execute
    * ... = put your command here
    * {} = the results of find
    * \; = stop exec
    
    An example; Converting all JPEG's to PNG's:
    
        find /path/to/JPEG/folder -type f -iname "*.jpeg" -exec convert "{}" "{}.png" \;
        
        *This of course would also output "FileName.jpeg.png," but you
         get the idea.
         
    HOWEVER, sometimes using "\;" does not work as intended; for
    example, let us say that you want to open a bunch of files in a
    text-editor or an IDE like 'geany'. To open all of these files,
    each in their own tab, you may have to use "+" instead like so:
    
        find /path/to/folder -type f -iname "*.txt" -exec geany "{}" +

Find the largest file in a directory and its subdirectories:
-----------------------------------------------------------

    find /path/to/dir/ -type f -printf '%s %p\n'| sort -nr | head -10

List all files owned by user "johndoe":
--------------------------------------

    find . -user johndoe

List files larger than 100MB:
----------------------------

    find . -size +100M

List dead symbolic links:
------------------------

    find -L -type l

File count:
----------

    find . -type f | wc -l
    
Recursive find and replace file extension:
-----------------------------------------
    find /path/to/folder -type f -iname '*.md' -print0 | xargs -0 rename --no-overwrite .md .txt {}
