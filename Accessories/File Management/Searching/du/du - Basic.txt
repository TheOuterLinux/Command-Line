##############
# du - Basic #
##############

File size of a file:
-------------------

    du -hs "/path/to/file"

Top 10 largest file/directories:
-------------------------------

    du -a /var | sort -n -r | head -n 10
    
Non-GNU friendly UNIX version:
-----------------------------

    for i in G M K
    do 
      du -ah | grep [0-9]$i | sort -nr -k 1
    done | head -n 11
    
    
Top 10 files/dirs eating your disk space:
----------------------------------------

    du -cks * | sort -rn | head

Output the sizes of all files and folders in current directory:
--------------------------------------------------------------

    du -h –max-depth=1
