
Usage: clex [OPTION]

  -a, --admin      admin mode (system-wide configuration)
      --version    display program version and exit
      --help       display this help and exit
