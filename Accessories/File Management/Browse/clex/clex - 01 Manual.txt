CLEX(1)                              General Commands Manual                              CLEX(1)

NAME
       clex - file manager

SYNOPSIS
       clex [option]

DESCRIPTION
       CLEX is an interactive full-screen file manager. Refer to the on-line help for more infor‐
       mation about usage.

OPTIONS
       -a, --admin
              Run CLEX in admin mode. System-wide configuration can be customized in admin  mode.
              Write access to the system-wide configuration file is required.

       --version
              Display  program version and some basic information (including location of the sys‐
              tem-wide configuration file) and exit.

       --help Display help and exit.

NOTES
       CLEX uses CURSES library.

FILES
       sysconfdir/clexrc
              system-wide  configuration  file  (sysconfdir  directory  is  typically   /etc   or
              /usr/local/etc)

       ~/.clexrc
              personal configuration file

       ~/.clexbm
              personal bookmarks file

AUTHOR
       Copyright (C) 2001-2006 Vlado Potisk <vlado_potisk@clex.sk>

NOTES
       CLEX web page: http://www.clex.sk

                                                                                          CLEX(1)
