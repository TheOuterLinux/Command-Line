pilot-link(7)                               PILOT-LINK                              pilot-link(7)

NAME
       pilot-link - A suite of tools for communicating with Palm handhelds, such as those made by
       Palm, Handspring, Handera, TRGPro, Sony or other Palm Compatible Handheld PDA device.

SECTION
       Pilot-link: Overview

DESCRIPTION
       The pilot-link suite of tools contains a series of conduits, libraries, and language bind‐
       ings  for moving information to and from your Palm device and your desktop or server/work‐
       station system, as well as across the network.

TARGET DEVICE
       The /dev/pilot fallback has been removed in v0.12. The environment variable $PILOTPORT can
       be  set in your shell, to save specifying the port each time. A serial device specified on
       the command-line will be used regardless of any $PILOTPORT setting. If $PILOTPORT  is  not
       set,  and -p is not supplied, all conduits in pilot-link will print the usage information.
       The default connection rate is 9600 baud. You are welcome to try higher baud rates (19200,
       38400,  57600  or  higher)  by  setting  the  $PILOTRATE environment variable, but various
       machines have various limitations. (Be careful about values higher than  115200  on  older
       Linux boxes if you've been using setserial to change the multiplier).

CONDUITS
       A complete list of conduits available from within the pilot-link package as of the date of
       this manpage. If you invoke any of the conduits from pilot-link with no command-line argu‐
       ments at all, the usage summary will be printed.

       Note that there are widespread changes in this release - some conduits have been merged, a
       lot have had new options added and some options have been renamed or removed.

   addresses
       DEPRECATED: Use pilot-addresses --write|--human-readable instead.

   pilot-hinotes
       Syncihronize your Hi-Notes database with your desktop machine.

   install-datebook
       installs a new datebook entry onto your Palm handheld.

   pilot-install-expenses
       Install an expense record from various parameters and arguments passed at connection time.

   install-hinote
       Install local files into your Hi-Note database on your Palm handheld.

   install-memo
       Installs a new Memo Pad entry onto your Palm handheld.

   pilot-install-netsync
       reads or sets the Network Preferences information on a Palm Device. Modifies the  "Network
       Preference"  settings  and  reports additional intormation about the Palm, such as the IP,
       hostname, and other information as they are set on the Palm.

   install-todo
       Updates the Palm ToDo list with one new entry.

   install-todos
       Updates the Palm ToDo list with entries from a local file.

   install-user
       Reads or sets a Palm User and UserID on a Palm Device. Modifies the "User Preference" set‐
       tings  and  reports additional intormation about the Palm, such as the Username and UserID
       as they are set on the Palm

   pilot-memos
       Manipulate Memo entries from a file or your Palm device.

   pilot-addresses
       Read and write address book databases to and from a Palm handheld.

       Changes in 0.12: Incorporates functionality from the now deprecated addresses conduit.

   pilot-archive
       DEPRECATED: Use pilot-read-todos -w --archived instead.

   pilot-clip
       Get or Set the Palm Clipboard contents from STDOUT/STDIN.

   pilot-dedupe
       Removes duplicate records from any Palm database.

   pilot-file
       Dump application and header information from your local PRC/PDB files.

   pilot-foto
       Palm 'Foto' Image Installer/Remover/Fetcher/Converter

   pilot-read-screenshot
       Read screenshots from Palm (ScreenShotDB).

   pilot-schlep
       Package up any arbitrary file and sync it to your Palm device.

   pilot-foto-treo600
       Copies Treo 600 foto databases to current directory and extracts .jpg files from them.

   pilot-foto-treo650
       Copies Treo 600 images and videos to the current directory

   pilot-wav
       Decodes Palm Voice Memo files to wav files you can read

   pilot-xfer
       Backup, sync, and restore databases from a Palm handheld device.

   pilot-read-expenses
       Export Palm Expense application data in a text format.

   pilot-read-ical
       Exports the DatebookDB and/or ToDo applications  (DatebookDB.pdb  and  ToDoDB.pdb  respec‐
       tively) to Ical format. (The ical application must be installed and working on your system
       first. This is NOT the same thing as the iCal Calendar format, however).

   pilot-read-notepad
       List the record information found in the Palm Notepad (Palm OS4 and later) application.

   pilot-read-palmpix
       Convert all pictures in the files given or found on a Palm handheld.

   pilot-read-todos
       Synchronize your Palm ToDo application's database with your desktop machine.

   pilot-read-veo
       Synchronize your Veo Traveler databases

   pilot-reminders
       Exports your Palm Datebook database into a 'remind' data file format.

TOOLS
   pilot-debugsh
       Simple debugging console for a Palm Handheld device (deprecated).

   pilot-dlpsh
       An interactive Desktop Link Protocol (DLP) Shell for your Palm device. This is very useful
       for debugging potential problems with your serial port. Query the RAM/ROM, list databases,
       change your UserID or Username, and many other useful functions. This is the  "Swiss  Army
       Knife" of any of pilot-link's tools.

   pilot-csd
       Connection Service Daemon for Palm Devices

   pilot-getram
       Retrieves the RAM image from your Palm device for use in debugging.

   pilot-getrom
       Retrieves the ROM image from your Palm device for use in debugging.

   pilot-getromtoken
       Reads a ROM token from a Palm Handheld device.

   pilot-nredir
       Accept connection and redirect via Network Hotsync Protocol.

PERL SCRIPTS
       (Available from the source code, not always installed from packages).

   pilot-ietf2datebook.pl
       Converts IETF agenda format to install-datebook format

   pilot-undelete.pl
       Turn  previously  archived  records  into  normal  (non-archived) records by resetting the
       "dirty" bit.

   pilot-sync-plan.pl
       completely synchronize the Palm datebook with the "plan" calendar via  netplan,  including
       authenticating to the netplan server itself.

ADVANCED PROGRAMS
       Some of these programs are not intended for general end-users. More detailed documentation
       on their usage can be obtained by reading the source of the application itself.

   ccexample
       test program demonstrating C++ interface.

   iambicexample
       test program demonstrating C++ interface.

   validate
       experimental program to test C++ features.

OBTAINING
       The most current version of the pilot-link suite of tools can be found at the  pilot  link
       homepage: http://www.pilot-link.org/

MAILING LIST
       The  original  pilot-unix mailing list has been taken offline, and is now being hosted and
       maintained by David A. Desrosiers <desrod@gnu-designs.com>. The original pilot-unix  mail‐
       ing list was maintained by Matthew Cravit. Its mandate was:

       The  pilot-unix  mailing list is for discussion and "idea-sharing" for those interested in
       using your Palm handheld with UNIX systems. This includes people  who  are  interested  in
       helping  to  develop tools to allow the Palm to operate with UNIX and other POSIX systems,
       and possibly to develop an SDK (Software Development Kit) for the Palm for Unix.

       For more information, including how to subscribe to the pilot-link mailing  lists,  please
       visit the pilot-link mailing list subscription page:

       http://lists.pilot-link.org/

       pilot-link  specific  mailing  lists  are  also  available  for  Development, General, and
       Announce topics, at the following:

       http://lists.pilot-link.org/mailman/listinfo/pilot-link-devel

       http://lists.pilot-link.org/mailman/listinfo/pilot-link-general

       http://lists.pilot-link.org/mailman/listinfo/pilot-link-announce

AUTHOR
       Most of the pilot-link tools were written by Kenneth  Albanowski.  This  manual  page  was
       written  by David H. Silber <pilot@orbits.com>, and updated for version 0.12 of pilot-link
       by Neil Williams < linux@codehelp.co.uk > .

       Current maintainer of pilot-link  as  of  the  authoring  of  this  manpage  is  David  A.
       Desrosiers <desrod@gnu-designs.com>.

       Any  questions  or  problems with pilot-link should be directed at the various  pilot-link
       mailing  lists  or  to  the  irc   channel   dedicated   to   supporting   pilot-link   at
       irc.pilot-link.org  in your irc application (not a web browser).

PATCHES
       All patches you wish to submit should be sent to <patches@pilot-link.org> directly, with a
       brief explanation of what the patch adds or fixes. Please make sure to send  your  patched
       in unified diff format (diff -u).

SEE ALSO
        pilot-addresses(1),     pilot-dlpsh(1),     pilot-hinotes(1),     pilot-ietf2datebook(1),
       pilot-install-datebook(1),       pilot-install-expenses(1),       pilot-install-hinote(1),
       pilot-install-memo(1),  pilot-install-netsync(1),  pilot-install-user(1), pilot-getrom(1),
       pilot-getram(1),       pilot-addresses(1),       pilot-xfer(1),        pilot-read-ical(1),
       pilot-read-notepad(1), pilot-read-palmpix(1)

       David  Silber  <pilot@orbits.com>  has  written  a  Pilot  HOWTO,  which  you  can find at
       http://www.sgmltools.org/HOWTO/Pilot-HOWTO/t1.html

0.12.5                               Copyright 1996-2007 FSF                        pilot-link(7)
