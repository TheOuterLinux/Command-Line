.::gnugo(6)                    User Contributed Perl Documentation                    .::gnugo(6)

NAME
       gnugo - The GNU program to play the game of Go

SYNOPSIS
       gnugo [--boardsize <num>] [--color <color>] [--handicap <num>] [--komi <num>] [--quiet]
       [-v, --version] [-h, --help] [--help debug] [--copyright] [--mode <mode>] [--replay
       <color>] [-l, --infile <filename>] [-L, --until <move>] [-o, --outfile <filename>]
       [--printsgf <filename>] [-D, --depth <num>] [-B, --backfill_depth <num>] [--score [esti‐
       mate⎪finish⎪aftermath] ] [-a, --allpats] [-T, --printboard] [-d, --debug <level>] [-w,
       --worms] [-m, --moyo <level>] [-b, --benchmark num] [-t, --trace] [-r, --seed num]

DESCRIPTION
       GNU Go plays a game of Go against the user. It has many other features: it can play
       against itself or another program, analyse and score a recorded game. GNU Go is compliant
       with Go modem protocol, load and save game in the Smart Game format.

       GNU Go default is a simple alpha-numeric board display, but you can use a client such as
       CGoban.

       The game of Go

       Go is a game of strategy between two players usually played on a 19x19 grid called goban.
       The two players put black and white stones on the goban to enclose territory. Go was
       invented about 4000 years ago in ancient China. Other names for this game are (Chinese)
       Wei Chi, (Korean) Baduk and (Ing) Goe.

       Playing a game in ASCII mode

       To start a game with default options, just invoke "gnugo". The board will be drawn at your
       terminal using ASCII letters.  In this mode, you can get help on available commands by the
       h key.  To play as Black with 4 stones handicap, with a 0.5 komi, recording the game in
       the file record.sgf:

        gnugo --color black --handicap 4 --komi 0.5 -o record.sgf

       Playing a game with CGoban

       CGoban is a general purpose client program by Bill Shubert for playing Go. It runs under X
       Window System with a beautiful resizeable graphic display. To use GNU Go under X Window
       System, obtain the most recent version of CGoban from Bill Shubert's web site

       http://www.igoweb.org/~wms/comp/cgoban/index.html

       Start CGoban. When the CGoban Control panel comes up, select `Go Modem.'  You will get the
       Go Modem Protocol Setup. Choose one (or both) of the players to be ``Program,'' and fill
       out the box to the path to gnugo. After clicking OK, you get the Game Setup window. Choose
       ``Rules Set'' to be Japanese (otherwise handicaps won't work). Set the board size and
       handicap if you want. Click OK and you are ready to go.

       In the Go Modem Protocol Setup window, when you specify the path to GNU Go, you can give
       it command line options, such as --quiet to suppress most messages. Since the Go Modem
       Protocol preempts standard I/O, other messages are sent to stderr, even if they are not
       error messages. These will appear in the terminal from which you started CGoban.

       Scoring system

       The game stops when both players pass. GNU Go will attempt to compute and report the score
       to you. It may occasionally make mistakes due to wrong evaluation of the status of a
       group. You can check the score as follows. In ASCII mode, at the end of the game, stones
       believed dead are marked in lower case letters, and you have the option of toggling their
       status before counting.  Using CGoban, you may use CGoban's counting facility to count the
       game using either Japanese or Chinese rules.

       Viewing a stored game

       gnugo -l filename.sgf --mode ascii

       loads filename.sgf and lets you navigate through the game by using the commands forward,
       back, goto and last.  It is not possible to navigate through variations in ascii mode.
       You may also use CGoban to view stored games. CGoban can navigate variations.

       Documentation

       The files in the doc directory contain detailed documentation about debugging options and
       internal program structure. Other documentation may be found in comments throughout the
       source code.

       Go Modem Protocol

       The Go Modem Protocol is a standard interface between Go programs and graphical display.

       The Go Modem Protocol was developed by Bruce Wilcox with input from David Fotland, Anders
       Kierulf and others. Any Go program *should* use this protocol since it is standard. Since
       CGoban supports this protocol, the user interface for any Go program can be done entirely
       through CGoban. Using the Go Modem Protocol, you can play with another computer running a
       different program (even on a different operating system) using a modem, a serial cable or
       over the internet if the other program also supports the protocol. You can also communi‐
       cate with the Go servers using CGoban.

       Smart Game Format

       Games (with comments, variations and other features) can be stored in the Smart Game For‐
       mat (SGF). This format originated in Anders Kierulf's program Smart Go. Martin Muller and
       Arno Hollosi developed the current standard, which may be found at

       http://www.red-bean.com/sgf/

       GNU Go supports the Smart Game Format.

OPTIONS
       Main options

       --mode mode

       force the playing mode (ascii', gtp or gmp). Default is ASCII. If no terminal is detected
       GMP (Go Modem Protocol) will be assumed.

       --replay color

       replay the game generating moves for color, where color is white, black, or both.
       (requires -l)

       --quiet

       Don't print copyright and other informational messages.

       -l, --infile file

       Load the SGF file (to score or analyze a recorded game).

       -L, --until move

       Stop loading just before move is played (e.g. 154 or L10).

       -o, --outfile file

       Save the played game to file in SGF format.

       Game Options:

       --boardsize num

       Set the board size to use (1-19). Default is 19, other common formats are 13 and 9.

       --color color

       Choose your color (black or white). Black plays first, White gets the komi compensation.

       --handicap num

       Set the number of handicap stones.

       --komi num

       Set the komi (points given to white player to compensate advantage of the first move, usu‐
       ally 5.5 or 0.5). Default is 5.5.

       Informative Output:

       -v, --version

       Display the version of GNU Go.

       -h, --help

       Display help message.

       --help debug

       Display help about debugging options.

       --copyright

       Display copyright notice.

       Debugging and advanced options:

       -T, --printboard

       Show board each move.

       --level num

       Level of play. (default 10; smaller=faster, weaker).

       -b, --benchmark num

       Benchmarking mode - can be used with -l.

       -t, --trace

       Verbose tracing (use twice or more to trace reading).

       -r, --seed num

       Set random number seed.

       --score [estimate⎪finish⎪aftermath]

       Count or estimate territory of the input file. Usage:

       gnugo --score estimate -l filename

       Loads the SGF file and estimates the score by measuring the influence. Use with -L if you
       want the estimate somewhere else than at the end of the file.

       gnugo --score finish -l filename

       Loads the SGF file and gnugo continues to play by itself up to the very end. Then the win‐
       ner is determined by counting the territory.

       gnugo --score aftermath -l filename

       Similar to --score finish except that a more accurate but slower algorithm is used to
       determine the final status of the groups.

       If the option -o outputfilename is provided, the results will also be written as comment
       at the end of the output file.

       --printsgf outfile

       Load SGF file, output final position (requires -l).

BUGS
       If you find a bug, please send the SGF output file to gnugo@gnu.org together with a
       description of the bug.

3rd Berkeley Distribution                     3.7.7                                   .::gnugo(6)
