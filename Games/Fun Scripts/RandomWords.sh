#!/bin/bash
# Originally from 
# https://superuser.com/questions/692175/how-to-create-a-random-txthuman-readable-text-like-ascii-file-in-linux
# and then edited by TheOuterLinux (https://theouterlinux.gitlab.io)
#
# Purpose: To generate a text file with random words; you will most
#          likely have to add "the," punctuation, and so forth later.

WORDCOUNT="$1"
LINELENGTH="$2"
FILE="$3"
if [ "$WORDCOUNT" = "" ] && [ "$LINELENGTH" = "" ] && [ "$FILE" = "" ]
then
    echo ''
    echo 'Usage:'
    echo '    RandomWords.sh [WordCount] [LineLength] output.txt'
    echo ''
else
    (sed -e "/^[A-Z]/d" -e "/'s\$/d" | shuf -n "$WORDCOUNT" | fmt -w "$LINELENGTH") </usr/share/dict/words > "$FILE"
fi
