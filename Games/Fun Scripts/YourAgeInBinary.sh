#!/bin/bash

#By https://www.commandlinefu.com/commands/by/flatcap
#Edited by TheOuterLinux

read -p "What's your birth date in YYYY-MM-DD format?: " bday
echo "obase=2;$((($(date +%s)-$(date +%s -d "$bday"))/86400))" | bc
