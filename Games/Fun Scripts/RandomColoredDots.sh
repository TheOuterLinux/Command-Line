#!/bin/bash

#By https://www.commandlinefu.com/commands/by/atoponce

#Looks best in an 80x24 256-color terminal emulator. 

while true; do printf "\e[38;5;$(($(od -d -N 2 -A n /dev/urandom)%$(tput colors)))m.\e[0m"; done
