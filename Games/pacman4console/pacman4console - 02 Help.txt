Usage: pacman4console [OPTION]

Options:
  -h, --help        print help
  --level=[1..9]    start at specified standard level
  --level=LEVEL     play specified non-standard LEVEL
