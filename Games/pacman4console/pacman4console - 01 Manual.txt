PACMAN4CONSOLE(6)                   Console based pacman game                   PACMAN4CONSOLE(6)

NAME
       pacman4console - console based pacman game

SYNOPSIS
            pacman4console [ OPTION ]

DESCRIPTION
       This manual page documents briefly the pacman4console.

       pacman4console is an ASCII character based game. This game has nine levels officially.

OPTIONS
       -h, --help
              print help

       --level=[1..9]
              start at specified standard level

       --level=LEVEL
              play specified non-standard LEVEL

SEE ALSO
       pacman4consoleedit(1)

AUTHOR
       pacman4console  was  written  by  Michael  Billars  (aka  Dr.  Mike)  and  is available at
       https://sites.google.com/site/doctormike/pacman.html.

       This manual page was written by Joao Eriberto Mota  Filho  <eriberto@eriberto.pro.br>  for
       the Debian project (but may be used by others).

PACMAN4CONSOLE                               Feb 2014                           PACMAN4CONSOLE(6)
