###################################
# Retrieve your public IP address #
###################################

curl:
----
    
    curl icanhazip.com
    
    curl ipecho.net/plain
    
    curl ifconfig.co
    
    curl -s checkip.dyndns.org|sed -e 's/.*Current IP Address: //' -e 's/<.*$//'
    
wget:
----

    wget -O - -q icanhazip.com
    
    wget -qO- http://ipecho.net/plain ; echo
    
    wget -q -O - checkip.dyndns.org|sed -e 's/.*Current IP Address: //' -e 's/<.*$//'

    
dig:
---

    dig +short myip.opendns.com @resolver1.opendns.com

    dig TXT +short o-o.myaddr.l.google.com @ns1.google.com | awk -F'"' '{ print $2}'

w3m:
---
    w3m -dump ifconfig.me | grep 'IP Address'
    
    w3m https://duckduckgo.com/html/?q=ip

lynx:
----

    lynx -dump ifconfig.me | grep 'IP Address'
