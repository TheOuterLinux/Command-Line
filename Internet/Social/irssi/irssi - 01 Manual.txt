Irssi(1)                    General Commands Manual                   Irssi(1)

NAME
       Irssi - a modular IRC client for UNIX

SYNOPSIS
       irssi  [--config=PATH]  [--home=PATH] [-dv!?] [-c server] [-p port] [-n
       nickname] [-w password] [-h hostname]

DESCRIPTION
       Irssi is a modular Internet Relay Chat client; it is highly  extensible
       and  very  secure.  Being  a fullscreen, termcap based client with many
       features, Irssi is easily extensible through scripts and modules.

OPTIONS
       --config=FILE
              use FILE instead of ~/.irssi/config

       --home=PATH
              PATH specifies the home directory of Irssi; default is ~/.irssi

       -c, --connect=SERVER
              connect to SERVER

       -w, --password=PASSWORD
              use PASSWORD for authentication.

       -p, --port=PORT
              automatically connect to PORT on server.

       -!, --noconnect
              disable autoconnecting of servers

       -n, --nick=NICKNAME
              specify NICKNAME as your nick

       -h, --hostname=HOSTNAME
              use HOSTNAME for your irc session

       -v, --version
              display the version of Irssi

       -?, --help
              show a help message

SEE ALSO
       Irssi has a solid amount of documentation  available;  check  /HELP  or
       look online at http://www.irssi.org

FILES
       ~/.irssi/config
              personal configuration file

       ~/.irssi/config.autosave
              automatic  save  of the personal config file when it was changed
              externally

       ~/.irssi/default.theme
              default irssi theme

       ~/.irssi/away.log
              logged messages in away status

       ~/.irssi/scripts/
              default scripts directory

       ~/.irssi/scripts/autorun/
              directory containing links to  scripts  that  should  be  loaded
              automatically on startup

       ~/.irssi/startup
              file containing a list of commands to execute on startup

AUTHORS/CREDITS
       Irssi  was written by Timo Sirainen; this manpage was written by Istvan
       Sebestyen, Stefan Tomanek, Geert Hauwaerts.

Irssi IRC client                   June 2015                          Irssi(1)
