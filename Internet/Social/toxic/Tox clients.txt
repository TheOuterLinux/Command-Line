From: https://tox.chat/clients.html

Tox Clients

The following is a list of Tox clients available for download through our
repositories and/or build servers.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
[qtox_mac]

qTox

     Website:         https://qtox.github.io
   Repository:     https://github.com/qTox/qTox
   Maintainers:                tux3
    Language:                  C++
Graphical Toolkit:              Qt
Operating Systems:   Linux, Windows, OSX, BSD

qTox is a powerful client based on Qt, with an intuitive and feature rich user
interface as well as a fast core written in C++. qTox runs on Windows, Linux,
MacOS and FreeBSD and offers text messaging, audio and video calls, screen
sharing and file transfers. Additionally it has support for text and audio
group chats as well as Identicons as avatars. New features are regularly added
by our active and welcoming community.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
[utox_mac]

uTox

     Website:               https://utox.io
   Repository:       https://github.com/uTox/uTox
   Maintainers:               GrayHatter
    Language:                      C
Graphical Toolkit:               None
Operating Systems: Linux, Windows, OSX, BSD, Android

µTox is the lightweight client with minimal dependencies; it not only looks
pretty, it runs fast! µTox is available for Windows, OS X, and Linux (Android
support is experimental), with full support for chat, file transfers, audio/
video calling, and desktop sharing (both as video and inline screenshots). It
also supports text group chats with audio groups pending the next Toxcore
update.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
[ricin_linu]

Ricin

   Repository:     https://github.com/RicinApp/Ricin
   Maintainers:            SkyzohKey, gmscs
    Language:                    Vala
Graphical Toolkit:               GTK+3
Operating Systems:               Linux

Ricin aims to be a secure, lightweight, hackable and fully-customizable Tox
client with focus on user experience. Development is lead by SkyzohKey.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
[toxygen_1]

Toxygen

   Repository:     https://github.com/toxygen-project/toxygen
   Maintainers:                      Ingvar
    Language:                       Python3
Graphical Toolkit:                   PySide
Operating Systems:               Linux, Windows

Toxygen is cross-platform Tox client written in pure Python3 with a lot of
unique features such as plugins and faux offline file transfers. Development is
led by Ingvar.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
[toxic_linu]

Toxic

   Repository:     https://github.com/Jfreegman/toxic
   Maintainers:            Jfreegman, mannol
    Language:                      C
Graphical Toolkit:              ncurses
Operating Systems:          Linux, OSX, BSD

Toxic is a client with an ncurses interface, written entirely in C. It has
support for all basic features, as well as 1-on-1 audio/video chats, and is
capable of working on bare-bones systems that lack graphical interfaces.
Development is lead by Jfreegman.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
[antox]

Antox

   Repository:     https://github.com/Antox/Antox
   Maintainers:                wiiaam
    Language:                  Scala
Graphical Toolkit:              N/A
Operating Systems:            Android

Antox is an Android client for Tox. It aims to bring the full multimedia
support Tox offers to your device, although it's still currently in heavy
development. Antox is built on Google's Material Design Guidelines, and is both
fast and fluid. Support for stickers, geo-location, and other services offered
by competing instant messaging applications are currently being decided upon.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
[antidote_1]

Antidote

   Repository:     https://github.com/Antidote-for-Tox/Antidote
   Maintainers:                   Chuongv, dvor
    Language:                      Objective-C
Graphical Toolkit:                     N/A
Operating Systems:                     iOS

Antidote is an iOS client for Tox. It works on both iPhone and iPad. You can
download Antidote on https://antidote.im/

OS support:

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

           Windows   GNU/ Linux BSD OSX Android iOS
  qTox       Yes        Yes     Yes Yes   No    No
  uTox       Yes        Yes     Yes Yes Minimal No
 Ricin   Coming soon    Yes     Yes No    No    No
Toxygen      Yes        Yes     No  No    No    No
 Toxic       No         Yes     Yes Yes   No    No
 Antox       No          No     No  No    Yes   No
Antidote     No          No     No  No    No    Yes
