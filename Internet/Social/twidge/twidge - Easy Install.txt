#########################
# twidge - Easy Install #
#########################

The easiest way to install is to check your package manager. Most of them
have it. However, if not, I made sure to have a copy of the source code
from GitHub here.

Do beware: You will need to install 'ghc.' If you do not have this installed,
it may take up as much as 500 - 600 MB of space.
