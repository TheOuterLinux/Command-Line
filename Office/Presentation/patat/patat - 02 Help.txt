patat v0.4.7.1

Usage: patat [FILENAME] [-w|--watch]
  Terminal-based presentations using Pandoc
  
  Controls:
  - Next slide:             space, enter, l, right
  - Previous slide:         backspace, h, left
  - Go forward 10 slides:   j, down
  - Go backward 10 slides:  k, up
  - First slide:            0
  - Last slide:             G
  - Reload file:            r
  - Quit:                   q

Available options:
  -h,--help                Show this help text
  FILENAME                 Input file
  -f,--force               Force ANSI terminal
  -d,--dump                Just dump all slides and exit
  -w,--watch               Watch file for changes
  --version                Display version info and exit
