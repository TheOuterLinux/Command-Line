   When
   a simple personal calendar
   (c) 2003-2011 Benjamin Crowell
   This free software is copyleft licensed under the same terms as Perl, or,
   at your option, under version 2 of the GPL license.

   The full documentation for When is in its man page, which you can access
   (after installing the software) by doing the command `man when'. If you want
   to read the documentation without first installing the software, use the
   command `nroff -man when.1', or go to http://www.lightandmatter.com/when/when.html
   and look at the reproduction of the man page there.

   To install the program, do the command `make install' while logged in as
   the root user.

   The basic idea is just to type `when' at the command line. The first time
   you run the program, it will prompt you for some setup information. To
   edit you calendar file in your favorite editor, do `when e'. The basic
   format of the calendar file is like this:
        2003 feb 3 , Fly to Stockholm to accept Nobel Prize.
   Once you have a calendar file, running the program as plain old `when'
   from the command line will print out the things on your calendar for the
   next two weeks.

