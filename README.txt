Plain, old-school, the way God intended, UTF-8 text is what you will get 
here. I'm not saying that I don't appreciate markup/markdown, PDF's, and 
so forth, but I want to create a public notes repository that won't take 
up a lot of space if you want to grab a copy of the whole thing while 
also be able to easily access information with text browsers like w3m. 
I'm also planning to make use of ASCII art as best as I can when 
necessary.

Also, please feel free to let me know if I need to correct something.

If you see "⭐," that means that there's more than one program being
mentioned that does most of the same things, but that's the one you want
to use if you can.
